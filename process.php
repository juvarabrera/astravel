<?php
if(isset($_GET['action'])) {
	$action = $_GET['action'];
	if($action == "send") {
		if(isset($_POST['name'], $_POST['email'], $_POST['message'])) {
			$name =  $_POST['name'];
			$email =  $_POST['email'];
			$message =  nl2br($_POST['message']);
			$subject = $_POST['subject'];
			$contact = $_POST['contact'];
			if(!$name || !$email || !$message) {
				header("Location: contact.php?error&haha");
			} else {
				$success = 1;
				date_default_timezone_set('Asia/Manila');
				$emailMessage = $message;
				$emailSubject = "AWT - ".$subject;
				$webmaster = 'inquiry@astravel.com.ph';
				$contact1 = "";
				if($contact != "") {
					$contact1 = " ($contact)";
				}
				$emailContent = '
				<html> 
				<body style="background: #f8f8f8; padding: 20px;">
					<table width="600px" cellpadding="10px" cellspacing="0px" style="background: white; margin: 20px auto; color: black; font-family: sans-serif; font-size: 12px;"> 
						<tr style="background: #1db79c; color: white"> 
							<td align="right">Astravel World Tours Email</td>
					 	</tr>
					 	<tr>
					 		<td style="padding: 20px; line-height: 200%;">
					 			<h1 style="border-bottom: 2px dotted #1db79c; padding-bottom: 10px;">'.$subject.'</h1>
					 			<p>'.$emailMessage.'</p>
					 			<p align="right">'.$name.', <br>'.$email.$contact1.'</p>
					 		</td>
					 	</tr>
					 	<tr style="background: #ddd; color: #4a4a4a; padding: 20px;line-height: 200%;">
					 		<td>
					 			&copy; Astravel World Tours 2015. All rights reserved. 
					 		</td>
					 	</tr>
					</table>
				</body>
				</html>';
				$headers = "From : $name <$email>" . "\r\n";
		 		$headers .= 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				mail($webmaster, $emailSubject, $emailContent, $headers);
				header("Location: contact.php?success");
			}
		} else {
			header("Location: contact.php?error&hehe");
		}
	} else {
		header("Location: index.php");
	}
} else {
	header("Location: index.php");
}
/*
<html> <body style="background: #f8f8f8; padding: 20px;"><table width="600px" cellpadding="10px" cellspacing="0px" style="background: white; margin: 20px auto; color: black; font-family: sans-serif; font-size: 12px;"> <tr style="background: #1db79c; color: white"> <td align="right">

	Astravel World Tours Email

</td></tr><tr><td style="padding: 20px; line-height: 200%;">]<h1 style="border-bottom: 2px dotted #1db79c; padding-bottom: 10px;">

	{{Subject}}

</h1><p>

	{{Message}}

</p><p align="right">

	{{Your Name}}, <br>{{Your Email}}

</p></td></tr><tr style="background: #ddd; color: #4a4a4a; padding: 20px;line-height: 200%;"><td>&copy; Astravel World Tours 2015. All rights reserved. </td></tr></table></body></html>
*/
?>