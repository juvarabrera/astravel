<?php 
	$backDir = '../../';
	if(file_exists($backDir."config.php")) {
		require_once($backDir."config.php"); 
		$pageTitle = "AWT | Terms";
		require_once($backDir."pageview.php");
	}
?><!DOCTYPE html>
<html>
<head>
	<title>Astravel World Tours</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
	<link rel="shortcut icon" href="favicon.ico" />
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="styles/style.php">
	<script src="scripts/jquery.min.js"></script>
	<script src="scripts/smooth_scroll.js"></script>
	<meta name="keywords" content="Astravel world world tours Philippines ph astra star cavite operator cavite agent Philippine philippine Philippine agent Philippine operator international domestic customize group pilgrimage individual southeast asia promos philippine flight ticketing cavite flight ticketing international flight ticketing Philippine airlines cebu pacific air air asia zest tiger airways cebgo skyjet cathay pacific delta airlines united airways Korean air asiana airlines jetstar Malaysia airlines emirates luftansa klm Qatar airlines royal brunei airlines china airlines eva air china southern airlines china eastern airlines garuda Indonesia air busan jeju air jetstar asia airways silkair Singapore airlines tigerair thai airways Bangkok airways thai airasia Etihad Vietnam airlines  hotel reservation hotel booking Philippine hotel reservation Philippine hotel booking international hotel booking international hotel reservation international tickets universal studios legoland Disneyland madame tussauds noah’s ark sentosa hello kitty town genting highlands windows of the world cu chi tunnel nanta car rentals Philippine car rentals international car rentals ferry ticketing sun cruises 2go star cruises royal Caribbean oceania cruises celebrity cruises international cruise reservation Philippine ferry reservation Philippine ferry booking international cruise booking Philippine bus ticketing philtranco ohayahami dltb south west tours passport assistance Philippine passport assistance visa assistance Philippine visa assistance united states visa assistance Schengen visa assistance Europe visa assistance united kingdom visa assistance uae visa assistance abu dhabi visa assistance dubai visa assistance china visa assistance Taiwan visa assistance Australia visa assistance spain visa assistance france visa assistance south Africa visa assistance Netherlands visa assistance Holland visa assistance Greece visa assistance Switzerland visa assistance korea visa assistance japan visa assiatance Egypt visa assistance Czech republic visa assistance turkey visa assistance india visa assistance Sweden visa assistance Russia visa assistance Poland visa assistance Norway visa assistance Denmark visa assistance Ireland visa assistance Belgium visa assistance Austria visa assistance insurance domestic insurance Schengen insurance Europe insurance bluecross worldwide insurance north Luzon baguio ilocos norte ilocos sur banaue sagada alaminos hundred islands Bacolod baler bataan batanes bicol bohol boracay Cagayan de oro cebu cavite davao dumaguete Iloilo manila manila coron el nido Puerto princesa Palawan Puerto galera pangasinan subic tagaytay Corregidor china Beijing shanghai korea south korea seoul jeju island busan macau bruinei Cambodia Indonesia laos Jakarta bali Malaysia kuala lumpur Langkawi johor bahru Yogyakarta Myanmar Singapore Thailand Bangkok Phuket phi phi island Ayutthaya pattaya Vietnam Hanoi ho chi minh Saigon siem reap phnom penh Bhutan india Maldives Nepal sri lanka Tibet uae dubai abu dhabi Europe Austria Italy rome Vatican Greece Santorini Athens Iceland Reykjavik venice Switzerland London united kingdom uk paris france Scotland Scandinavia Norway finland Sweden Denmark spain madrid Barcelona turkey Israel Taiwan ctta member dot accredited agent dot accredited operator department of tourism accredited establishment philtoa Philippine operator association member cavite and tours association member facebook Instagram twitter tumblr packages astravel travel tour">
	<meta name="description" content="Astravel world world tours Philippines ph astra star cavite operator cavite agent Philippine philippine Philippine agent Philippine operator international domestic customize group pilgrimage individual southeast asia promos philippine flight ticketing cavite flight ticketing international flight ticketing Philippine airlines cebu pacific air air asia zest tiger airways cebgo skyjet cathay pacific delta airlines united airways Korean air asiana airlines jetstar Malaysia airlines emirates luftansa klm Qatar airlines royal brunei airlines china airlines eva air china southern airlines china eastern airlines garuda Indonesia air busan jeju air jetstar asia airways silkair Singapore airlines tigerair thai airways Bangkok airways thai airasia Etihad Vietnam airlines  hotel reservation hotel booking Philippine hotel reservation Philippine hotel booking international hotel booking international hotel reservation international tickets universal studios legoland Disneyland madame tussauds noah’s ark sentosa hello kitty town genting highlands windows of the world cu chi tunnel nanta car rentals Philippine car rentals international car rentals ferry ticketing sun cruises 2go star cruises royal Caribbean oceania cruises celebrity cruises international cruise reservation Philippine ferry reservation Philippine ferry booking international cruise booking Philippine bus ticketing philtranco ohayahami dltb south west tours passport assistance Philippine passport assistance visa assistance Philippine visa assistance united states visa assistance Schengen visa assistance Europe visa assistance united kingdom visa assistance uae visa assistance abu dhabi visa assistance dubai visa assistance china visa assistance Taiwan visa assistance Australia visa assistance spain visa assistance france visa assistance south Africa visa assistance Netherlands visa assistance Holland visa assistance Greece visa assistance Switzerland visa assistance korea visa assistance japan visa assiatance Egypt visa assistance Czech republic visa assistance turkey visa assistance india visa assistance Sweden visa assistance Russia visa assistance Poland visa assistance Norway visa assistance Denmark visa assistance Ireland visa assistance Belgium visa assistance Austria visa assistance insurance domestic insurance Schengen insurance Europe insurance bluecross worldwide insurance north Luzon baguio ilocos norte ilocos sur banaue sagada alaminos hundred islands Bacolod baler bataan batanes bicol bohol boracay Cagayan de oro cebu cavite davao dumaguete Iloilo manila manila coron el nido Puerto princesa Palawan Puerto galera pangasinan subic tagaytay Corregidor china Beijing shanghai korea south korea seoul jeju island busan macau bruinei Cambodia Indonesia laos Jakarta bali Malaysia kuala lumpur Langkawi johor bahru Yogyakarta Myanmar Singapore Thailand Bangkok Phuket phi phi island Ayutthaya pattaya Vietnam Hanoi ho chi minh Saigon siem reap phnom penh Bhutan india Maldives Nepal sri lanka Tibet uae dubai abu dhabi Europe Austria Italy rome Vatican Greece Santorini Athens Iceland Reykjavik venice Switzerland London united kingdom uk paris france Scotland Scandinavia Norway finland Sweden Denmark spain madrid Barcelona turkey Israel Taiwan ctta member dot accredited agent dot accredited operator department of tourism accredited establishment philtoa Philippine operator association member cavite and tours association member facebook Instagram twitter tumblr packages astravel travel tour">
</head>
<body>
<div id="container">
	<div class="full-cover small">
		<div class="photo"></div>
		<div class="filter"></div>
		<div class="text">Astravel World Tours<br><small><small>The Guiding Star</small></small></div>
		<div id="action-bar">
			<a id="logo" class="logo"></a>
			<ul class="navigation">
				<li><a href="index.php">Home</a></li>
				<li><a href="about.php">About</a></li>
				<li><a href="services.php">Services</a></li>
				<li><a href="contact.php">Contact Us</a></li>
			</ul>
		</div>
	</div>
	<div class="body-container">
		<div class="wrapper">
			<div class="column-container">
				<div class="col-10">
					<h1 class="center">Terms of Use</h1>
					<p>Dealing with the company through this website is subject to the following conditions:</p>
					<i>The company or the acronym AWT refers to Astravel World Tours.</i>
					<ol>
						<li>Communications between AWT and the User or client via electronic mail (email) is considered official and binding hence content therein is considered accepted by the user for all intents and purposes.</li>
						<li>User or clients refer to the individual or group who uses the website for purposes of doing tour or travel through services provided by AWT.</li>
						<li>Use of this website shall constitute an express acceptance of the Terms and Conditions set forth herein, including own and governing terms and conditions that are specific for certain products and services offered. Any inconsistency between the Terms and Conditions in general and for specific product or service, the terms and conditions of the latter will prevail. Hence user or client is required to read carefully and understand these Terms and Conditions prior to booking.</li>
						<li>Contents of this site are copyrighted by AWT hence reproduction and copying without prior and written permission from AWT will be charged and penalized under applicable laws.</li>
						<li>User of this site is deemed to be of legal age and qualified as well as capable of entering into business transactions. Users or clients shall only make legitimate transactions and not for any other purpose that may include speculative, false, or fraudulent reservations; shall not give misleading information and shall not perform, using this site, any act inimical to others and against the law.</li>
						<li>User of this site will be responsible for maintaining the confidentiality of his/her password or user ID, and shall be fully responsible for all activities that occur under your password or user ID. User must ensure to exit from its account at end of every session, and immediately notify AWT of any unauthorized use or breach of its password or account.</li>
						<li>User agrees that the Statement of Privacy is reasonable and within the capacity of the user to abide with. While AWT is committed to protect privacy and security of its clients within the highest consumer standards, it is the user or clients ultimately, who has the full control of personal information provided.</li>
						<li>AWT may from time to time monitor and review information received through this site and reserves the right to sensor, edit, or remove such information that are inappropriate or in violation of the Terms and Conditions. User agree that should he/she submits suggestions, ideas, comments, queries, or any other information on this site, he/she grants AWT a non-exclusive, royalty-free, perpetual and irrevocable right to use, reproduce, publish, distribute, display, and otherwise communicate to the public such content in any form, media, or technology.</li>
						<li>The site cannot distinguish actual age. The User represent that he/she is of legal age to use this site and create binding legal obligations, and understand that he/she is financially responsible for the use of this site. Immediately contact AWT should a parent/guardian believe that a minor has provided in this site his/her personal information.</li>
						<li>AWT acts as an agent for third party providers. By indicating your acceptance (Contract) to purchase a product or service, you waive your rights to challenge the validity or enforceability of the Contract entered into on this site on the grounds that it was made in electronic form instead of by paper that are signed and/or sealed. Users warrant that they have the capability, proper documents, and is not prohibited by law to purchase and utilise certain products/services. Users must not indicate acceptance to purchase where they have no intention to complete the transaction.</li>
						<li>Users are solely responsible for checking the details of purchase, such as dates, time, and names, before proceeding to payment. AWT and third party providers shall rely only on the information contained in your booking.</li>
						<li>User represent that he/she read the Cancellation Policy of AWT. Bookings that are requested and finalized seven (7) days prior to travel date must be paid within 24 hours upon confirmation of booking. Bookings requested less than seven (7) days prior to travel date must be paid in full before any request for confirmation is initiated.</li>
						<li>Booking requests that have been fully paid but were not confirmed, or for any reason declined by AWT or third party provider, will be refunded in full, less utilised monies, if any. AWT, however, shall not be held liable for any resulting loss, damages, or compensation arising therefrom.</li>
						<li>Any change to confirmed booking is subject to charges applicable to specific product or service. For any change in confirmed booking, please call our customer service during office hours (9:30 am to 4:30 pm Mondays to Fridays/9:30 am to 12:30 pm Saturdays). </li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<div class="body-container logos">
		<div class="wrapper">
			<div class="column-container">
				<div class="col-10 center">
					<img src="images/skin/default/bg/ctta.png"/>
					<img src="images/skin/default/bg/dot.png"/>
					<img src="images/skin/default/bg/philtoa.png"/>
				</div>
			</div>
		</div>
	</div>
	<?php require_once("includes/footer.inc"); ?>
</div>
<script src="scripts/main.js"></script>
</body>
</html>