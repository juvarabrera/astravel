function isMobile() {
	return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}
function showInfo(info) {
	var msg;
	if(info == "making-your-booking") {
		msg = "<h1>Making your Booking</h1><p>To make your tour/travel booking, simply select the package that you want and send over an email to AWT (refer to contact us) or better else, call and/or visit the company's office.</p>";
	} else if(info == "travel-insurance") {
		msg = "<h1>Travel Insurance</h1> <p>As support services to its tour/travel packages, AWT also provides the following insurance-related services as follows:</p> <ul> <li>Travel Including USA, Canada and Hong Kong</li> <ul> <li>Individual</li> <li>Family</li> </ul> <li>Travel Excluding USA, Canada and Hong Kong</li> <ul> <li>Individual</li> <li>Family</li> </ul> <li>Domestic Travel</li> <ul> <li>Individual</li> <li>Family</li> </ul> <li>Travel Insurance for Schengen Visa</li> </ul>";
	} else if(info == "payment-system") {
		msg = "<h1>Payment System</h1> <p>Pay for tour packages can be made via any of the following:</p> <ul> <li>Direct payment to the bank account of AWT</li> <li>Payment via PayPal account</li> </ul>";
	} else if(info == "cancellation-of-bookings") {
		msg = "<h1>Cancellation of Bookings</h1> <p>While bookings already paid by the user/client is considered confirmed, users/clients may opt to make travel plans adjustments that can result to rebooking and/or cancellation of the booking itself.<br> In case of rebooking or cancellation of the trip, the following rules applies:</p> <ol> <li>Should there be rebooking and/or cancellation where AWT will be penalized by third parties (airline company, hotels, and other tourist destinations), such penalty will be at the cost of the user/client and will be deducted from whatever refundable amount will be. Additional payments may be levied upon the user/client as may deemed appropriate or necessary.</li> <li>Refunds of payments for cancellation done 30 days before the actual trip/travel is subject to 30 percent (of the total costs of the booking/trip) charge.</li> <li>No refund will be made for cancellation within the week prior to actual travel schedule and the payment made is considered forfeited hence no refund will be made.</li> </ol>";
	}
	$("#lblInformation").html(msg);
}
function showContainer(id) {
	$("#"+id).css({"display": "block"});
	$("#"+id).css({"max-height": "1000px", "padding-bottom": "50px"});
}
function hideContainer(id) {
	$("#"+id).css({"max-height": "0px", "padding": "0px"});
}
$(document).ready(function() {
	if(isMobile()) {
		$("html").click(function(e) {
			if(!$(e.target).hasClass("ul.navigation")){
				$("ul.navigation").css("max-height", "0px");
			}
		});
		$("#logo").click(function(e) {
			$("#action-bar ul.navigation").css({
				"max-height": "300px"
			});
			e.stopPropagation();
		});
	}
	$("#action-bar .logo").height($("#action-bar ul.navigation").height());
	$("#action-bar .logo").width($("#action-bar ul.navigation").height());
	$("#container .full-cover:not(.small)").height($(window).height());
	$(".circle-image").height($(".circle-image").width());
	$(".icon").height($(".icon").width());
	$h = 0;
	$(".icon-container .icon-list").each(function() {
		if($h < $(this).height())
			$h = $(this).height();
	});
	$(".icon-container .icon-list").height($h);
	$(".package-container .package-list").height($(".package-container .package-list").width()/1.5);
	//470x313
	$("#container .full-cover:not(.small) .text").css({
		"margin-left": "-"+($("#container .full-cover:not(.small) .text").width()/2).toFixed()+"px"
	});
});
$(window).resize(function() {
	$(".circle-image").height($(".circle-image").width());
	$(".icon").height($(".icon").width());
	$("#container .full-cover:not(.small)").height($(window).height());
	$(".package-container .package-list").height($(".package-container .package-list").width()/1.5);
	$("#container .full-cover:not(.small) .text").css({
		"margin-left": "-"+($("#container .full-cover:not(.small) .text").width()/2).toFixed()+"px"
	});
});