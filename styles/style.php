<?php
header("Content-type: text/css");
?>

@font-face {
    font-family: 'allerregular';
    src: url('aller_rg-webfont.eot');
    src: url('aller_rg-webfont.eot?#iefix') format('embedded-opentype'),
         url('aller_rg-webfont.woff2') format('woff2'),
         url('aller_rg-webfont.woff') format('woff'),
         url('aller_rg-webfont.ttf') format('truetype'),
         url('aller_rg-webfont.svg#allerregular') format('svg');
    font-weight: normal;
    font-style: normal;
}
@font-face {
    font-family: 'aramisitalic';
    src: url('aramisi-webfont.eot');
    src: url('aramisi-webfont.eot?#iefix') format('embedded-opentype'),
         url('aramisi-webfont.woff2') format('woff2'),
         url('aramisi-webfont.woff') format('woff'),
         url('aramisi-webfont.ttf') format('truetype'),
         url('aramisi-webfont.svg#aramisitalic') format('svg');
    font-weight: normal;
    font-style: normal;
}
a {
    text-decoration: none;
    color: #1db79c;
    cursor: pointer;
}
body {
    font-family: 'allerregular';
    margin: 0px;
    background: #1db79c;
}
#container {
    width: 100%;
    position: relative;
}
#container .full-cover {
    width: 100%;
    position: relative;
    background-color: #1db79c;
}
#container .full-cover.small {
    height: 130px;
}
#container .full-cover.small .text {
    position: absolute;
    top: 45px;
    left: 150px;
    margin-left: 0px;
    font-size: 35px;
}
#container .full-cover.small .text small {
    display: none;
}
#container .full-cover .photo {
    background-image: url(../images/skin/default/cover/3.jpg);
    background-position: center center;
    backgronud-repeat: no-repeat;
    background-size: cover;
    position: absolute;
    top: 0px;
    left: 0px;
    width: 100%;
    height: 100%;
    -webkit-filter: blur(5px);
    -moz-filter: blur(5px);
    -ms-filter: blur(5px);
    -o-filter: blur(5px);
    filter: blur(5px);
}
#container .full-cover .filter {
    position: absolute;
    top: 0px;
    left: 0px;
    width: 100%;
    height: 100%;
    background: rgba(0,0,0,.4);
    -webkit-transition: all .5s ease-in-out;
    -moz-transition: all .5s ease-in-out;
    -ms-transition: all .5s ease-in-out;
    -o-transition: all .5s ease-in-out;
    transition: all .5s ease-in-out;
}
#container .full-cover .text {
    font-family: 'aramisitalic';
    position: absolute;
    text-shadow: 0px 5px 5px rgba(0,0,0,.5);
    color: white;
    font-size: 80px;
    text-align: center;
    line-height: 120%;
    bottom: 100px;
    left: 50%;
}
#action-bar {
    border-top: 5px solid #1db79c;
    position: relative;
    overflow: visible;
    width: 100%;
    -webkit-transition: all .5s ease-in-out;
    -moz-transition: all .5s ease-in-out;
    -ms-transition: all .5s ease-in-out;
    -o-transition: all .5s ease-in-out;
    transition: all .5s ease-in-out;
}
#action-bar .logo {
    height: 100%;
    display: block;
    position: absolute;
    cursor: pointer;
    top: 0px;
    left: 0%;
    background-image: url(../images/skin/default/logo/logo1.png);
    float: left;
    background-size: contain;
    background-repeat: no-repeat;
    background-position: center center;
    margin: 20px;
}
#action-bar ul.navigation {
    float: right;
    padding: 0px;
    z-index: 5;
    margin: 0px;
    list-style-type: none;
    overflow: hidden;
    margin: 20px;
    -webkit-transition: all 1s ease-in-out;
    -moz-transition: all 1s ease-in-out;
    -ms-transition: all 15s ease-in-out;
    -o-transition: all 1s ease-in-out;
    transition: all 1s ease-in-out;
}
#action-bar ul.navigation li {
    float: left;
    display: inline;
    padding: 0px;
    margin: 0px;
}
#action-bar ul.navigation li a {
    color: white;
    padding: 30px;
    font-weight: bold;
    text-transform: uppercase;
    display: block;
    font-size: 13px;
    position: relative;
    border: 3px solid transparent;
    border-radius: 10px;
    -webkit-transition: all .3s ease-in-out;
    -moz-transition: all .3s ease-in-out;
    -ms-transition: all .3s ease-in-out;
    -o-transition: all .3s ease-in-out;
    transition: all .3s ease-in-out;
}
#action-bar ul.navigation li a:hover {
    color: #1db79c;
}
.body-container {
    width: 100%;
    background: #f9f9f9;
    position: relative;
    padding-bottom: 50px;
    line-height: 190%;
    color: #222;
    -webkit-transition: all .3s ease-in-out;
    -moz-transition: all .3s ease-in-out;
    -ms-transition: all .3s ease-in-out;
    -o-transition: all .3s ease-in-out;
    transition: all .3s ease-in-out;
}
.body-container.color .card {
    margin-bottom: 50px;
    background: white;
    overflow: hidden;
    cursor: pointer;
    position: relative;
    color: #222;
    font-size: 15px;
    border-radius: 10px;
    box-shadow: 0px 0px 10px rgba(0,0,0,.2);
    -webkit-transition: all .3s ease-in-out;
    -moz-transition: all .3s ease-in-out;
    -ms-transition: all .3s ease-in-out;
    -o-transition: all .3s ease-in-out;
    transition: all .3s ease-in-out;
}
.body-container.color .card:hover {
    box-shadow: 0px 0px 20px rgba(0,0,0,.4);
}
.body-container.color .card .wrapper {
    padding: 20px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    -ms-box-sizing: border-box;
    -o-box-sizing: border-box;
    box-sizing: border-box;
}
.body-container .wrapper {
    min-width 480px;
    max-width: 980px;
    margin: 0px auto;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    -ms-box-sizing: border-box;
    -o-box-sizing: border-box;
    box-sizing: border-box;
    padding: 20px;
}
.body-container h1 {
    padding-top: 20px;
    color: #1db79c;
    text-shadow: 0px 4px 4px rgba(0,0,0,.15);
}
.body-container h1.center {
    text-align: center;
}
.body-container hr {
    border: 0px;
    border-top: 2px dashed #1db79c;
}
.body-container.color {
    background: #1db79c;
    color: white;
}
.body-container.color h1 {
    color: white;
    text-shadow: 0px 4px 3px rgba(0,0,0,.2);
}
.body-container.logos {
    background: #eee;
    padding-bottom: 0px;
    border-top: 5px solid #1db79c;
}
.body-container.logos img {
    margin: 20px 30px;
    vertical-align: middle;
}
.body-container.logos .wrapper {
    padding: 0px;
}
.circle-image {
    overflow: hidden;
    color: rgba(255,255,255,.5);
    font-size: 30px;
    line-height: 200%;
    margin: 20px auto;
    width: 75%;
    display: block;
    border-radius: 100%;
    border-bottom: 5px solid #1db79c;
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center center;
}
a.button {
    border: 3px solid #1db79c;
    padding: 10px 15px;
    text-transform: uppercase;
    font-weight: bold;
    border-radius: 10px;
    display: inline-block;
    -webkit-transition: all .3s ease-in-out;
    -moz-transition: all .3s ease-in-out;
    -ms-transition: all .3s ease-in-out;
    -o-transition: all .3s ease-in-out;
    transition: all .3s ease-in-out;
}
a.button.block {
    display: block;
    text-align: center;
}
a.button:hover {
    background: #1db79c;
    border: 3px solid white;
    color: white;
}
.body-container.color a.button {
    color: white;
    border: 3px solid white;
}
.body-container.color a.button:hover {
    background: white;
    border: 3px solid #13a38a;
    color: #13a38a;
}
.body-container.color .card a.button {
    border: 3px solid #1db79c;
    color: #1db79c;
}
.body-container.color .card a.button:hover {
    background: #1db79c;
    color: white;
    border: 3px solid white;
}
.body-container.color .card a.button.orange {
    border: 3px solid #ff7f28;
    color: #ff7f28;
    margin: 15px 0px;
    font-size: 20px;
}
.body-container.color .card a.button.orange:hover {
    background: #ff7f28;
    color: white;
    border: 3px solid white;
}
.body-container.color .card .title {
    width: 100%;
    position: relative;
    top: 0px;
    left: 0px;
    background-color: #13a38a;
    background-repeat: no-repeat;
    background-position: center;
    background-size: 100%;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    -ms-box-sizing: border-box;
    -o-box-sizing: border-box;
    box-sizing: border-box;
    padding: 70px 20px;
    text-align: center;
    -webkit-transition: all .3s ease-in-out;
    -moz-transition: all .3s ease-in-out;
    -ms-transition: all .3s ease-in-out;
    -o-transition: all .3s ease-in-out;
    transition: all .3s ease-in-out;
}
.body-container.color .card:hover .title {
    background-size: 120%;
}
.body-container.color .card .title span {
    position: relative;
    color: white;
    font-size: 20px;
    font-weight: bold;
    text-shadow: 0px 3px 3px rgba(0,0,0,.6);
}
.body-container.color .card .title .filter {
    position: absolute;
    top: 0px;
    left: 0px;
    width: 100%;
    height: 100%;
    background: rgba(0,0,0,.3);
    -webkit-transition: all .5s ease-in-out;
    -moz-transition: all .5s ease-in-out;
    -ms-transition: all .5s ease-in-out;
    -o-transition: all .5s ease-in-out;
    transition: all .5s ease-in-out;
}
.body-container.color ul li a {
    color: white;
}
.body-container .card ul {
    list-style-type: none;
    padding: 0px;
    margin: 0px;
}
.body-container.color .card ul li a {
    color: #1db79c;
}
.body-container ul li a:hover {
    text-decoration: underline;
}
.package-container {
    overflow: hidden;
    width: 100%;
    margin-top: 50px;
}
.package-container .package-list {
    width: 50%;
    background-color: #13a38a;
    background-repeat: no-repeat;
    background-position: center;
    background-size: 110%;
    display: inline-block;
    float: left;
    position: relative;
    -webkit-transition: all .5s ease-in-out;
    -moz-transition: all .5s ease-in-out;
    -ms-transition: all .5s ease-in-out;
    -o-transition: all .5s ease-in-out;
    transition: all .5s ease-in-out;
}
.package-container .package-list:hover {
    background-size: 120%;
}
.package-container .package-list .filter {
    position: absolute;
    top: 0px;
    left: 0px;
    width: 100%;
    height: 100%;
    background: rgba(0,0,0,0);
    -webkit-transition: all .5s ease-in-out;
    -moz-transition: all .5s ease-in-out;
    -ms-transition: all .5s ease-in-out;
    -o-transition: all .5s ease-in-out;
    transition: all .5s ease-in-out;
}
.package-container .package-list .text {
    background: rgba(0,0,0,.4);
    padding: 10px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    -ms-box-sizing: border-box;
    -o-box-sizing: border-box;
    box-sizing: border-box;
    color: white;
    text-align: center;
    font-weight: bold;
    position: absolute;
    width: 100%;
    left: 0px;
    top: 0px;
    -webkit-transition: all .5s ease-in-out;
    -moz-transition: all .5s ease-in-out;
    -ms-transition: all .5s ease-in-out;
    -o-transition: all .5s ease-in-out;
    transition: all .5s ease-in-out;
}
.package-container .package-list:hover .filter {
    background: rgba(0,0,0,.7);
}
.package-container .package-list:hover .text {
    background: rgba(0,0,0,0);
    font-size: 20px;
    top: 50%;
    margin-top: -15px;
    text-shadow: 0px 3px 10px rgba(0,0,0,1);
}
.icon-container {
    overflow: hidden;
    width: 100%;
}
.icon-container .icon-list {
    width: 25%;
    display: inline-block;
    float: left;
    position: relative;
    margin: 20px 0px;
    padding: 10px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    -ms-box-sizing: border-box;
    -o-box-sizing: border-box;
    box-sizing: border-box;
}
.icon-container .icon-list .icon {
    width: 60%;
    border-radius: 100%;
    background-color: #13a38a;
    background-repeat: no-repeat;
    background-position: center;
    background-size: 40% auto;
    margin: 0px auto;
    margin-bottom: 10px;
}
.icon-container .icon-list span {
    display: block;
    word-wrap: break-word;
}
table.form {
    width: 100%;
}
table.form tr td {
    padding: 10px;
}
input:focus, input:active, textarea:focus, textarea:active, select:focus, select:active {
    outline: none;
}
input {
    border: 0px;
    width: 100%;
    cursor: pointer;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    -ms-box-sizing: border-box;
    -o-box-sizing: border-box;
    box-sizing: border-box;
    border-radius: 5px;
    font-weight: bold;
    padding: 12px;
    font-family: 'allerregular';
    color: white;
    background: #1db79c;
    box-shadow: inset 0px 0px 10px rgba(0,0,0,.2);
    -webkit-transition: all .3s ease-in-out;
    -moz-transition: all .3s ease-in-out;
    -ms-transition: all .3s ease-in-out;
    -o-transition: all .3s ease-in-out;
    transition: all .3s ease-in-out;
}
input:hover, input:focus, input:active {
    box-shadow: inset 0px 0px 10px rgba(0,0,0,.4);
}
input[type="submit"] {
    text-shadow: 0px 2px 2px rgba(0,0,0,.4);
}
select {
    border: 0px;
    width: 100%;
    cursor: pointer;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    -ms-box-sizing: border-box;
    -o-box-sizing: border-box;
    box-sizing: border-box;
    border-radius: 5px;
    font-weight: bold;
    padding: 12px;
    font-family: 'allerregular';
    color: white;
    background: #1db79c;
    box-shadow: inset 0px 0px 10px rgba(0,0,0,.2);
    -webkit-transition: all .3s ease-in-out;
    -moz-transition: all .3s ease-in-out;
    -ms-transition: all .3s ease-in-out;
    -o-transition: all .3s ease-in-out;
    transition: all .3s ease-in-out;
}
textarea {
    cursor: pointer;
    resize: none;
    box-shadow: inset 0px 0px 10px rgba(0,0,0,.2);
    border-radius: 5px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    -ms-box-sizing: border-box;
    -o-box-sizing: border-box;
    box-sizing: border-box;
    width: 100%;
    border: 0px;
    height: 150px;
    padding: 12px;
    font-family: 'allerregular';
    color: white;
    background: #1db79c;
    box-sizing: border-box;
    -webkit-transition: all .3s ease-in-out;
    -moz-transition: all .3s ease-in-out;
    -ms-transition: all .3s ease-in-out;
    -o-transition: all .3s ease-in-out;
    transition: all .3s ease-in-out;
}
textarea:hover, textarea:active, textarea:focus {
    box-shadow: inset 0px 0px 10px rgba(0,0,0,.4);
}
#footer-container {
    width: 100%;
    padding-top: 30px;
    position: relative;
    background-color: #1db79c;
    color: #d9fcf6;
    position: relative;
}
#footer-container .photo {
    background-image: url(../images/skin/default/cover/4.jpg);
    background-position: center center;
    backgronud-repeat: no-repeat;
    background-size: cover;
    background-attachment: fixed;
    position: absolute;
    top: 0px;
    left: 0px;
    width: 100%;
    height: 100%;
    -webkit-filter: blur(5px);
    -moz-filter: blur(5px);
    -ms-filter: blur(5px);
    -o-filter: blur(5px);
    filter: blur(5px);
}
#footer-container .filter {
    position: absolute;
    top: 0px;
    left: 0px;
    width: 100%;
    height: 100%;
    background: rgba(0,0,0,.4);
}
#footer-container .wrapper {
    min-width 480px;
    max-width: 980px;
    margin: 0px auto;
    color: inherit;
    position: relative;
}
#footer-container h4 {
    color: #inherit;
}
#footer-container p {
    font-size: 14px;
    line-height: 180%;
}
#footer-container ul {
    list-style-type: none;
    padding: 0px;
    margin: 0px;
}
#footer-container ul li {
    margin-bottom: 8px;
}
#footer-container ul li a {
    color: inherit;
    font-size: 14px;
    -webkit-transition: all .3s ease-in-out;
    -moz-transition: all .3s ease-in-out;
    -ms-transition: all .3s ease-in-out;
    -o-transition: all .3s ease-in-out;
    transition: all .3s ease-in-out;
}
#footer-container ul li a:hover {
    color: #1db79c;
}
#footer-container hr {
    border: 0px;
    border-top: 1px solid rgba(255,255,255,.2);
}
#footer-container .copyright {
    font-size: 12px;
    padding-bottom: 20px;
}
#footer-container .copyright a {
    font-weight: bold;
}
.column-container {
    width: 100%;
    overflow: hidden;
}
.column-container [class^="col-"] {
    float: left;
    padding: 15px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    -ms-box-sizing: border-box;
    -o-box-sizing: border-box;
    box-sizing: border-box;
}
[class^="col-"].right {
    text-align: right;
}
[class^="col-"].center {
    text-align: center;
}
[class^="col-"].left {
    text-align: left;
}
.col-1 {
    width: 10%;
}
.col-2 {
    width: 20%;
}
.col-3 {
    width: 30%;
}
.col-4 {
    width: 40%;
}
.col-5 {
    width: 50%;
}
.col-6 {
    width: 60%;
}
.col-7 {
    width: 70%;
}
.col-8 {
    width: 80%;
}
.col-9 {
    width: 90%;
}
.col-10 {
    width: 100%;
}
.offset-1 {
    margin-left: 10%;
}
.offset-2 {
    margin-left: 20%;
}
.offset-3 {
    margin-left: 30%;
}
.offset-4 {
    margin-left: 40%;
}
.offset-5 {
    margin-left: 50%;
}
.offset-6 {
    margin-left: 60%;
}
.offset-7 {
    margin-left: 70%;
}
.offset-8 {
    margin-left: 80%;
}
.offset-9 {
    margin-left: 90%;
}
.social img {
    vertical-align: middle;
    display: inline-block;
    margin-right: 10px;
    width: 24px;
    float: left;
}
#container .full-cover .social {
    position: absolute;
    top: 120px;
    right: 30px;
    overflow: hidden;
}
#container .full-cover .social img {
    margin-right: 25px;
    width: 28px;
}
@media screen and (max-height: 360px) {
    .body-container [class^="col-"] {
        width: 100%;
    }
    .offset-1 {
        margin-left: 0%;
    }
    .offset-2 {
        margin-left: 0%;
    }
    .offset-3 {
        margin-left: 0%;
    }
    .offset-4 {
        margin-left: 0%;
    }
    .offset-5 {
        margin-left: 0%;
    }
    .offset-6 {
        margin-left: 0%;
    }
    .offset-7 {
        margin-left: 0%;
    }
    .offset-8 {
        margin-left: 0%;
    }
    .offset-9 {
        margin-left: 0%;
    }
    #footer-container .column-container:not(.copyright) [class^="col-"]:first-child {
        width: 100%;
    }
    #footer-container .column-container:not(.copyright) [class^="col-"]:not(:first-child) {
        width: 50%;
    }
    #footer-container .copyright [class^="col-"] {
        width: 100%;
        text-align: center;
    }
    .icon-container .icon-list {
        width: 50%;
    }
    .package-container .package-list {
        width: 100%;
    }
    #container .full-cover .text {
        font-size: 30px !important;
        bottom: 50px;
    }
} 
@media screen and (max-width: 850px) {
    #container .full-cover.small .text {
        display: none !important;
    }
}
<?php $useragent=$_SERVER['HTTP_USER_AGENT']; if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
?>
#action-bar {
    height: 100px !important;
}
#action-bar .logo {
    height: 80px !important;
    width: 80px !important;
    left: 50%;
    margin-left: -40px;
}
.full-cover.small .text {
    display: none;
}
#action-bar ul {
    position: absolute !important;
    top: 70px;
    background: white;
    border-radius: 10px;
    max-height: 0px;
}
#action-bar ul li {
    display: block;
    width: 100%;
}
#action-bar ul li a {
    color: #1db79c !important;
    padding: 15px !important;
}
.body-container [class^="col-"] {
    width: 100%;
}
.icon-container .icon-list {
    width: 50%;
}
.package-container .package-list {
    width: 100%;
}
.offset-1 {
    margin-left: 0%;
}
.offset-2 {
    margin-left: 0%;
}
.offset-3 {
    margin-left: 0%;
}
.offset-4 {
    margin-left: 0%;
}
.offset-5 {
    margin-left: 0%;
}
.offset-6 {
    margin-left: 0%;
}
.offset-7 {
    margin-left: 0%;
}
.offset-8 {
    margin-left: 0%;
}
.offset-9 {
    margin-left: 0%;
}
#footer-container .column-container:not(.copyright) [class^="col-"]:first-child {
    width: 100%;
}
#footer-container .column-container:not(.copyright) [class^="col-"]:not(:first-child) {
    width: 50%;
}
#footer-container .copyright [class^="col-"] {
    width: 100%;
    text-align: center;
}
#container .full-cover .text {
    font-size: 30px !important;
}
<?php
}
?>