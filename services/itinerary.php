<div class="body-container" id="bdyCon2" name="info" style="display: none;">
	<div class="wrapper">
		<div class="colulmn-container">
			<div class="col-5" id="lblInformation">
				<h1>Information</h1>
			</div>
			<div class="col-10 right">
				<a onclick="hideContainer('bdyCon2');" class="button">Dismiss</a>
			</div>
		</div>
	</div>
</div>
<div class="body-container color" name="haha">
	<div class="wrapper">
		<div class="column-container">
			<div class="col-7">
				<h1 class="center"><?php echo $tourname; ?> Package</h1><br>
				<?php
				$n = 1;
				$photo = 1;
				foreach($packages as $package) {
				?><div class="card">
					<div class="title" style="background-image: url(services/<?php echo $page; ?>/<?php echo $code; ?>/<?php 
					if(file_exists("services/$page/$code/$photo.jpg"))
						echo $photo; 
					else {
						$photo = 1;
						echo $photo;
					}
					$photo++;
					?>.jpg)">
						<div class="filter"></div>
						<span>Package <?php echo $n; ?><br>Code: <?php echo $package['code']; ?></span>
					</div>
					<div class="wrapper">
						<b><?php echo $package['daysnights']; ?></b><br>
						<?php
						for($i = 1; $i <= 15; $i++) {
							if(isset($package['day'.$i])) {
								echo '<br><b>Day '.$i.':</b><ul>';
								foreach($package['day'.$i] as $list) {
									echo '<li>'.$list.'</li>';
								}
								echo '</ul>';
							}
						}
						if(isset($package['inclusions'])) {
							echo '<br><b>Inclusions:</b><ul>';
							foreach($package['inclusions'] as $list) {
								echo '<li>'.$list.'</li>';
							}
							echo '</ul>';
						}
						if(isset($package['highlights'])) {
							echo '<br><b>Highlights:</b><ul>';
							foreach($package['highlights'] as $list) {
								echo '<li>'.$list.'</li>';
							}
							echo '</ul>';
						}
						if(isset($package['exclusions'])) {
							echo '<br><b>Exclusions:</b><ul>';
							foreach($package['exclusions'] as $list) {
								echo '<li>'.$list.'</li>';
							}
							echo '</ul>';
						}
						if(isset($package['optional'])) {
							echo '<br><b>Optional Tours:</b><ul>';
							foreach($package['optional'] as $list) {
								echo '<li>'.$list.'</li>';
							}
							echo '</ul>';
						}
						?>
						<center><a href="contact.php?code=<?php echo $package['code']; ?>&p=<?php echo $package['daysnights']; ?>" class="button orange">Get Package</a></center>
					</div>
				</div>
			<?php
				$n++;
			}
			?></div>
			<div class="col-3">
				<h1>Information</h1><br>
				<div class="card">
					<div class="wrapper">
					<ul>
						<li><a href="#info" onclick="showContainer('bdyCon2'); showInfo('making-your-booking');">Making your Booking</a></li>
						<li><a href="#info" onclick="showContainer('bdyCon2'); showInfo('travel-insurance');">Travel Insurance</a></li>
						<li><a href="#info" onclick="showContainer('bdyCon2'); showInfo('payment-system');">Payment System</a></li>
						<li><a href="#info" onclick="showContainer('bdyCon2'); showInfo('cancellation-of-bookings');">Cancellation of bookings</a></li>
					</ul>
					<br>	
					<a href="services.php?<?php echo $page; ?>" class="button block">Back to <?php echo $page;?></a>
					</div>
				</div>
				<div class="card">
					<div class="wrapper">
						<!-- Simple Currency Rates Table START -->
						<link rel="stylesheet" type="text/css" href="http://www.exchangerates.org.uk/widget/ER-SCRT2-css.php?w=180&nb=10&bdrc=E0E0E0&mbg=FFFFFF&fc=333333&tc=333333" media="screen" />
						<div id="erscrt2"><div id="erscrt2-widget"></div><div id="erscrt2-infolink"><a href="http://www.exchangerates.org.uk/US-Dollar-USD-currency-table.html" target="_new" ><img src='http://www.exchangerates.org.uk/widget/logo-333333.png' alt='ExchangeRates.org.uk'></a></div>
						<script type="text/javascript">	
						var tz = 'userset';
						var w = '180';
						var mc = 'USD';
						var nb = '10';
						var c1 = 'USD';
						var c2 = 'EUR';
						var c3 = 'AUD';
						var c4 = 'JPY';
						var c5 = 'INR';
						var c6 = 'CAD';
						var c7 = 'PHP';
						var c8 = 'NZD';
						var c9 = 'SGD';
						var c10 = 'CNY';
						var t = 'Live Exchange Rates';
						var tc = '333333';
						var bdrc = 'E0E0E0';
						var mbg = 'FFFFFF';
						var fc = '333333';

						var ccHost = (("https:" == document.location.protocol) ? "https://www." : "http://www.");
						document.write(unescape("%3Cscript src='" + ccHost + "exchangerates.org.uk/widget/ER-SCRT2-1.php' type='text/javascript'%3E%3C/script%3E"));
						</script>
						</div>
						<!-- Simple Currency Rates Table END -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>