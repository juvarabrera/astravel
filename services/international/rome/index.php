<?php
$tourname = "Rome";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "4FCOER",
		"daysnights" => "5 Days 4 Nights Experience Rome",
		"day1" => array(
			"Rome",
			"Meet and greet upon arrival and transfer to your Hotel. (Private Transfer). Check in Time is 1400hrs. Rest of the day is free at leisure. Overnight at Hotel."
		),
		"day2" => array(
			"Rome",
			"Breakfast at Hotel. <b>Discover Rome with Hop On Hop Off 48hours Ticket</b>. There is no better way to see Rome than on a double-decker, hop-on hop-off bus! See top Rome attractions on this comprehensive sightseeing tour, such as Vatican City, the Colosseum, Trevi Fountain, Tiber Island and much more. Layers of history have left the city with architectural treasures scattered at every twist and turn, and its Baroque piazzas and ancient antiquities are a joy to behold. Admire top city attractions, such as Rome’s must-see Colosseum - the scene of gory gladiator battles during the Roman Empire - and Piazza Venezia with its ostentatious Monument to Vittorio Emanuele II. Pass downtown streets near the eye-catching Trevi Fountain or perhaps hop off at Vatican City to visit one of the most sacred places in Christendom. Free at leisure. Overnight at Hotel"
		),
		"day3" => array(
			"Rome",
			"Breakfast at Hotel. <b>Continue with your Hop On Hop Off 48hours Ticket</b>. Free at own leisure. Overnight at Hotel"
		),
		"day4" => array(
			"Rome - Shopping Day at Castel Romano Designer Outlet",
			"Breakfast at Hotel, Make your own way to starting point of this tour and Discover a shopping paradise at <b>Castel Romano</b>, a luxurious outdoor mall just south of Rome, on this independent shopping tour! Meet your host near Rome Termini train station and then hop aboard your coach for the short journey south to <b>Castel Romano Designer Outlet</b>. Your host will have shown you where to catch your coach back from when dropping you off. Just return to that point at 1:45pm, 5pm or 8pm, and then board your coach for the trip back to Rome Termini station. If you travel between June and August, there's an additional departure at 8:30pm too. Overnight at Hotel"
		),
		"day5" => array(
			"Rome",
			"Breakfast at Hotel. Day free at leisure until departure transfer to Airport."
		),
		"inclusions" => array(
			"4 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Return Airport or Train Station Transfers",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program"
		)
	)
);
?>