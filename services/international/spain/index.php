<?php
$tourname = "Spain";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "5MADSS - R",
		"daysnights" => "6 Days 5 Nights Splendors of Spain",
		"day1" => array(
			"Madrid",
			"Meet and greet upon arrival and proceed to your Hotel on own. Check in Time is 1400hrs. Rest of the day is free at leisure. Overnight at Hotel."

		),
		"day2" => array(
			"Madrid - Seville",
			"Breakfast at Hotel. In the morning, make your own way to the starting point of this tour. Madrid Sightseeing Tour (SIC Basis). During the 2 hour tour, we will visit the oldest districts in Madrid: Austrias, Plaza Mayor, Retiro Park, and the Borbones area. After the tour, make your own way back to your Hotel. In the afternoon proceed on your own to train station to Seville. Arrive at Seville and make your own way to Hotel. Check into Hotel. Overnight at Hotel."
		),
		"day3" => array(
			"Seville - Granada - Seville",
			"Breakfast at Hotel. Depart for a Full Day Tour of Granada (SIC Basis). In this city, the Eurasian, Iberian, Phonetician, Roman, Arab and Christian cultures have left their imprint, making it one of the wonders of the world. Visit the Royal Palace, Alcazaba and the surrounding palaces, gardens, patios and towers. An outstanding feature are the Gardens of Generalife, which is considered as one of the most beautiful gardens in the world. After the visit free time for lunch (Not Included) and we will finally return to Seville. Overnight at Hotel."
		),
		"day4" => array(
			"Seville - Cordoba - Seville",
			"Breakfast at Hotel. You will go on a Full Day Tour of Cordoba (SIC Basis). We depart from Seville for the 1 and a half hour drive to Cordoba. When arriving in Cordoba, walk along the wonderful gardens of Alcazar of the Christian Kings. We will then visit the Mosque, once the crowning symbol of the Caliphate and now a Cathedral. Continue with a walk through the Jewish quarter, the old market (now the handicraft market) and the old Synagogue. Afterwards there will be free time for lunch (Not Included). Later, return to Seville. Overnight at Hotel."
		),
		"day5" => array(
			"Seville - Madrid",
			"Breakfast at Hotel. Start off with a half day Seville Classic Tour (SIC Basis). Learn about Seville's Architecture in 1920 and visit Plaza de Espana and the Cathedral. Then visit the Orange Tree Courtyard and the Giralda Tower which is the symbol of Seville. We will go to the Golden Tower where we will visit the City's Nautical History Exhibition. The tour will end with Santa Cruz Quarter, known for its orange trees and flower filled courtyards. In the afternoon, proceed on your own to train station. Arrive and proceed to your Hotel on own. Overnight at Hotel."
		),
		"day6" => array(
			"Madrid",
			"Breakfast at Hotel. Make your own way to Airport."
		),
		"inclusions" => array(
			"5 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Train travel between Madrid - Seville - Madrid on AVE Train",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program"
		)
	),
	array(
		"code" => "5MADSE",
		"daysnights" => "6 Days 5 Nights Spain Experience",
		"day1" => array(
			"Madrid",
			"Meet and greet upon arrival and transfer to your Hotel. (Private Transfer). Check in Time is 1400hrs. Rest of the day is free at leisure. Overnight at Hotel."
		),
		"day2" => array(
			"Madrid - Seville",
			"Breakfast at Hotel. In the morning, make your own way to the stating point of this tour. <b>Madrid Sightseeing Tour (SIC Basis)</b>. During the 2 hour tour, we will visit the oldest districts in Madrid: Austrias, Plaza Mayor, Retiro Park, and the Borbones area. Here you will get the opportunity to see the Royal Palace, Prado Museum, Cibeles fountain, Puerta de Alcalá and Isabel's. We will also visit some of the finest districts like Salamanca, Castellana and Ciudad Universitaria, Las Ventas Bull Ring and Santiago Bernabeu Stadium. Make your own way back to your Hotel. In the afternoon departure transfer to the train station to connect your <b>High Speed AVE train to Seville (Standard Train Travel Included)</b>. Train departs at 1500hrs and arrives in Seville at 1730hrs. Arrival transfer to your Hotel. Check into Hotel. Overnight at Hotel."
		),
		"day3" => array(
			"Seville - Granada - Seville",
			"Breakfast at Hotel. Depart for a <b>Full Day Tour of Granada (SIC Basis)</b>. In this city, the Eurasian, Iberian, Phonetician, Roman, Arab and Christian cultures have left their imprint, making it one of the wonders of the world. It was Spain's last Islamic Kingdom inhabited jointly by Muslims, Jews and Christians. The Alhambra is the main jewel of this beautiful city. Visit the Royal Palace, Alcazaba and the surrounding palaces, gardens, patios and towers. An outstanding feature are the Gardens of Generalife, which was the summer residence for the King. It is a combination of water springs, gardens and architecture of indescribable beauty. No surprise, it is considered as one of the most beautiful gardens in the world. After the visit to the Alhambra there will be free time for lunch (Not Included) and we will finally return to Seville. Overnight at Hotel."
		),
		"day4" => array(
			"Seville - Cordoba - Seville",
			"Breakfast at Hotel. Today you will go on a <b>Full Day Tour of Cordoba (SIC Basis)</b>. We depart from Seville for the 1 and a half hour drive to Cordoba. When arriving in Cordoba, walk along the wonderful gardens of Alcazar of the Christian Kings, where we will also see the mosaics that decorate the rooms. We will then visit the Mosque, once the crowning symbol of the Caliphate and now a Cathedral. Continue with a walk through the Jewish quarter, the old market (now the handicraft market) and the old Synagogue. Afterwards there will be free time for lunch (Not Included). Later, return to Seville. Overnight at Hotel."
		),
		"day5" => array(
			"Seville - Madrid",
			"Breakfast at Hotel. Start off with a half day <b>Seville Classic Tour (SIC Basis)</b>. Learn about Seville's Architecture in 1920 and visit Plaza de Espana and the Cathedral, which is the third largest temple in the world. We will continue our tour by visiting the Orange Tree Courtyard and the Giralda Tower which is the symbol of Seville. We will go from the Cathedral to the Golden Tower where we will visit the City's Nautical History Exhibition. The tour will end with Santa Cruz Quarter, known for its orange trees and flower filled courtyards. In the afternoon, depart to Madrid by <b>High Speed AVE train (Standard Train Travel Included)</b>. Arrive and transfer to your Hotel. Overnight at Hotel."
		),
		"day6" => array(
			"Madrid",
			"Breakfast at Hotel. Day free at leisure until departure transfer to Airport."
		),
		"inclusions" => array(
			"5 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Return Airport or Train Station Transfers in all cities",
			"Train travel between Madrid - Seville - Madrid on AVE Train",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program"
		)
	),
	array(
		"code" => "7MADGS",
		"daysnights" => "8 Days 7 Nights Glamour of Spain",
		"day1" => array(
			"Madrid",
			"Meet and greet upon arrival and transfer to your Hotel. Check into hotel by 1400hrs. Overnight at Hotel."
		),
		"day2" => array(
			"Madrid",
			"Breakfast at Hotel. In the morning, make your own way to the stating point of this tour. <b>Madrid Sightseeing Tour (SIC Basis)</b>. During this 2 hour tour, we will visit the oldest districts in Madrid; Austrias, Plaza Mayor, Retiro Park, and the Borbones area. Here you will see the Royal Palace, Prado Museum, Cibeles fountain, Puerta de Alcalá and Isabel's. We will also visit some of the finest districts like Salamanca, Castellana and Ciudad Universitaria, Las Ventas Bull Ring and Santiago Bernabeu Stadium. Make your own way back to your Hotel. In the afternoon, free at leisure. Overnight at Hotel"
		),
		"day3" => array(
			"Madrid - Seville",
			"Breakfast at Hotel, departure transfer to train station to connect your <b>High Speed AVE train to Seville</b>. (Standard Class Train Ticket Included). Arrival transfer to your Hotel. Check into Hotel. Overnight at Hotel."
		),
		"day4" => array(
			"Seville - Granada - Seville",
			"Breakfast at Hotel. Depart for a <b>Full Day Tour of Granada (SIC Basis)</b>. In this City the Eurasian, Iberian, Phonetician, Roman, Arab and Christian cultures have left their imprint, making it one of the wonders of the world. It was Spain' s last Islamic Kingdom inhabited jointly by Muslims, Jews and Christians. The Alhambra is the main jewel of this beautiful city. Visit the Royal Palace, Alcazaba and the surrounding palaces, gardens, patios and towers. An outstanding feature are the Gardens of Generalife, which was the summer residence for the King. It’s a combination of water springs, gardens and architecture of indescribable beauty. No surprise, it is considered as one of the most beautiful gardens in the world. After the visit to the Alhambra there will be free time for lunch (Not Included) and we will finally return to Seville. Overnight at Hotel."
		),
		"day5" => array(
			"Seville - Cordoba - Seville",
			"Breakfast at Hotel. Enjoy a <b>Full Day Tour of Cordoba (SIC Basis)</b>. We depart from Seville for the 1 and a half hour drive to Cordoba. When arriving in Cordoba, walk along the wonderful gardens of the Alcazar of the Christian Kings, where we will also see the mosaics that decorate the rooms. We will then visit the Mosque, once the crowning symbol of the Caliphate and now a Cathedral. Continue with a walk through the Jewish quarter, the old market (now the handicraft market) and the old Synagogue. Afterwards there will be free time for lunch (Not Included). Later return to Seville. Overnight at Hotel."
		),
		"day6" => array(
			"Seville - Barcelona",
			"Breakfast at Hotel. Start the day with a Half Day Seville Classic Tour (SIC Basis). Learn about Seville's 1920's Architecture and visit Plaza de Espana and the Cathedral, which is the third largest temple in the world. We will continue our tour by visiting the Orange Tree Courtyard and the Giralda Tower which is the symbol of Seville. We will go from the Cathedral to the Golden Tower where we will visit the City's Nautical History Exhibition. The tour will end with Santa Cruz Quarter, known for its orange trees and flower filled courtyards. In the afternoon, depart to Barcelona by <b>High Speed AVE train</b>. Arrive and transfer to your Hotel. Overnight at Hotel."
		),
		"day7" => array(
			"Barcelona",
			"Breakfast at Hotel. Make you own way to the start point for the tour at <b>Ronda Universidad, 5, Barcelona</b>, to join your <b>Half Day Guided City Tour of Barcelona (SIC Basis)</b>. Start your tour by driving to Plaça Catalunya, Via Laietana, the Cathedral and its cloister, the Gothic Quarter and Plaça Sant Jaume (Generalitat and Town Hall). We will continue on to the Vila Olimpica (athletes residential complex during the Olympics) and the Port Olimpic (Olympic Harbour), where we will go up the Montjuich Mountain, which offers a panoramic view of the City and the Port Harbour. We will tour the Stadium, Palu Sant Jordi and Calatrava Tower. Also on the agenda will be a visit to the Poble Espanyol (Spanish Village) which reproduces the most typical regional architectural style of Spain. At the same time, you will be able to taste the famous Catalan Cava. On our way back to Terminal we will make a stop at the famous Hard Rock Cafe. Tour ends here and you can make your own way back to your Hotel. Afternoon free at leisure. Overnight at Hotel."
		),
		"day8" => array(
			"Barcelona",
			"Breakfast at Hotel. Day free at leisure until departure transfer to Barcelona Airport."
		),
		"inclusions" => array(
			"7 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Return Airport or Train Station Transfers in all cities",
			"Train travel between Madrid - Seville - Barcelona on AVE Train",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program"
		)
	)
);
?>