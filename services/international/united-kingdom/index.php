<?php
$tourname = "United Kingdom";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "8LHRDU",
		"daysnights" => "9 Days 8 Nights Discover United Kingdom",
		"day1" => array(
			"London",
			"Arrival transfer at London Heathrow Airport to your Hotel. Check into Hotel by 1400hrs. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day2" => array(
			"London",
			"Breakfast at Hotel. <b>The Original London Sightseeing Tour: Hop-on Hop-off 24hours Ticket</b> - See all the top sights like the London Eye, Big Ben, Westminster Abbey, Tower Bridge and more. Redeem your voucher, and then ride around London aboard your spacious open-top double- decker bus. Free at own leisure. Overnight at Hotel."
		),
		"day3" => array(
			"London - Edinburgh",
			"Breakfast at Hotel, departure transfer to train station to connect your train to <b>Edinburgh (Standard Class Train Ticket Included)</b>. Arrive and transfer to your Hotel. Check into Hotel by 1400hrs. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day4" => array(
			"Edinburgh",
			"Breakfast at Hotel. <b>Hop On Hop Off Edinburgh Tour with 1 Day Ticket</b> - Make your own way to the starting point of the tour which is Hanover Street. . The tour will highlight all the things to do and see in Edinburgh as you tour through the city streets.Overnight at Hotel"
		),
		"day5" => array(
			"Edinburgh - Windermere (Lake District)",
			"Breakfast at Hotel. You have the morning free to explore Edinburgh with your unlimited ticket and then in the late afternoon departure transfer to Edinburgh Train Station to connect your <b>train to Lake District (Windermere) (Standard Class Train Ticket Included)</b>. Upon arrival transfer to your Hotel in Windermere. Overnight at Hotel."
		),
		"day6" => array(
			"Windermere",
			"Breakfast at Hotel. Day free at leisure to explore this beautiful city and its surroundings on your own. Overnight at Hotel."
		),
		"day7" => array(
			"Windermere - London",
			"Breakfast at Hotel. In the morning departure transfer to Windermere Train Station to connect to your <b>train to London (Standard Class Train Ticket Included)</b>. Upon arrival transfer to your Hotel in London. Check into Hotel by 1400hrs. Rest of the afternoon free at leisure. Overnight at Hotel."
		),
		"day8" => array(
			"London",
			"Breakfast at Hotel. Day free at leisure to explore London. Overnight at Hotel."
		),
		"day9" => array(
			"London",
			"Breakfast at Hotel. Day free at leisure until departure transfer to London Airport."
		),
		"inclusions" => array(
			"8 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Arrival & Departure Transfers in all cities",
			"Return Train Station or Airport Transfers in all cities",
			"Train Travel from London - Edinburgh - Windermere - London"
		)
	),
	array(
		"code" => "3LHRDL",
		"daysnights" => "4 Days 3 Nights Discover London",
		"day1" => array(
			"London",
			"Meet and greet upon arrival and transfer to your Hotel. (Private Transfer). Check in Hotel. Check in Time is 1400hrs. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day2" => array(
			"London",
			"Breakfast at Hotel. <b>The Original London Sightseeing Tour: Hop-on Hop-off 24hours Ticket</b> - See top London attractions at your own pace. See all the top sights like the London Eye, Big Ben, Westminster Abbey, Tower Bridge and more. Redeem your voucher, and then ride around London aboard your spacious open-top double- decker bus. Tour routes are great options for all the family, boasting entertaining commentary for both adults and kids plus children’s activity packs Overnight at Hotel."
		),
		"day3" => array(
			"London",
			"Breakfast at Hotel. Day free at leisure for shopping or optional sightseeing. The most popular destinations for shopping in London are Westfield Shopping Centre (Europe's largest Shopping Centre), Bicester Village Retail Park or Oxford Street. Overnight at Hotel."
		),
		"day4" => array(
			"London",
			"Breakfast at Hotel. Day free until departure transfer to Airport or Train Station."
		),
		"inclusions" => array(
			"3 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Return Airport Transfers",
			"Hop-on Hop-off 24hours Ticket"
		)
	)
);
?>