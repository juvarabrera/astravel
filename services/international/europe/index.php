<?php
$tourname = "Europe";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "6CDGGE - R",
		"daysnights" => "7 Days 6 Nights Glimpse of Europe",
		"day1" => array(
			"Paris",
			"Arrive and make your own way to your Hotel for check in by 1400hrs. Then you may start Discovering Paris at your leisure with 2 Days Hop-on Hop-off Tour. Get a bird's eye view of the cobbled streets of the Left Bank and the Napoleonic splendour of the Ecole Militaire. See the Eiffel Tower, Paris' most famous landmark. Stop off at the Arc de Triomphe and walk down the stylish Champs-Elysees. Become acquainted with some of the world's most famous artworks at the Louvre including the mysterious Mona Lisa. The round tour lasts approximately 2 hours and 15minutess with frequent daily departures. Your ticket will be valid for 2 consecutive days. Overnight at Hotel."
		),
		"day2" => array(
			"Paris",
			"Breakfast at Hotel. Continue with your 2nd Day of your Hop-on Hop-off Tour is the easiest way to get around town. Free at own leisure. Overnight at Hotel"
		),
		"day3" => array(
			"Paris - Amsterdam",
			"Breakfast at Hotel. Make your own way to Paris Train station to connect to your train to Amsterdam. Make your own way to your Hotel. Check into Hotel. Overnight at Hotel."
		),
		"day4" => array(
			"Amsterdam",
			"Breakfast at Hotel. Proceed on your own to Central Station where your tour will start. Discover the City with Hop On Hop Off Amsterdam Tour with 9 stops. The tour ticket includes a free Canal Cruise with the Blue Boat Company or Amsterdam Canal Cruises and a free coffee at the Diamond Museum. Marvel at the original works of the famous Dutch artists in the renowned Van Gogh Museum. Contemplate the heart-rending history of the Jewish community with a visit to the home of the diarist Anne Frank. There are plenty of museums to explore including Amsterdam's Historisch Museum, Rembrandt House Museum, Madame Tussauds, the Diamond Museum and the Jewish Historical Museum. Other must-see attractions include the Amsterdam Dungeon, the Royal Palace, and the Madame Tussauds Tour. Ticket is valid for 24hours. Overnight at Hotel."
		),
		"day5" => array(
			"Amsterdam - Brussels - London",
			"Breakfast at Hotel. Make your own way to Amsterdam train station to connect your train to Brussels. Then connect to a Eurostar Train to London. Make your own way to your Hotel. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day6" => array(
			"London",
			"Breakfast at Hotel. The Original London Sightseeing Tour: Hop-on Hop-off 24hours Ticket - See top London attractions at your own pace on a comprehensive hop-on hop-off sightseeing tour! See all the top sights like the London Eye, Big Ben, Westminster Abbey, Tower Bridge and more. Free at own leisure. Overnight at Hotel."
		),
		"day7" => array(
			"London",
			"Breakfast at Hotel. Day free at leisure make your own way to London Heathrow Airport."
		),
		"inclusions" => array(
			"6 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Train Travel between Paris - Amsterdam - Brussels",
			"2nd Class Eurostar Brussels - London",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program"
		)
	),
	array(
		"code" => "4VIEEE",
		"daysnights" => "5 Days 4 Nights Surprising Eastern Europe",
		"day1" => array(
			"Vienna",
			"Meet and greet upon arrival and transfer to your Hotel. (Private Transfer). Check into Hotel. Check in Time is 1400hrs. Rest of the day is free at leisure. Overnight at Hotel."
		),
		"day2" => array(
			"Vienna",
			"Breakfast at Hotel. You will be picked up from your Hotel. Proceed for <b>Grand City Tour of Vienna with Palace of SCHOBRUNN</b>. You will get a first impression of the most important sights of Vienna. The famous Ringstrasse, in Vienna's main boulevard. MAK, Austrian Museum of Applied Arts, State Opera House, Museums of Fine Arts and Natural History. The beautiful Parliament, National Theatre, City Hall, University, Votive Church, Urania, Belvedere Palace and an inside tour of the Schonbrunn Palace. Tour ends at Opera. Later, make your own way back to your Hotel. Afternoon free at leisure. Overnight at Hotel."
		),
		"day3" => array(
			"Vienna - Budapest - Vienna",
			"Breakfast at Hotel. Today you will go on a <b>Full Day Tour of Budapest - Hungary (SIC Basis)</b> <i>(Pick up from most Hotels in Vienna City)</i>. Visit the Paris of the East through the Burgenland to the Border, Hegyeshalom, Gyor. Upon arrival in Budapest you will go on a City Tour of Budapest seeing the Mathias Church, Fisherman's Bastion, House of Parliament, Heroes Square and many more sights. Later return to Vienna. Overnight at Hotel."
		),
		"day4" => array(
			"Vienna - Prague - Vienna",
			"Breakfast at Hotel. Enjoy a <b>Full Day Prague Tour - (SIC Basis)</b> After a drive through the characteristic landscape of Moravia, you reach Prague. Afterwards drive to Hradschin (Royal Palace). In the afternoon we take you on an extensive City tour, showing you the wonderful panorama of the City from Charles Bridge. We will also get the chance to visit the National Theatre, the elegant residences on the Moldavien River banks, the Jewish cemetery (individual visit possible), the Parisian Street, the old Town Square and Wencels Square. There will be certainly enough time for shopping or exploring the old quarters of Prague. Later, return to Vienna. You will arrive back in Vienna by 2100hrs. Overnight at Hotel."
		),
		"day5" => array(
			"Vienna",
			"Breakfast at Hotel. Day free at leisure until departure transfer to Airport."
		),
		"inclusions" => array(
			"4 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Return Airport or Train Station Transfers",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program"
		)
	),
	array(
		"code" => "6CDGGE",
		"daysnights" => "7 Days 6 Nights Glance of Europe",
		"day1" => array(
			"Paris",
			"Meet and greet upon arrival and transfer to Hotel for check in by l400hrs. Arrival transfer to your Hotel in Paris. Check into Hotel by 1400hrs. Then you may start Discovering Paris at your leisure with <b>2 Days Hop-on Hop-off Tour</b>. See the Eiffel Tower, Paris' most famous landmark. Stop off at the Arc de Triomphe and walk down the stylish Champs-Elysees. Become acquainted with some of the world's most famous artworks at the Louvre including the mysterious Mona Lisa. Your ticket will be valid for 2 consecutive days. Overnight at Hotel."
		),
		"day2" => array(
			"Paris",
			"Breakfast at Hotel. Continue with your 2nd Day of your Hop-on Hop-off Tour is the easiest way to get around town. You can hop-on and off at any of the nine stops as often as you like over a one or two day period. The open- top double-decker bus provides fantastic views from the top floor. Buses depart every 10 minutes in summer and every 20 minutes in winter, every day of the year. Recorded commentary is available onboard in French, English, Spanish, German, Italian, Japanese, Russian and Chinese. You will receive headphones for the commentary the first time you use your ticket. Overnight at Hotel"
		),
		"day3" => array(
			"Paris - Amsterdam",
			"Breakfast at Hotel. Prepare for your departure transfer to Paris Train station to connect to your <b>train to Amsterdam (Standard Class Train Ticket Included to Amsterdam)</b>. Meet upon arrival and transfer to your Hotel. Check into Hotel. Overnight at Hotel."
		),
		"day4" => array(
			"Amsterdam",
			"Breakfast at Hotel. Proceed on your own to Central Station where your tour will start. Discover the City with <b>Hop On Hop Off Amsterdam Tour with 9 stops</b>, located about the City. The tour ticket includes a free Canal Cruise with the Blue Boat Company or Amsterdam Canal Cruises and a free coffee at the Diamond Museum. Marvel at the original works of the famous Dutch artists in the renowned Van Gogh Museum. Contemplate the heart-rending history of the Jewish community with a visit to the home of the diarist Anne Frank. There are plenty of museums to explore including Amsterdam's Historisch Museum, Rembrandt House Museum, Madame Tussauds, the Diamond Museum and the Jewish Historical Museum. Other must-see attractions include the Amsterdam Dungeon, the Royal Palace, and the Madame Tussauds Tour. This starts from Central Station every 30 minutes. You could take a loop of the whole tour in 60 minutes or stop at any stops and continue later. Ticket is valid for 24hours. Overnight at Hotel."
		),
		"day5" => array(
			"Amsterdam - Brussels - London",
			"Breakfast at Hotel. A departure transfer to Amsterdam train station to connect your train to Brussels (Standard Class Train Ticket Included). Then connect to an Eurostar Train to London (Standard Class Train Ticket Included). Meet and greet upon arrival and transfer to your Hotel. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day6" => array(
			"London",
			"Breakfast at Hotel. <b>The Original London Sightseeing Tour: Hop-on Hop-off 24hours Ticket</b>. See all the top sights like the London Eye, Big Ben, Westminster Abbey, Tower Bridge and more. Redeem your voucher, and then ride around London aboard your spacious open-top double- decker bus. Overnight at Hotel."
		),
		"day7" => array(
			"London",
			"Breakfast at Hotel. Day free at leisure until departure transfer to London Heathrow Airport."
		),
		"inclusions" => array(
			"6 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Return Airport or Train Station Transfers in all cities",
			"Train Travel between Paris - Amsterdam - Brussels",
			"2nd Class Eurostar Brussels - London",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program"
		)
	),
	array(
		"code" => "8VIEBE",
		"daysnights" => "9 Days 8 Nights Best of Eastern Europe",
		"day1" => array(
			"Vienna",
			"Meet upon arrival and transfer to your Hotel. Check into Hotel by 1400hrs. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day2" => array(
			"Vienna",
			"Breakfast at Hotel. You will be picked up from your Hotel. Proceed for <b>G rand City Tour with Palace Of Schönbrunn</b>. You will get a first impression of the most important sights of Vienna. The famous Ringstrasse, Vienna's main boulevard where you can view the MAK, Austrian Museum of Applied Arts, State Opera House, Museums of Fine Arts and Natural History. As well as the Parliament, National Theatre, City Hall, University, Votive Church and Urania. We will take an inside tour of the Schonbrunn Palace and observe the Belvedere Palace. Tour ends at Opera. Later make your own way back to your Hotel. Afternoon free at leisure. Overnight at Hotel."
		),
		"day3" => array(
			"Vienna - Salzburg - Vienna",
			"Breakfast at Hotel. You will be picked up from your Hotel for your <b>Full Day Tour of Salzburg - SIC Basis</b>. This tour will take you to the Salzkammergut Lake District and to Salzburg which is the capital of festivals towards the west to the town of Gmunden, the impressive mountain region of the Lake District. After arrival in Salzburg, you will have some free time for lunch and shopping before you start your Guided Tour of Salzburg. You will see the Fortress Hohensalzburg, Cathedral, Festival Halls Mozarteum, Getreidegasse, Mozart's Birthplace, Mirabell Gardens and many other sights. Later return to Vienna. Overnight at Hotel."
		),
		"day4" => array(
			"Vienna - Prague",
			"Breakfast at Hotel. Check out of Hotel. Departure Transfer to the Train Station. <b>Train to Prague (Standard Class Ticket Included)</b>. Travel time around 5 hours. Meet upon arrival and transfer to your Hotel. Check into Hotel. Overnight at Hotel."
		),
		"day5" => array(
			"Prague",
			"Breakfast at Hotel. Make your own way to the meeting point of the tour. In the morning enjoy a <b>Grand City Tour of Prague (SIC Basis)</b>. During this tour you will be given all the basic information about Golden Prague's historic monuments like the State Opera, the National Museum, Wenceslas Square, the Charles Square and Prague Castle. There is a walk around the Castle courtyards where there are other things to do. You will also have the opportunity to see St. Vitus Cathedral. The bus will then take you across the Vltava to the Old Town Square, with its astronomical clock (HORLOGE) and apostles. Here the tour ends. Make your own way back to your Hotel. Afternoon free at leisure. Overnight at Hotel."
		),
		"day6" => array(
			"Prague - Budapest",
			"Breakfast at Hotel. Check out and transfer to train station. <b>Travel to Budapest (Standard Class Train Included)</b>. Travel time around 7 hours. Arrive in Budapest and transfer to your Hotel. Check into Hotel. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day7" => array(
			"Budapest",
			"Breakfast at Hotel. Wait at Hotel lobby for pick up for your sightseeing tour. Proceed for your City Tour of Budapest (SIC Basis) . We start the tour from the Chain Bridge, pass by the Parliament and take the Margaret Bridge to drive over to the Buda side. We stop in the Castle District and during a short walk we show you the main attractions of the area. We walk to Matthias Church and to the Fishermen’s Bastion from where you can enjoy the beautiful panorama. Next, we drive to the Gellért Hill (Citadel) and show you the most spectacular view of Budapest. We then take you over the Elisabeth Bridge to Pest and show you the famous Central Market Hall and the largest Synagogue of Europe, after which we drive to the City Park. We pass by Europe’s largest Thermal Spa, the Budapest Zoo and the Amusement Park, then stop at the Heroes Square, which display statues of the most famous Hungarian Kings and Dukes. We then take the Andrássy Avenue to down town Pest, pass by the Opera House and the St Stephen’s Basilica. During the tour we offer a brief overview of the last 1000 years of Hungarian history. The Tour ends in the City Centre. Make your own way back to your Hotel. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day8" => array(
			"Budapest - Vienna",
			"Breakfast at Hotel. Check out of Hotel and transfer to train station to connect your train to Vienna (Standard Class Train Ticket Included) . Travel time around 3 and a half hours. Upon arrival in Vienna transfer to your Hotel. Check into Hotel by 1400hrs. Rest of the afternoon free at leisure. Overnight at Hotel."
		),
		"day9" => array(
			"Vienna",
			"Breakfast at Hotel. Check out of Hotel and departure transfer to Vienna Airport."
		),
		"inclusions" => array(
			"8 Nights Accommodation",
			"Continental Breakfast Daily",
			"Arrival & Departure Transfers in all cities",
			"Return Train Station or Airport Transfers in all Cities",
			"Train Travel from Vienna - Prague - Budapest - Vienna"
		)
	),
	array(
		"code" => "8LHRSE",
		"daysnights" => "9 Days 8 Nights Splendors of Europe",
		"day1" => array(
			"London",
			"Meet upon arrival at London Heathrow Airport and transfer to your Hotel. Check into Hotel. Overnight at Hotel."
		),
		"day2" => array(
			"London",
			"Breakfast at Hotel. <b>The Original London Sightseeing Tour: Hop-on Hop-off 24hours Ticket</b> - See all the top sights like the London Eye, Big Ben, Westminster Abbey, Tower Bridge and more. Redeem your voucher, and then ride around London aboard your spacious open-top double- decker bus. Free at own leisure. Overnight at Hotel."
		),
		"day3" => array(
			"London - Brussels",
			"Breakfast at Hotel. Prepare for a departure transfer to St Pancras Eurostar Station to connect to your <b>Eurostar Train to Brussels (Standard Train Travel Ticket Included)</b>. Meet upon arrival in Brussels and transfer to your Hotel. Check into Hotel by 1400hrs. Rest of the afternoon free at leisure. Overnight at Hotel."
		),
		"day4" => array(
			"Brussels - Amsterdam",
			"Breakfast at Hotel. Discover all the things to do in Brussels from the top deck of an open top bus on your <b>Brussels Hop On Hop Off Tour</b>. This tour has 13 Hop on Hop off stops conveniently located around the City. You can travel to points of interest and the City's attractions at your leisure. Brussels is home to the European Union with the European Parliament, Manneken Pis, Cinquantenaire Arch, Art Nouveau and Victor Horta, the Grand-Place and the Atomium. The City abounds in architectural monuments, from the Renaissance to modern times and discovering this heritage is full of surprises. Make your own way back to your Hotel. At around 2pm you will be transferred to Brussels Train Station to connect to your <b>train to Amsterdam (Standard Class Train Ticket Included to Amsterdam)</b>. Meet upon arrival and transfer to your Hotel. Check in Hotel. Overnight at Hotel."
		),
		"day5" => array(
			"Amsterdam",
			"Breakfast at Hotel. Proceed on your own to Central Station where your tour will start. Discover the City with <b>Hop On Hop Off Amsterdam Tour with 9 stops</b>, located about the City. The tour ticket includes a free Canal Cruise with the Blue Boat Company or Amsterdam Canal Cruises and a free coffee at the Diamond Museum. Marvel at the original works of the famous Dutch artist in the renowned Van Gogh Museum. Contemplate the heart-rending history of the Jewish community with a visit to the home of the diarist Anne Frank. There are plenty of museums to explore including the Amsterdam's Historisch Museum, Rembrandt House Museum, Madame Tussauds, the Diamond Museum and the Jewish Historical Museum. Other must-see attractions include the Amsterdam Dungeon and the Royal Palace. The Madame Tussauds Tour starts from Central Station every 30 minutes. You could take a loop of the whole tour in 60 minutes or stop at any destination and continue later. Ticket is valid for 24 hours. Overnight at Hotel."
		),
		"day6" => array(
			"Amsterdam - Paris",
			"Breakfast at Hotel. Today departure transfer to <b>Amsterdam Train Station</b>. Train travel to Paris via Brussels. You will have to change trains in <b>Brussels (Standard Class Train Ticket Included)</b>. Arrive in Paris where you are met and transferred to your Hotel. Check into Hotel by 1400hrs. You can enjoy your Paris tour, <b>Paris at your Leisure Tour on an Open Top Bus</b> today. Discover the main sites of Paris in an original and relaxed way. The round tour lasts approximately 2 hours and 15 minutes with frequent daily departures. You may break your tour at any of our nine stops and continue later. You can stop at these stops and also shop. A multi-lingual system is available on all buses allowing you to hear commentaries in one of the 8 languages. Your ticket will be valid for 2 consecutive days. Overnight at Hotel."
		),
		"day7" => array(
			"Paris",
			"Breakfast at Hotel. Discover Paris at your leisure <b>1 Day Hop-on Hop-off Tour</b>. See the Eiffel Tower, Paris' most famous landmark. Stop off at the Arc de Triomphe and walk down the stylish Champs-Elysees. Become acquainted with some of the world's most famous artworks at the Louvre including the mysterious Mona LisaYour ticket will be valid for 2 consecutive days. Overnight at Hotel."
		),
		"day8" => array(
			"Paris - London",
			"Breakfast at Hotel. Prepare for your departure transfer to Eurostar Train Station to connect to your <b>Eurostar Train to London</b>. Meet and transfer upon arrival at London St Pancras Station to your Hotel. Check into Hotel by 1400hrs. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day9" => array(
			"London",
			"Breakfast at Hotel. Day free until departure transfer to London Heathrow Airport. Check out of Hotel by 1100hrs."
		),
		"inclusions" => array(
			"8 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Return Airport or Train Station Transfers in all cities",
			"Train Travel between London - Brussels - Amsterdam - Paris - London",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program"
		)
	),
	array(
		"code" => "10CDGAE",
		"daysnights" => "11 Days 10 Nights Affordable Europe",
		"day1" => array(
			"Paris",
			"Meet and greet upon arrival and transfer to Hotel for check in by l400hrs. Discover Paris at your leisure <b>2 Days Hop-on Hop-off Tour</b>. See the Eiffel Tower, Paris' most famous landmark. Stop off at the Arc de Triomphe and walk down the stylish Champs-Elysees. Become acquainted with some of the world's most famous artworks at the Louvre including the mysterious Mona Lisa. Discover the main sites of Paris in an original and relaxed way. The round tour lasts approximately 2 hours and 15minutess with frequent daily departures. You may break your tour at any of our nine stops and continue later. You can stop at this stops and also shop. A multi-lingual system is available on all buses allowing you to hear commentaries in one of the 8 languages. Your ticket will be valid for 2 consecutive days. Overnight at Hotel."
		),
		"day2" => array(
			"Paris",
			"Breakfast at Hotel. Continue with your 2nd Day of your Hop-on Hop-off Tour is the easiest way to get around town. You can hop-on and off at any of the nine stops as often as you like over a one or two day period. The open- top double-decker bus provides fantastic views from the top floor. Buses depart every 10 minutes in summer and every 20 minutes in winter, every day of the year. Recorded commentary is available onboard in French, English, Spanish, German, Italian, Japanese, Russian and Chinese. You will receive headphones for the commentary the first time you use your ticket. Overnight at Hotel"
		),
		"day3" => array(
			"Paris - Zurich",
			"Breakfast at Hotel. Take the departure transfer from Hotel to train station for your <b>TGV train to Zurich (Standard Class Train Ticket Included)</b>. Arrive in Zurich and transfer to your Hotel. Check into Hotel by 1400hrs. Rest of the afternoon free at leisure. Overnight at Hotel."
		),
		"day4" => array(
			"Zurich - Mt Titlis - Lucerne - Zurich",
			"Breakfast at Hotel. Proceed to the starting point of this tour at Zurich Bus Station. <b>Full Day Tour of Mt Titlis and Lucerne (SIC Basis)</b>. An adventure excursion to the highest vantage point in Central Switzerland. A pleasant ride with our deluxe motor coach takes you to the mountain resort of Engelberg in Central Switzerland. Here you will board the aerial cable way for a 45 minute journey from the green valley bottom to the high alpine glacier region at 3020m/10000 ft which guarantees snow the whole year round. Enjoy a magnificent ride in the world's first ever revolving 'ROTAIR' gondola, which offers a 360° panoramic view into the Alps. During the 2 hours stay on the mountain visit the Ice Grotto, experience the new Ice Flyer. A chair lift ride over the glacier with fantastic views into crevasses and ice falls and enjoy a snow slide on the 'Fun lift' (These activities are included in our tour). After the descent to Engelberg we board our coach for a drive to Lucerne. Time at leisure approximately 1 hour for sightseeing and shopping before the coach takes you back to Zurich. Overnight at Hotel."
		),
		"day5" => array(
			"Zurich - Rhine Falls - City Tour - Amsterdam",
			"Breakfast at Hotel, Proceed to the starting point of this tour at Zurich Bus Station. <b>Full Day Tour of Zurich and Rhine Falls (SIC Basis)</b>. Travel by air-conditioned coach to Northern Switzerland to see the beautiful <b>Rhine Falls - Europe's largest waterfall</b>. Capture stunning photographs of the cascading water and natural beauty of the area, The tour concludes at Zurich Sihlquai where you will have approximately 30 minutes to spend at your leisure before your join the next tour Zurich West including the Lindt Chocolate Factory Outlet tour. See the fun side of Zurich on this unique tour to Zurich West and the Lindt Chocolate Factory outlet. Delve into the district’s rich history, and learn about its transformation from industrial district to epicenter of cool and quirky culture! See the Prime Tower (Switzerland's tallest building), before shopping for indulgent Swiss chocolates at the world-famous Lindt Chocolate Factory outlet shop. Lindt has been producing some of Switzerland’s finest chocolates for over 260 years. You will have time to spend at your leisure here, so take your time to browse the array of chocolates at the outlet shop. After satisfying your shopping fix, you will travel back to Zurich Sihlquai Bus Terminal, where your tour concludes.Make your own way back to your hote. In the evening departure transfer to Zurich Train Station to connect to your overnight train to <b>Amsterdam (2nd Class Train Ticket included with 2 berth Sleepers). Overnight on Train.</b>"
		),
		"day6" => array(
			"Amsterdam",
			"Breakfast on your own. Arrive in the morning at Amsterdam Central Station and you will be met and transferred to your hotel. Leave your baggage at reception (Check in Hotel is 1400hrs) and you are free to explore Amsterdam today with your <b>Hop On Hop Off Amsterdam Tour with 9 stops</b>, located about the City. The tour ticket includes a free canal cruise with the Blue Boat Company or Amsterdam Canal Cruises and a free coffee at the Diamond Museum. Marvel at the original works of famous Dutch artists in the renowned Van Gogh Museum. Contemplate the heart-rending history of the Jewish community with a visit to the home of the diarist Anne Frank. There are plenty of museums to explore including the Amsterdam's Historisch Museum, Rembrandt House Museum, Madame Tussauds, the Diamond Museum and the Jewish Historical Museum. Other must-see attractions include the Amsterdam Dungeon, the Royal Palace, and the Madame Tussauds Tour starts from Central Station every 30mins. You could take a loop of the whole tour in 60 minutes or stop at any stops and continue later. Ticket is valid for 24 hours. Overnight at Hotel."
		),
		"day7" => array(
			"Amsterdam - Brussels",
			"Breakfast at Hotel. Prepare for your departure transfer to the <b>Amsterdam Train Station to connect to Brussels (Standard Class Train Ticket Included)</b>. Meet and transfer to your Hotel. Check into Hotel by 1400hrs. Later make your own way to the starting point of the tour. Discover all the things to do in Brussels from the top deck of an open top bus on your <b>Brussels Hop On Hop Off Tour</b>. This tour has 13 Hop on Hop off stops conveniently located around the City. You can travel to points of interest and the City's attractions at your leisure. Brussels is home to the European Union with the European Parliament, Manneken Pis, Cinquantenaire Arch, Art Nouveau and Victor Horta, the Grand- Place and the Atomium. The City abounds in architectural monuments, from the Renaissance, to modern times. Make your own way back to your Hotel. Overnight at Hotel."
		),
		"day8" => array(
			"Brussels - London",
			"Breakfast at Hotel. Discover all the things to do in Brussels from the top deck of an open top bus on your <b>Brussels Hop On Hop Off Tour</b>. This tour has 13 Hop on Hop off stops conveniently located around the City. You can travel to points of interest and the City's attractions at your leisure. Brussels is home to the European Union with the European Parliament, Manneken Pis, Cinquantenaire Arch, Art Nouveau and Victor Horta, the Grand-Place and the Atomium. The City abounds in architectural monuments, from the Renaissance to modern times. Make your own way back to your Hotel and at around 2pm you will be transferred to Brussels Train Station to connect to your <b>Eurostar train to London (Standard Class Train Ticket Included)</b>. Meet upon arrival and transfer to your Hotel. Check into Hotel. Overnight at Hotel."
		),
		"day9" => array(
			"London",
			"Breakfast at Hotel. <b>The Original London Sightseeing Tour: Hop-on Hop-off 24hours Ticket</b> - See all the top sights like the London Eye, Big Ben, Westminster Abbey, Tower Bridge and more. Redeem your voucher, and then ride around London aboard your spacious open-top double- decker bus. Overnight at Hotel."
		),
		"day10" => array(
			"London",
			"Breakfast at Hotel. Day free at leisure for shopping or optional sightseeing. The most popular destinations for shopping in London are Westfield Shopping Centre (Europe's largest Shopping Centre), Bicester Village Retail Park or Oxford Street. Overnight at Hotel."
		),
		"day11" => array(
			"London",
			"Breakfast at Hotel. Day free until departure transfer to Airport or Train Station."
		),
		"inclusions" => array(
			"10 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Return Airport or Train Station Transfers in all Cities",
			"Train travel between Paris - Zurich - Amsterdam - Brussels",
			"Overnight Train - Zurich to Amsterdam (2 in a Berth Sleeper)",
			"Eurostar Train Travel between Brussels - London",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program"
		)
	),
	array(
		"code" => "10FCOSE",
		"daysnights" => "11 Days 10 Nights Schengen Europe",
		"day1" => array(
			"Rome",
			"Meet and greet upon arrival and proceed directly to your Hotel for check in by 1400hrs. Then start with <b>Discovering Rome with Hop On Hop Off 48hours Ticket</b>. There is no better way to see Rome than on a double-decker, hop-on hop-off bus! See top Rome attractions on this comprehensive sightseeing tour, such as Vatican City, the Colosseum, Trevi Fountain, Tiber Island and much more. Rome has more to see than most other capitals all put together. Layers of history have left the city with architectural treasures scattered at every twist and turn, and its Baroque piazzas and ancient antiquities are a joy to behold.. Aboard a comfortable, open-top, double-decker bus, admire top city attractions, such as Rome’s must-see Colosseum - the scene of gory gladiator battles during the Roman Empire - and Piazza Venezia with its ostentatious Monument to Vittorio Emanuele II. Pass downtown streets near the eye-catching Trevi Fountain or perhaps hop off at Vatican City to visit one of the most sacred places in Christendom. With your ticket, you can remain on the bus for an entire loop as you listen to the informative audio commentary, or you can hop on and off at any of the eight stops around the city to explore Rome’s countless delights. Overnight at Hotel"
		),
		"day2" => array(
			"Rome - Florence",
			"Breakfast at Hotel. Continue with <b>Discovering Rome with Hop On Hop Off 48hours Ticket</b>. Free at own leisure. In the afternoon around 2pm departure transfer to Railway Station to connect your <b>train to Florence (Standard Class Train Travel Included)</b>. Meet upon arrival and transfer to your Hotel. The rest of the day you are free to enjoy Florence. Overnight at Hotel."
		),
		"day3" => array(
			"Florence - Venice",
			"Breakfast at Hotel. Today take the <b>Hop On Hop Off Tour of Florence</b>. Explore Florence with 2 routes on this Hop On Hop Off tour. In the romantic region of Tuscany lies the jewel City of the Renaissance, Florence. Surrounded by the Appenine Mountain chain, it lies in a fertile valley surrounded by the charming Tuscan vineyards and olive groves. The art galleries and museums of Florence are renowned throughout the world and holidaymakers flock to admire the superb David of Michelangelo, the Venus of Botticelli and the stunning Duomo with its marble façade which glows in the golden sunlight. The start point for Line A is S M Novella Station and for Line B is S. Frediano. Later in the afternoon at around 2pm departure transfer to Florence Railway Station to connect your <b>train to Venice St Lucia Station (Standard Class Train Travel Included)</b>. Arrive in Venice and you will be transferred to your Hotel. Overnight at Hotel."
		),
		"day4" => array(
			"Venice",
			"Breakfast at Hotel. In the morning make your own way to the Tourist Office, Plaza San Marcos by 0900hrs. Take a <b>Guided Walking Tour of Venice (SIC Basis)</b>. This interesting tour is centred around the history of St. Mark's Square and its main monuments. Visit the St. Mark's Basilica with its marble and mosaics and the Ducal Palace. This is where the old Republic of Venice exercised their political and commercial powers in the Doge Palace. Cross the Bridge of Sighs arriving at the famous Venice Jail. Finishing off the tour is the opportunity to visit the glass factory on the island of Murano (The visit to Murano is optional and not included in the Walking Tour). The rest of the afternoon free to explore Venice. Overnight at Hotel."
		),
		"day5" => array(
			"Venice - Milan",
			"Breakfast at Hotel. Departure transfer to train station to connect <b>train to Milan (Standard Class Train Travel Included)</b>. Arrive and transfer to your Hotel by afternoon. Check into Hotel by 1400hrs. Later make your own way to the starting point of the <b>Hop On Hop Off Milan Tour</b> which offers two routes with one ticket. Enjoy a panoramic view of the City on an open top double-decker red Bus. With two lines and 12 stops, both lines touch the most popular sights of Milan which include the magnificent Gothic Cathedral of Duomo, the world famous La Scala Opera House and the beautiful fortress of the Castello Sforzesco. Overnight at Hotel."
		),
		"day6" => array(
			"Milan - Zurich",
			"Breakfast at Hotel. Today in the morning continue with your <b>Hop On Hop Off Milan Tour</b> which offers two routes with one ticket. With its panoramic open top double-decker red buses, this tour offers you the opportunity to discover this busy and attractive City from a preferential point of view. With two lines and 12 stops, both lines touch the most popular sights of Milan. Which include the magnificent Gothic Cathedral of Duomo, the world famous La Scala Opera House and the beautiful fortress of the Castello Sforzesco. Later departure transfer in the afternoon to the train station to connect your <b>train to Zurich (Standard Class Train Ticket Included)</b>. Arrival transfer to your Hotel. Overnight at Hotel."
		),
		"day7" => array(
			"Zurich - Mt Titlis - Lucerne - Zurich",
			"Breakfast at Hotel. Proceed to the starting point of this tour at Zurich Bus Station. <b>Full Day Tour of Mt Titlis and Lucerne (SIC Basis)</b>. An adventure excursion to the highest vantage point in Central Switzerland. A pleasant ride with our deluxe motor coach takes you to the mountain resort of Engelberg in Central Switzerland. Here you will board the aerial cableway for a 45 minute journey from the green valley bottom to the high alpine glacier region at 3020m /10000 ft which guarantees snow the whole year round. Enjoy a magnificent ride in the world's first ever revolving 'ROTAIR' gondola, which offers a 360° panoramic view into the Alps. During the 2 hour stay on the mountain visit the Ice Grotto and experience the new Ice Flyer. This is a chair lift ride over the glacier with fantastic views into crevasses and ice falls. Also enjoy a snow slide on the 'Fun lift' (These activities are included in our tour). After the descent to Engelberg we board our coach for a drive to Lucerne, time at leisure approximately 1 hour for sightseeing and shopping before the coach takes you back to Zurich. Overnight at Hotel."
		),
		"day8" => array(
			"Zurich - Paris",
			"Breakfast at Hotel. Later will be a departure transfer to Zurich Train Station to connect to your <b>train to Paris (Standard Class Train Ticket Included)</b>. Meet upon arrival in Paris Train Station and transfer to your Hotel. Check into Hotel by 1400hrs. Rest of the afternoon free at leisure. Overnight at Hotel."
		),
		"day9" => array(
			"Paris",
			"Breakfast at Hotel. Discover Paris at your leisure <b>2 Days Hop-on Hop-off Tour</b>. See the Eiffel Tower, Paris' most famous landmark. Stop off at the Arc de Triomphe and walk down the stylish Champs-Elysees. Become acquainted with some of the world's most famous artworks at the Louvre including the mysterious Mona Lisa. Your ticket will be valid for 2 consecutive days. Overnight at Hotel."
		),
		"day10" => array(
			"Paris",
			"Breakfast at Hotel. Continue with your 2nd Day of your Hop-on Hop-off Tour is the easiest way to get around town. You can hop-on and off at any of the nine stops as often as you like over a one or two day period. The open- top double-decker bus provides fantastic views from the top floor. Buses depart every 10 minutes in summer and every 20 minutes in winter, every day of the year. Recorded commentary is available onboard in French, English, Spanish, German, Italian, Japanese, Russian and Chinese. You will receive headphones for the commentary the first time you use your ticket. Overnight at Hotel"
		),
		"day11" => array(
			"Paris",
			"Breakfast at Hotel. Day free until departure transfer to Airport or Train Station."
		),
		"inclusions" => array(
			"10 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Return Airport or Train Station Transfers in all cities",
			"Train travel between Rome - Florence - Venice - Milan - Zurich",
			"Train travel between Zurich - Paris",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program"
		)
	),
	array(
		"code" => "12FCOED",
		"daysnights" => "13 Days 12 Nights European Delight",
		"day1" => array(
			"Rome",
			"Meet and greet upon arrival and proceed directly to your Hotel for check in by 1400hrs. Then start with <b>Discovering Rome with Hop On Hop Off 48hours Ticket</b>. There is no better way to see Rome than on a double-decker, hop-on hop-off bus! See top Rome attractions on this comprehensive sightseeing tour, such as Vatican City, the Colosseum, Trevi Fountain, Tiber Island and much more. Free at own leisure. Overnight at Hotel"
		),
		"day2" => array(
			"Rome - Florence",
			"Breakfast at Hotel. Continue with <b>Discovering Rome with Hop On Hop Off 48hours Ticket</b>. Free at leisure. In the afternoon around 2pm departure transfer to Railway Station to connect your <b>train to Florence (Standard Class Train Travel Included)</b>. Meet upon arrival and transfer to your Hotel. The rest of the day you are free to enjoy Florence. Overnight at Hotel."
		),
		"day3" => array(
			"Florence - Venice",
			"Breakfast at Hotel. Today take the <b>Hop On Hop Off Tour of Florence</b>. Explore Florence with 2 routes on this Hop On Hop Off tour. In the romantic region of Tuscany lies the jewel City of the Renaissance, Florence. Surrounded by the Appenine Mountain chain, it lies in a fertile valley surrounded by the charming Tuscan vineyards and olive groves. The art galleries and museums of Florence are renowned throughout the world and holidaymakers flock to admire the superb David of Michelangelo, the Venus of Botticelli and the stunning Duomo with its marble façade which glows in the golden sunlight. The start point for Line A is S M Novella Station and for Line B is S. Frediano. Later in the afternoon at around 2pm departure transfer to Florence Railway Station to connect your <b>train to Venice St Lucia Station (Standard Class Train Travel Included)</b>. Arrive in Venice and you will be transferred to your Hotel. Overnight at Hotel."
		),
		"day4" => array(
			"Venice",
			"Breakfast at Hotel. In the morning make your own way to the Tourist Office, Plaza San Marcos by 0900hrs. Take a <b>Guided Walking Tour of Venice (SIC Basis)</b>. This interesting tour is centred around the history of St. Mark's Square and its main monuments. Visit the St. Mark's Basilica with its marble and mosaics and the Ducal Palace. This is where the old Republic of Venice exercised their political and commercial powers in the Doge Palace. Cross the Bridge of Sighs arriving at the famous Venice Jail. The rest of the afternoon free to explore Venice. Overnight at Hotel."
		),
		"day5" => array(
			"Venice - Milan",
			"Breakfast at Hotel. Departure transfer to train station to connect <b>train to Milan (Standard Class Train Travel Included)</b>. Arrive and transfer to your Hotel by afternoon. Check into Hotel by 1400hrs. Later make your own way to the starting point of the <b>Hop On Hop Off Milan Tour</b> which offers two routes with one ticket. Enjoy a panoramic view of the City on an open top double-decker red Bus. With two lines and 12 stops, both lines touch the most popular sights of Milan which include the magnificent Gothic Cathedral of Duomo, the world famous La Scala Opera House and the beautiful fortress of the Castello Sforzesco. Overnight at Hotel."
		),
		"day6" => array(
			"Milan - Zurich",
			"Breakfast at Hotel. Today in the morning continue with your <b>Hop On Hop Off Milan Tour</b> which offers two routes with one ticket. With its panoramic open top double-decker red buses, this tour offers you the opportunity to discover this busy and attractive City from a preferential point of view. With two lines and 12 stops, both lines touch the most popular sights of Milan. Which include the magnificent Gothic Cathedral of Duomo, the world famous La Scala Opera House and the beautiful fortress of the Castello Sforzesco. Later departure transfer in the afternoon to the train station to connect your <b>train to Zurich (Standard Class Train Ticket Included)</b>. Arrival transfer to your Hotel. Overnight at Hotel."
		),
		"day7" => array(
			"Zurich - Mt Titlis - Lucerne - Zurich",
			"Breakfast at Hotel. Proceed to the starting point of this tour at Zurich Bus Station. <b>Full Day Tour of Mt Titlis and Lucerne (SIC Basis)</b>. An adventure excursion to the highest vantage point in Central Switzerland. A pleasant ride with our deluxe motor coach takes you to the mountain resort of Engelberg in Central Switzerland. Here you will board the aerial cableway for a 45 minute journey from the green valley bottom to the high alpine glacier region at 3020m /10000 ft which guarantees snow the whole year round. Enjoy a magnificent ride in the world's first ever revolving 'ROTAIR' gondola, which offers a 360° panoramic view into the Alps. During the 2 hour stay on the mountain visit the Ice Grotto and experience the new Ice Flyer. This is a chair lift ride over the glacier with fantastic views into crevasses and ice falls. Also enjoy a snow slide on the 'Fun lift' (These activities are included in our tour). After the descent to Engelberg we board our coach for a drive to Lucerne, time at leisure approximately 1 hour for sightseeing and shopping before the coach takes you back to Zurich. Overnight at Hotel."
		),
		"day8" => array(
			"Zurich - Paris",
			"Breakfast at Hotel. Later will be a departure transfer to Zurich Train Station to connect to your <b>train to Paris (Standard Class Train Ticket Included)</b>. Meet upon arrival in Paris Train Station and transfer to your Hotel. Check into Hotel by 1400hrs. Rest of the afternoon free at leisure. Overnight at Hotel."
		),
		"day9" => array(
			"Paris",
			"Breakfast at Hotel. Discover Paris at your leisure <b>2 Days Hop-on Hop-off Tour</b>. See the Eiffel Tower, Paris' most famous landmark. Stop off at the Arc de Triomphe and walk down the stylish Champs-Elysees. Become acquainted with some of the world's most famous artworks at the Louvre including the mysterious Mona Lisa. Discover the main sites of Paris in an original and relaxed way. The round tour lasts approximately 2 hours and 15minutess with frequent daily departures. You may break your tour at any of our nine stops and continue later. You can stop at this stops and also shop. A multi-lingual system is available on all buses allowing you to hear commentaries in one of the 8 languages. Your ticket will be valid for 2 consecutive days. Overnight at Hotel."
		),
		"day10" => array(
			"Paris",
			"Breakfast at Hotel. Continue with your 2nd Day of your Hop-on Hop-off Tour is the easiest way to get around town. You can hop-on and off at any of the nine stops as often as you like over a one or two day period. The open- top double-decker bus provides fantastic views from the top floor. Buses depart every 10 minutes in summer and every 20 minutes in winter, every day of the year. Recorded commentary is available onboard in French, English, Spanish, German, Italian, Japanese, Russian and Chinese. You will receive headphones for the commentary the first time you use your ticket. Overnight at Hotel"
		),
		"day11" => array(
			"Paris - London",
			"Breakfast at Hotel. Prepare for a departure transfer to Eurostar Train Station to connect to your <b>Eurostar train to London (Standard Class Train Travel Included)</b>. Meet and transfer upon arrival at London St Pancras Station to your Hotel. Check into Hotel by 1400hrs. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day12" => array(
			"London",
			"Breakfast at Hotel. <b>The Original London Sightseeing Tour: Hop-on Hop-off 24hours Ticket</b>. See all the top sights like the London Eye, Big Ben, Westminster Abbey, Tower Bridge and more. Redeem your voucher, and then ride around London aboard your spacious open-top double- decker bus. Overnight at Hotel."
		),
		"day13" => array(
			"London",
			"Breakfast at Hotel. Day free until departure transfer to Airport or Train Station."
		),
		"inclusions" => array(
			"12 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Return Airport or Train Station Transfers in all cities",
			"Train travel between Rome - Florence - Venice - Milan - Zurich",
			"Train travel between Zurich - Paris - London",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program"
		)
	),
	array(
		"code" => "13FCOGE",
		"daysnights" => "14 Days 13 Nights Grand Europe",
		"day1" => array(
			"Rome",
			"Meet and greet upon arrival and proceed directly to your Hotel for check in by 1400hrs. Then start with <b>Discovering Rome with Hop On Hop Off 48hours Ticket</b>. There is no better way to see Rome than on a double-decker, hop-on hop-off bus! See top Rome attractions on this comprehensive sightseeing tour, such as Vatican City, the Colosseum, Trevi Fountain, Tiber Island and much more. Free at own leisure. Overnight at Hotel"
		),
		"day2" => array(
			"Rome - Florence",
			"Breakfast at Hotel. Continue with <b>Discovering Rome with Hop On Hop Off 48hours Ticket</b>. Free at own leisure. In the afternoon around 2pm departure transfer to Railway Station to connect your <b>train to Florence (Standard Class Train Travel Included)</b>. Meet upon arrival and transfer to your Hotel. The rest of the day you are free to enjoy Florence. Overnight at Hotel."
		),
		"day3" => array(
			"Florence - Venice",
			"Breakfast at Hotel. Today take the <b>Hop On Hop Off Tour of Florence</b>. Explore Florence with 2 routes on this Hop On Hop Off tour. In the romantic region of Tuscany lies the jewel City of the Renaissance, Florence. Surrounded by the Appenine Mountain chain, it lies in a fertile valley surrounded by the charming Tuscan vineyards and olive groves. The art galleries and museums of Florence are renowned throughout the world and holidaymakers flock to admire the superb David of Michelangelo, the Venus of Botticelli and the stunning Duomo with its marble façade which glows in the golden sunlight. The start point for Line A is S M Novella Station and for Line B is S. Frediano. Later in the afternoon at around 2pm departure transfer to Florence Railway Station to connect your <b>train to Venice St Lucia Station (Standard Class Train Travel Included)</b>. Arrive in Venice and you will be transferred to your Hotel. Overnight at Hotel."
		),
		"day4" => array(
			"Venice",
			"Breakfast at Hotel. In the morning make your own way to the Tourist Office, Plaza San Marcos by 0900hrs. Take a <b>Guided Walking Tour of Venice (SIC Basis)</b>. This interesting tour is centred around the history of St. Mark's Square and its main monuments. Visit the St. Mark's Basilica with its marble and mosaics and the Ducal Palace. This is where the old Republic of Venice exercised their political and commercial powers in the Doge Palace. Cross the Bridge of Sighs arriving at the famous Venice Jail. Finishing off the tour is the opportunity to visit the glass factory on the island of Murano (The visit to Murano is optional and not included in the Walking Tour). The rest of the afternoon free to explore Venice. Overnight at Hotel."
		),
		"day5" => array(
			"Venice - Innsbruck",
			"Breakfast at Hotel. Departure transfer to train station to connect <b>train to Innsbruck (Standard Train Ticket Included)</b>. Arrive and transfer to your Hotel. Check into Hotel. Free to just explore this breathtaking town in the Alps, visit the Crystal Factory Showroom or just relax. Overnight at Hotel."
		),
		"day6" => array(
			"Innsbruck - Zurich",
			"Breakfast at Hotel. Departure transfer to train station to connect your <b>train to Zurich (Standard Train Travel Included)</b>. Meet upon arrival at Zurich Train Station and transfer to your Hotel. Check into Hotel by 1400hrs. Rest of the day is free at leisure. Overnight at Hotel."
		),
		"day7" => array(
			"Zurich - Mt Titlis - Lucerne - Zurich",
			"Breakfast at Hotel. Proceed to the starting point of this tour at Zurich Bus Station. <b>Full Day Tour of Mt Titlis and Lucerne (SIC Basis)</b>. An adventure excursion to the highest vantage point in Central Switzerland. A pleasant ride with our deluxe motor coach takes you to the mountain resort of Engelberg in Central Switzerland, where you will board the aerial cable way for a 45 minute journey from the green valley bottom to the high alpine glacier region at 3020m/10000 ft which guarantees snow the whole year round. Enjoy a magnificent ride in the world's first ever revolving 'ROTAIR' gondola, which offers a 360° panoramic view into the Alps. During the 2 hour stay on the mountain visit the Ice Grotto and experience the new Ice Flyer. This is a chair lift ride over the glacier with fantastic views into crevasses and ice falls. Also enjoy a snow slide on the 'Fun lift' (These activities are included in our tour). After the descent to Engelberg we board our coach for a drive to Lucerne. Time at leisure approximately 1 hour for sightseeing and shopping before the coach takes you back to Zurich."
		),
		"day8" => array(
			"Zurich - Frankfurt",
			"Breakfast at Hotel. Departure transfer to Zurich Train Station to connect to your <b>train to Frankfurt (Standard Class Train Ticket Included)</b>. Meet and transfer to your Hotel. Check in Hotel. Make your own way to the starting point of your <b>Hop On Hop Off Frankfurt City Tour</b>. Frankfurt is located on the River Main and is the financial capital of Continental Europe and the Transportation Centre of Germany. Frankfurt is home of the European Central Bank and the German Stock Exchange. Additionally, it hosts some of the world's most important trade shows, such as the Frankfurt Auto Show and the Frankfurt Book Fair. The City is known for its futuristic skyline and the biggest airport in Germany. At the City Centre, one finds our start point at St. Paul´s Church/Paulskirche, the historical Römer, Frankfurt Cathedral and the Goethe-House all within close proximity to one another, yet still within sight of the modern banking district. The tour lasts for 1 hour. Later return to your Hotel on your own. Overnight at Hotel."
		),
		"day9" => array(
			"Frankfurt - Amsterdam",
			"Breakfast at Hotel. Departure transfer to Frankfurt Train Station to connect to your <b>train to Amsterdam (Standard Class Train Ticket Included)</b>. Meet and transfer to your Hotel. Check in Hotel. Proceed on your own to Central Station where you tour will start. Later Discover the City with <b>Hop On Hop Off Amsterdam Tour with 9 stops</b>, located about the City. The tour ticket includes a free canal cruise with the Blue Boat Company or Amsterdam Canal Cruises and a free coffee at the Diamond Museum. Marvel at the original works of famous Dutch artists in the renowned Van Gogh Museum. Contemplate the heart- rending history of the Jewish community with a visit to the home of the diarist Anne Frank. There are plenty of museums to explore including the Amsterdam's Historisch Museum, Rembrandt House Museum, Madame Tussauds, the Diamond Museum and the Jewish Historical Museum. Other must-see attractions include the Amsterdam Dungeon, the Royal Palace, and the Madame Tussauds Tour starts from Central Station every 30 minutes. You could take a loop of the whole tour in 60 minutes or stop at any destination and continue later. Ticket is valid for 24hours. Overnight at Hotel."
		),
		"day10" => array(
			"Amsterdam - Brussels",
			"Breakfast at Hotel. Departure transfer to the Amsterdam <b>Train Station to connect to Brussels (Standard Class Train Ticket Included)</b>. Meet and transfer to your Hotel. Check into Hotel by 1400hrs. Later make your own way to the starting point of the tour. Discover all the things to do in Brussels from the top deck of an open top bus on your <b>Brussels Hop On Hop Off Tour</b>. This tour has 13 Hop on Hop off stops conveniently located around the City. You can travel to points of interest and the City's attractions at your leisure. Brussels is home to the European Union with the European Parliament, Manneken Pis, Cinquantenaire Arch, Art Nouveau and Victor Horta, the Grand-Place and the Atomium. The city abounds in architectural monuments, from the Renaissance, to modern times. Make your own way back to your Hotel. Overnight at Hotel."
		),
		"day11" => array(
			"Brussels - Paris",
			"Breakfast at Hotel. Discover all the things to do in Brussels from the top deck of an open top bus on your <b>Brussels Hop On Hop Off Tour</b>. This tour has 13 Hop on Hop off stops conveniently located around the City. You can travel to points of interest and the City's attractions at your leisure. Brussels is home to the European Union with the European Parliament, Manneken Pis, Cinquantenaire Arch, Art Nouveau and Victor Horta, the Grand-Place and the Atomium. The city abounds in architectural monuments, from the Renaissance to modern times. Make your own way back to your Hotel and in the afternoon at around 2pm you will be transferred to Brussels Train Station to connect to your <b>train to Paris (Standard Class Train Ticket Included)</b>. Meet upon arrival and transfer to your Hotel. Check into Hotel. Overnight at Hotel."
		),
		"day12" => array(
			"Paris",
			"Breakfast at Hotel. Discover Paris at your leisure <b>2 Days Hop-on Hop-off Tour</b>. See the Eiffel Tower, Paris' most famous landmark. Stop off at the Arc de Triomphe and walk down the stylish Champs-Elysees. Become acquainted with some of the world's most famous artworks at the Louvre including the mysterious Mona Lisa. Your ticket will be valid for 2 consecutive days. Overnight at Hotel."
		),
		"day13" => array(
			"Paris",
			"Breakfast at Hotel. Continue with your 2nd Day of your Hop-on Hop-off Tour is the easiest way to get around town. You can hop-on and off at any of the nine stops as often as you like over a one or two day period. You will receive headphones for the commentary the first time you use your ticket. Free at own leisure. Overnight at Hotel"
		),
		"day14" => array(
			"Paris",
			"Breakfast at Hotel. Day free until departure transfer to Airport or Train Station."
		),
		"inclusions" => array(
			"13 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Return Airport or Train Station Transfers in all cities",
			"Train travel between Rome - Florence - Venice - Milan - Zurich",
			"Train travel between Zurich - Innsbruck - Frankfurt - Amsterdam - Brussels - Paris",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program",
			"Government Taxes and Service Charges"
		)
	)
);
?>