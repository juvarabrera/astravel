<?php
$tourname = "Turkey";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "9ISTTP",
		"daysnights" => "10 Days 9 Nights Turkey Package!",
		"day1" => array(
			"Arrival in Istanbul",
			"Meet and greet upon arrival to Istanbul",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Istanbul - Edirne - Ecebat - Tekirdag - Dardanalles(by ferry) - Canakkale  (B/L/D)",
			"Breakfast at your hotel.",
			"Proceed to Edirne - Selimiye Mosque - Sultan bayezit II Kulliye (Complex) and health Museum. Overnight at your hotel."
		),
		"day3" => array(
			"Cannakale - Troy - Pergamon - Kusadasi (B/L/D)",
			"Breakfast at your hotel.",
			"Visit Ancient Troy - Pergamon- Asclepion - Kusadasi. Overnight at your hotel."
		),
		"day4" => array(
			"Kusadasi - Pamukkale (B/L/D)",
			"Breakfast at your hotel.",
			"Drive to Ephesus. Visit and celebrate mass at the House of the Virgin. After lunch proceed to Pamukkale. Overnight at your hotel."
		),
		"day5" => array(
			"Pamukalle - Konya (B/L/D)",
			"Breakfast at your hotel.",
			"Visit Hierapolis and cotton shaped travertines of Pamukkkale. Drive to Konya then visit Mevlana Mausoleum."
		),
		"day6" => array(
			"Konya - Cappadocia (B/L/D)",
			"Breakfast at your hotel.",
			"Visit Valley of the Birds, proceed to Goreme Open Air Museum. Overnight at your hotel."
		),
		"day7" => array(
			"Cappadocia - Ankara (B/L/D)",
			"Breakfast at your hotel.",
			"Visit Underground City - local pottery and carpet shows.",
			"Drive to Ankara.  Overnight at your hotel."
		),
		"day8" => array(
			"Ankara - Istanbul (B/L/D)",
			"Breakfast at your hotel.",
			"Visit Mausoleum of Kemal Ataturk.",
			"Drive to Istanbul. Overnight at your hotel."
		),
		"day9" => array(
			"Istanbul (B/L/D)",
			"Breakfast at your hotel.",
			"Visit Ancient Hippodrome - Blue Mosque - St. Sophia.",
			"Visit Topkapi Palace - Grand Bazaar. Overnight at your hotel."
		),
		"day10" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Istanbul",
			"Round trip airport transfers ",
			"Nine nights hotel accommodation",
			"Full Board Meals",
			"Licensed professional local tour guide in English language throughout Turkey.",
			"Entrance fees to all museums, parks, and historic sites as marked on itinerary."
		)
	),
	array(
		"code" => "7ISTHT",
		"daysnights" => "8 Days 7 Nights Turkey Package!<br><br>Istanbul - Canakkale - Kusadasi(Ephesus) - Pamukkale - Cappadocia - Istanbul",
		"inclusions" => array(
			"Meet and greet services upon arrival at Istanbul",
			"Round trip airport transfers ",
			"Nine nights hotel accommodation",
			"Full Board Meals",
			"Licensed professional local tour guide in English language throughout Turkey.",
			"Entrance fees to all museums, parks, and historic sites as marked on itinerary."
		)
	),
	array(
		"code" => "5ISTWE",
		"daysnights" => "6 Days 5 Nights Turkey Package!<br><br>West Turkey and Edirine",
		"inclusions" => array(
			"Meet and greet services upon arrival at Istanbul",
			"Round trip airport transfers ",
			"Nine nights hotel accommodation",
			"Full Board Meals",
			"Licensed professional local tour guide in English language throughout Turkey.",
			"Entrance fees to all museums, parks, and historic sites as marked on itinerary."
		)
	),
	array(
		"code" => "7ISTHT",
		"daysnights" => "9 Days 8 Nights Turkey Package<br><br>Istanbul - Canakkale - Pergamon - Kusadasi (Ephesus) - Pamukkale - Konya - Cappadocia - Ankara - Bolu - Istanbul",
		"inclusions" => array(
			"Meet and greet services upon arrival at Istanbul",
			"Round trip airport transfers ",
			"Nine nights hotel accommodation",
			"Full Board Meals",
			"Licensed professional local tour guide in English language throughout Turkey.",
			"Entrance fees to all museums, parks, and historic sites as marked on itinerary."
		)
	)
);
?>