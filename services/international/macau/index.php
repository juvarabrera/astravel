<?php
$tourname = "Macau";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2MFMFE",
		"daysnights" => "3 Days 2 Nights Macau Free and Easy Package!",
		"day1" => array(
			"Arrival in Macau ",
			"Meet and greet upon arrival to Macau",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Macau (B/L)",
			"Breakfast at your hotel.",
			"Macau City tour. Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"-	Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Two nights hotel accommodation",
			"Daily Breakfast, One Lunch",
			"Macau City tour"
		)
	)
);
?>