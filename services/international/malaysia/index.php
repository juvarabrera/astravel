<?php
$tourname = "Malaysia";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2KULKZ",
		"daysnights" => "3 Days and 2 Nights Kuala Lumpur Kidzania Package!",
		"day1" => array(
			"Arrival in Kuala Lumpur",
			"Meet and greet upon arrival to Kuala Lumpur.",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Kuala lumpur (B/L/D)",
			"Breakfast at your hotel.",
			"Kidzania - I-City",
			"Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Kuala Lumpur",
			"Round trip airport transfers ",
			"Two nights hotel accommodation",
			"Daily Breakfast",
			"Kidzania entrance ticket (inc. 1x lunch voucher)",
			"A certificate of attendance will be awarded to the kids that will participate at KidZania program",
			"Round tour at I-City for lights experience only",
			"English speaking guide"
		)
	),
	array(
		"code" => "3KULGH",
		"daysnights" => "4 Days 3 Nights Kuala Lumpur and Genting Highlands Package!",
		"day1" => array(
			"Arrival in Kuala Lumpur Airport",
			"Meet and greet upon arrival to Kuala Lumpur.",
			"Transfer to the hotel of your choice for check in.",
			"Overnight at your hotel."
		),
		"day2" => array(
			"Kuala Lumpur (B)",
			"Breakfast at your hotel.",
			"Kuala Lumpur half day tour. ",
			"Free at own leisure. Overnight at your hotel."
		),
		"day3" => array(
			"Kuala Lumpur - Genting highlands (B)",
			"Breakfast at your hotel.",
			"Depart to Genting highlands Indoor and Outdoor theme park ride.",
			"Free at own leisure. Overnight at your hotel."
		),
		"day4" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Kuala Lumpur",
			"Round trip airport transfers ",
			"Two nights hotel accommodation in Kuala Lumpur",
			"Two nights hotel accommodation in Genting Highlands",
			"Daily breakfast",
			"Half day city Kuala Lumpur city tour (SIC basis)",
			"All transfers from Kuala Lumpur - Genting Highlands (SIC basis)"
		)
	),
	array(
		"code" => "4KULKP",
		"daysnights" => "5 Days 4 Nights Kuala Lumpur & Penang Package!",
		"day1" => array(
			"Arrival in Kuala Lumpur",
			"Meet and greet upon arrival to Kuala Lumpur ",
			"Transfer to the hotel of your choice for check in.",
			"Proceed to city of lights (I-City Shah Alam) (Red carpet + Trick art museum + Space mission) + Millions LCD Digital Lights.",
			"Overnight at your hotel."
		),
		"day2" => array(
			"Kuala Lumpur - (Domestic flight/SIC Express Bus / Private Car) - Penang (B)",
			"Breakfast at your hotel.",
			"Kuala Lumpur City Tour. Transfer to airport/bus terminal. ",
			"Overnight at your hotel in Penang."
		),
		"day3" => array(
			"Penang (B/L)",
			"Breakfast at your hotel.",
			"Full day Penang City Tour. Overnight at your hotel."
		),
		"day4" => array(
			"Penang - Kuala Lumpur (B)",
			"Breakfast at your hotel.",
			"Transfer to airport/bus terminal. ",
			"Free at your own leisure upon arriving to Kuala Lumpur. Overnight at your hotel."
		),
		"day5" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Kuala Lumpur / Penang",
			"Round trip airport transfers ",
			"Two nights hotel accommodation in Kuala Lumpur",
			"Two nights hotel accommodation in Penang",
			"Daily Breakfast, one lunch in Penang",
			"I-City Entrance ticket to Red Carpet / Trick Art Museum / Space Mission",
			"Kuala Lumpur City tour + Penang City Tour(SIC basis)"
		),
		"exclusions" => array(
			"KL - Penang - KL mode of transportation",
			"Personal expenses & tipping for driver"
		)
	),
	array(
		"code" => "4KULKPL",
		"daysnights" => "6 Days 5 Nights Kuala Lumpur + Penang + Langkawi Package!",
		"day1" => array(
			"Arrival in Kuala Lumpur",
			"Meet and greet upon arrival to Kuala Lumpur",
			"Transfer to the hotel of your choice for check in.",
			"Proceed to city of lights (I-City Shah Alam) (Red carpet + Trick art museum + Space mission) + Millions LCD Digital Lights.",
			"Overnight at your hotel."
		),
		"day2" => array(
			"Kuala Lumpur - (Domestic flight/SIC Express Bus / Private Car) - Penang (B)",
			"Breakfast at your hotel.",
			"Kuala Lumpur City Tour + Shopping. Transfer to airport/bus terminal.",
			"Overnight at your hotel in Penang."
		),
		"day3" => array(
			"Penang (B/L)",
			"Breakfast at your hotel.",
			"Full day Penang City Tour. Overnight at your hotel."
		),
		"day4" => array(
			"Penang - Langkawi ",
			"Breakfast at your hotel.",
			"Transfer to ferry terminal and proceed to Langkawi. ",
			"Langkawi Tour. Overnight at your hotel."
		),
		"day5" => array(
			"Langkawi - Evening Flight (B)",
			"Breakfast at your hotel.",
			"Transfer to airport for departure flight back to Kuala Lumpur.",
			"Transfer to Hotel for check in",
			"Overnight at your hotel in Kuala Lumpur."
		),
		"day6" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Kuala Lumpur / Penang",
			"Round trip airport transfers ",
			"Two nights hotel accommodation in Kuala Lumpur",
			"Two nights hotel accommodation in Penang",
			"One night hotel accommodation in Langkawi",
			"Daily Breakfast, One lunch at Penang, One lunch at Langkawi",
			"I-City Entrance ticket to Red Carpet / Trick Art Museum / Space Mission",
			"Kuala Lumpur City tour + Penang City Tour + Langkawi City Tour (SIC basis)"
		)
	),
	array(
		"code" => "3KULKL",
		"daysnights" => "4 Days 3 Nights Kuala Lumpur and Legoland Package!",
		"day1" => array(
			"Arrival in Kuala Lumpur",
			"Meet and greet upon arrival to Kuala Lumpur",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Kuala Lumpur (B)",
			"Breakfast at your hotel.",
			"Kuala Lumpur city tour and shopping. Overnight at your hotel."
		),
		"day3" => array(
			"Kuala Lumpur - Daily shuttle bus - Legoland (B)",
			"Breakfast at your hotel.",
			"Transfer to Johor Bahru.",
			"Upon arrival, proceed to legoland.",
			"Overnight at your hotel."
		),
		"day4" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Kuala Lumpur",
			"Round trip airport transfers",
			"Three nights hotel accommodation in Kuala Lumpur",
			"One night hotel accommodation in Johor Bahru",
			"Daily Breakfast",
			"One way Express Bus from KL to legoland",
			"Entrance ticket to legoland"
		)
	)
);
?>