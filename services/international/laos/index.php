<?php
$tourname = "Laos";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2LPQFE",
		"daysnights" => "3 Days and 2 Nights Luang Prabang Free and Easy Package!",
		"day1" => array(
			"Arrival in Luang Prabang",
			"Meet and greet upon arrival to Luang Prabang.",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Luang Prabang  (B)",
			"Breakfast at your hotel.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Luang Prabang",
			"Round trip airport transfers ",
			"Two nights hotel accommodation",
			"Daily Breakfast"
		)
	),
	array(
		"code" => "2LPQFD",
		"daysnights" => "3 Days 2 Nights Luang Prabang Package!",
		"day1" => array(
			"Arrival in Luang Prabang",
			"Meet and greet upon arrival to Luang Prabang.",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Luang Prabang (B/LD)",
			"Breakfast at your hotel.",
			"Full day Tour (Luang Prabang, Pak Ou Cave, Vat Xieng Thong)"
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Luang Prabang International Airport",
			"Round trip airport transfers",
			"Two nights hotel accommodation",
			"Daily Breakfast",
			"Full day Tour (Luang Prabang, Pak Ou Cave, Vat Xieng Thong)"
		)
	),
	array(
		"code" => "2VTEFE",
		"daysnights" => "3 Days and 2 Nights Vientiane Free and Easy Package!",
		"day1" => array(
			"Arrival in Vientiane",
			"Meet and greet upon arrival to Vientiane.",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Vientiane (B)",
			"Breakfast at your hotel",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Vientiane",
			"Round trip airport transfers ",
			"Two nights hotel accommodation",
			"Daily Breakfast"
		)
	),
	array(
		"code" => "3VTEFP",
		"daysnights" => "4 Days and 3 Nights Laos Package!",
		"day1" => array(
			"Arrival in Wattay Airport (D)",
			"Meet and greet upon arrival to Vientiane.",
			"Transfer to the hotel of your choice for check in.",
			"Vientiane half day tour. ",
			"Overnight at your hotel."
		),
		"day2" => array(
			"Vientiane - Luang Prabang by flight (B/L/D)",
			"Breakfast at your hotel.",
			"Transfer to the airport for the flight to Luang Prabang.",
			"Half day tour in Luang Prabang",
			"Dinner at the local restaurant. Overnight at your hotel."
		),
		"day3" => array(
			"Luang Prabang (B/L/D)",
			"Breakfast at your hotel",
			"Full day tour at Luang Prabang, Pak Ou Caves, Kuangsi  Waterfall."
		),
		"day4" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Vientiane",
			"Round trip airport transfers ",
			"Three nights hotel accommodation",
			"Full board meals",
			"All entrances fees for sightseeing and visits",
			"Traditional boat fare as specified in the program",
			"Services charges, room tax and baggage handling"
		)
	)
);
?>