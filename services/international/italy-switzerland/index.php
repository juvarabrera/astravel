<?php
$tourname = "Italy/Switzerland";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "9FCOBI",
		"daysnights" => "10 Days 9 Nights Best of Italy and Switzerland",
		"day1" => array(
			"Rome",
			"Meet and greet upon arrival and proceed directly to your Hotel for check in by 1400hrs. Then start with Discovering Rome with Hop On Hop Off 48hours Ticket. There is no better way to see Rome than on a double-decker, hop-on hop-off bus! See top Rome attractions on this comprehensive sightseeing tour, such as Vatican City, the Colosseum, Trevi Fountain, Tiber Island and much more. Free at own leisure. Overnight at Hotel"
		),
		"day2" => array(
			"Rome - Venice",
			"Breakfast at Hotel. Continue with <b>Discovering Rome with Hop On Hop Off 48hours Ticket</b>. Free at own leisure. <b>In the afternoon around 2pm departure transfer to Railway Station to connect your train to Venice (Standard Class Train Ticket Included)</b>."
		),
		"day3" => array(
			"Venice",
			"Breakfast at Hotel. In the morning make your own way to the Tourist Office, Plaza San Marcos by 0900hrs. Take a <b>Guided Walking Tour of Venice (SIC Basis)</b>. This interesting tour is centred around the history of St. Mark's Square and its main monuments. Visit the St. Mark's Basilica with its marble and mosaics and the Ducal Palace. This is where the old Republic of Venice exercised their political and commercial powers in the Doge Palace. Cross the Bridge of Sighs arriving at the famous Venice Jail. Finishing off the tour is the opportunity to visit the glass factory on the island of Murano (The visit to Murano is optional and not included in the Walking Tour). The rest of the afternoon free to explore Venice. Overnight at Hotel."
		),
		"day4" => array(
			"Venice - Milan",
			"Breakfast at Hotel. Departure transfer to train station to connect <b>train to Milan</b>. Arrive and transfer to your Hotel by afternoon. Check into Hotel by 1400hrs. Later make your own way to the starting point of the <b>Hop On Hop Off Milan Tour</b> which offers two routes with one ticket. Enjoy a panoramic view of the City on an open top double-decker red Bus. With two lines and 12 stops, both lines touch the most popular sights of Milan which include the magnificent Gothic Cathedral of Duomo, the world famous La Scala Opera House and the beautiful fortress of the Castello Sforzesco. Overnight at Hotel."
		),
		"day5" => array(
			"Milan - Interlaken",
			"Breakfast at Hotel. Today in the morning continue with your <b>Hop On Hop Off Milan Tour</b> which offers two routes with one ticket. With its panoramic open top double-decker red buses, this tour offers you the opportunity to discover this busy and attractive City from a preferential point of view. With two lines and 12 stops, both lines touch the most popular sights of Milan. Which include the magnificent Gothic Cathedral of Duomo, the world famous La Scala Opera House and the beautiful fortress of the Castello Sforzesco. Later departure transfer in the afternoon to the train station to connect your <b>train to Interlaken (Standard Class Train Ticket Included)</b>. Arrive in Interlaken and arrival transfer to your hotel. Check into Hotel by 1400hrs. Rest of the day is free at leisure. Overnight at Hotel."
		),
		"day6" => array(
			"Interlaken - Jungfrau - Top of Europe",
			"Breakfast at Hotel. Proceed on your own to the starting point of this tour at the train station of Interlaken. Here will be a <b>Full Day Tour Jungfrau (Train Basis)</b>. An unforgettable Alpine tour to the majestic world of the Jungfraujoch, Top of Europe at 3'454 m /11'333 ft. A scenic cogwheel train for the most interesting mountain train trip via Kleine Scheidegg at the foot of the famous Eiger North Face to Jungfraujoch, Europe's highest railway station. Enjoy the incredible world of eternal snow and ice. Visit the Ice Palace, experience the breathtaking panorama view from the Sphinx Observation Terrace overlooking the Aletsch Glacier (Europe's longest glacier) and the snow-capped peaks of the neighbouring countries. The down hill mountain trip takes us to Interlaken. Overnight at Hotel."
		),
		"day7" => array(
			"Interlaken - Lucerne",
			"Breakfast at Hotel. You are free in the morning to enjoy this beautiful paradise on earth. Later, check out of the Hotel and departure transfer to train station included to connect to your train to Lucerne. Arrive and you will be met and transferred to your hotel. Check into Hotel by 1400hrs. Rest of the day free to explore Lucerne or shop. Overnight at Hotel."
		),
		"day8" => array(
			"Lucerne - Mount Titlis",
			"Breakfast at Hotel. Proceed on your own to the Lucerne Tourist Office where you will board your coach for the <b>Full Day Tour to Mt Titlis (SIC Basis)</b>. This is an adventure excursion to the highest vantage point in Central Switzerland. A pleasant ride with our deluxe motor coach takes you to the mountain resort of Engelberg in Central Switzerland, where you will board the aerial cable way for a 45 minute journey from the green valley bottom to the high alpine glacier region at 3020m /10000 ft which guarantees snow the whole year round. Enjoy a magnificent ride in the world's first ever revolving 'ROTAIR' gondola, which offers a 360° panoramic view into the Alps. During the 2 hours stay on the mountain visit the Ice Grotto, experience the new Ice Flyer. A chair lift ride over the glacier with fantastic views into crevasses and ice falls and enjoy a snow slide on the Fun lift (These activities are included in our tour). After the descent to Engelberg we board our coach for a drive to Lucerne. Time for leisure approximately 1 hour. A short tour of Lucerne and then some time for shopping. End of Tour. Overnight at Hotel."
		),
		"day9" => array(
			"Lucerne - Zurich",
			"Breakfast at Hotel. Check out of Hotel and departuure transfer to train station to connect to your train to Zurich. (Standard Class Train Travel Included). Arrive and transfer to your hotel. . (You should reach your hotel by 11am) Check into Hotel by 1400hrs (Leave your baggage at Bell Desk in case if you cannot check in hotel if room will only be possible after 1400hrs). Proceed on your own to Zurich Bus Station by 1245hrs where you will take the 5hours <b>Best of Zurich City Tour including the Lindt Chocolate Factory Outlet</b> - Enjoy a comprehensive afternoon coach tour of Zurich and see the top attractions the city has to offer. Follow this up with an introduction to the trendy Zurich West district and an indulgent shopping trip at the world-famous Lindt Chocolate Factory outlet. Make your own way back to the hotel. Overnight at Hotel."
		),
		"day10" => array(
			"Zurich",
			"Breakfast at Hotel. Day free at leisure until departure transfer to Zurich Airport or Train Station for your onward journey. (Private Transfer)."
		),
		"inclusions" => array(
			"9 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Return Airport or Train Station Transfers in all cities",
			"Train between Rome - Venice - Milan - Interlaken - Lucerne - Zurich",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program"
		)
	)
);
?>