<?php
$tourname = "Maldives";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "3MLEFE",
		"daysnights" => "4 Days 3 Nights Maldives Free and Easy Package!",
		"day1" => array(
			"Arrival in Male",
			"Meet and greet upon arrival to Male",
			"Transfer to the hotel of your choice for check in",
			"Free at own leisure for the rest of the day. Overnight at your hotel"
		),
		"day2" => array(
			"Male (B)",
			"Breakfast at your hotel",
			"Free at own leisure for the rest of the day. Overnight at your hotel"
		),
		"day3" => array(
			"Male (B)",
			"Breakfast at your hotel",
			"Free at own leisure for the rest of the day. Overnight at your hotel"
		),
		"day4" => array(
			"Male (B)",
			"Breakfast at your hotel",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Male",
			"Round trip airport transfers",
			"Three nights hotel accommodation",
			"Daily Breakfast"
		)
	)
);
?>