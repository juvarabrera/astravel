<?php
$tourname = "Indonesia";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2DPSFE",
		"daysnights" => "3 Days 2 Nights Free and Easy Package!",
		"day1" => array(
			"Arrival in Bali",
			"Meet and greet upon arrival to Bali.",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Bali (B)",
			"Breakfast at your hotel.",
			"Free at your own leisure for the rest of the day. Overnight stay at hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Bali",
			"Roundtrip airport transfers",
			"Two nights hotel accommodation",
			"Daily breakfast"
		)
	),
	array(
		"code" => "3DPSFB",
		"daysnights" => "4 Days 3 Nights Bali Full Board Package!",
		"day1" => array(
			"Arrival in Bali",
			"Meet and greet upon arrival to Bali.",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Siem Reap (B/L/D)",
			"Breakfast at your hotel.",
			"Siem Reap Tour. ( Angkor Thom - Ta Prohm Temple - Angkor Wat Temple)",
			"Overnight stay at hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Siem Reap",
			"Private return airport transfers",
			"Two nights hotel accommodation",
			"Daily breakfast, one lunch and one dinner",
			"Siem Reap City tour  ( Angkor Thom - Ta Prohm Temple - Angkor Wat Temple)"
		)
	)
);
?>