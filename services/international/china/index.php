<?php
$tourname = "China";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2PEKFE",
		"daysnights" => "4 Days 3 Nights Beijing Free and Easy Package!",
		"day1" => array(
			"Arrival in Beijing ",
			"Meet and greet upon arrival to Beijing",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Beijing (B/L)",
			"Breakfast at your hotel.",
			"Morning half day Beijing city tour. Overnight at your hotel."
		),
		"day3" => array(
			"Beijing (B)",
			"Breakfast at your hotel.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day4" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Three nights hotel accommodation",
			"Daily Breakfast, One Lunch",
			"Beijing half day city tour"
		)
	),
	array(
		"code" => "2PEKBP",
		"daysnights" => "4 Days 3 Nights Beijing Package!",
		"day1" => array(
			"Arrival in Beijing ",
			"Meet and greet upon arrival to Beijing",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Beijing (B/L)",
			"Breakfast at your hotel.",
			"Morning half day Beijing city tour. Overnight at your hotel."
		),
		"day3" => array(
			"Beijing (B)",
			"Breakfast at your hotel.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day4" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Three nights hotel accommodation",
			"Daily Breakfast, One Lunch",
			"Beijing half day city tour"
		)
	),
	array(
		"code" => "2PEKBP",
		"daysnights" => "4 Days 3 Nights Beijing Package!",
		"day1" => array(
			"Arrival in Beijing ",
			"Meet and greet upon arrival to Beijing",
			"Transfer to the hotel of your choice for check in.",
			"Visit Olympic Green Common Domian Bird Nest and Water Cabe. Overnight at your hotel."
		),
		"day2" => array(
			"Beijing (B/L/D)",
			"Breakfast at your hotel.",
			"Great Wall of China - Ming Tombs - Medical Centre - Silk Factory. Overnight at your hotel."
		),
		"day3" => array(
			"Beijing (B/L/D)",
			"Breakfast at your hotel.",
			"Temple of Heaven - Summer Place - The Forbidden City - Tin an men Square. Overnight at your hotel."
		),
		"day4" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Three nights hotel accommodation",
			"Full board meals",
			"Tour 1: Olympic Green Common Domian Bird Nest and Water Cabe",
			"Tour 2: Great Wall of China - Ming Tombs - Medical Centre - Silk Factory",
			"Tour 3: Temple of Heaven - Summer Place - The Forbidden City - Tin an men Square"
		)
	),
	array(
		"code" => "2PVGFE",
		"daysnights" => "4 Days 3 Nights Shanghai Free and Easy Package!",
		"day1" => array(
			"Arrival in Shanghai",
			"Meet and greet upon arrival to Shanghai",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Shanghai (B/L)",
			"Breakfast at your hotel.",
			"Morning half day Shanghai city tour. Overnight at your hotel."
		),
		"day3" => array(
			"Shanghai (B)",
			"Breakfast at your hotel.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day4" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Three nights hotel accommodation",
			"Daily Breakfast, One Lunch",
			"Shanghai half day city tour"
		)
	)
);
?>