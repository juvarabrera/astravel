<?php
$tourname = "Iceland";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "4RKVHI",
		"daysnights" => "5 Days 4 Nights Heritage of Iceland",
		"day1" => array(
			"Reykjavik",
			"Meet and greet upon arrival at <b>Reykjavik International Airport</b> and transfer to your hotel in Reykjavik. Check into Hotel by 1400hrs. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day2" => array(
			"Reykjavik",
			"Breakfast at Hotel. Today we proceed to <b>Half day City Tour of Reykjavik</b>. Visit the presidential residence at Bessastaðir and the town of Hafnarfjörður. This tour will also take you in the town’s old fishing harbour. Then proceed to viewing platform at Perlan. It is built on several hot water tanks, which used to hold the city’s hot water supply. From here, you’ll be able to enjoy a spectacular view over Reykjavík. See the Hallgrímskirkja church. Its design is said to resemble lava flow, a key part of the Icelandic landscape. The church took 38 years to complete and today is one of the city’s best-known landmarks and a fantastic observation point. The church is in the old town centre, which you will also be able to take in, alongside the Icelandic parliament Alþingi and the town hall. Later we return to Reykjavik and some time to explore the area. Overnight at Hotel."
		),
		"day3" => array(
			"Reykjavik",
			"Breakfast at Hotel. Today we proceed to <b>The Blue Lagoon and Golden Circle Tour</b>. This tour takes you takes you to Iceland's top sightseeing attractions - the Blue Lagoon, Thingvellir National Park, Gullfoss waterfall and Geysir geothermal area in one day. Then visit to Þingvellir National Park where the Icelandic parliament Alþingi was founded in the year 930 AD. Return to the hotel. Overnight at Hotel."
		),
		"day4" => array(
			"Reykjavik",
			"Breakfast at Hotel. Today we proceed for <b>Whale Watching Tour</b>. This whale watching tour takes you sailing from Reykjavík out to Faxaflói bay which is home of variety of whales, dolphins, porpoise and other marine life The entrance to the Whale Exhibition Room is free of charge for whale watching passengers. Rest of the day is free at leisure. Overnight at Hotel."
		),
		"day5" => array(
			"Reykjavik",
			"Breakfast at Hotel. Day free at leisure until departure transfer to Airport."
		),
		"inclusions" => array(
			"4 Nights Accommodation",
			"Breakfast Daily",
			"Return Airport or Train Station Transfer upon Arrival and Departure",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program",
			"Government Taxes and Service Charges"
		)
	)
);
?>