<?php
$tourname = "Italy";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "6FCOIR - R",
		"daysnights" => "7 Days 6 Nights Italy by Rail",
		"day1" => array(
			"Rome",
			"Arrive and make your own way to your Hotel for check in by 1400hrs. Then you can start with Discover Rome with Hop On Hop Off 48hours Ticket. See top Rome attractions on this comprehensive sightseeing tour, such as Vatican City, the Colosseum, Trevi Fountain, Tiber Island and much more. Admire top city attractions, such as Rome’s must-see Colosseum - the scene of gory gladiator battles during the Roman Empire - and Piazza Venezia with its ostentatious Monument to Vittorio Emanuele II. Pass down town streets near the eye-catching Trevi Fountain or perhaps hop off at Vatican City to visit one of the most sacred places in Christendom. With your ticket, you can remain on the bus for an entire loop (roughly 1.5-2 hours) as you listen to the informative audio commentary, or you can hop on and off at any of the eight stops around the city to explore Rome’s countless delights. Overnight at Hotel"
		),
		"day2" => array(
			"Rome - Florence",
			"Breakfast at Hotel. Continue with your Hop On Hop Off 48hours Ticket. Then make your own way back to your Hotel. In the afternoon around 2pm, make your own way to the Railway Station to connect your train to Florence. Make your own way to your Hotel. The rest of the day you are free to enjoy Florence. Overnight at Hotel."
		),
		"day3" => array(
			"Florence - Venice",
			"Breakfast at Hotel. Today experience the Hop On Hop Off Tour of Florence. Explore Florence with 2 routes on this Hop On Hop Off tour. In the romantic region of Tuscany lies the jewel City of the Renaissance, Florence. Surrounded by the Appenine Mountain chain, it lies in a fertile valley surrounded by the charming Tuscan vineyards and olive groves. The art galleries and museums of Florence are renowned throughout the world and holidaymakers flock to admire the superb David of Michelangelo, the Venus of Botticelli and the stunning Duomo with its marble facade which glows in the golden sunlight. Later in the afternoon at around 2pm make your own way to Florence Railway Station to connect your train to Venice St Lucia Station. Arrive in Venice make your own way to your Hotel. Overnight at Hotel."
		),
		"day4" => array(
			"Venice",
			"Breakfast at Hotel. In the morning make your own way to the Tourist Office, Plaza San Marcos by 0900hrs. Join a Guided Walking Tour of Venice (SIC Basis). This interesting tour is centred around the history of St. Mark's Square and its main monuments. Visit the St. Mark's Basilica with its marble and mosaics and the Ducal Palace where the old Republic of Venice exercised their political and commercial powers in the Doge Palace. Cross the Bridge of Sighs arriving at the famous Venice Jail. The rest of the afternoon you are free to explore Venice. Overnight at Hotel."
		),
		"day5" => array(
			"Venice - Milan",
			"Breakfast at Hotel. Make your own way to the train station to connect train to Milan. Arrive and make your own way to your Hotel by afternoon. Check into Hotel by 1400hrs. Later make your own way to the starting point of the Hop On Hop Off Milan Tour which offers two routes with one ticket. It offers a panoramic open top double-decker red bus which allows you to visitthe most popular sights of Milan. The magnificent Gothic Cathedral of Duomo, the world famous La Scala Opera House and the beautiful fortress of the Castello Sforzesco. Overnight at Hotel."
		),
		"day6" => array(
			"Milan",
			"Breakfast at Hotel. Today in the morning, continue with your Hop On Hop Off Milan Tour which offers two routes with one ticket. Free at own leisure. Overnight at Hotel"
		),
		"day7" => array(
			"Milan",
			"Breakfast at Hotel. Day free at leisure. Make your own way to Milan Airport."
		),
		"inclusions" => array(
			"6 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Train travel between Rome - Florence - Venice - Milan - Rome",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program"
		)
	),
	array(
		"code" => "6FCOSI",
		"daysnights" => "7 Days 6 Nights Simply Italy",
		"day1" => array(
			"Rome",
			"Meet and greet upon arrival and proceed directly to your Hotel for check in by 1400hrs. Then you can start with <b>Discover Rome with Hop On Hop Off 48hours Ticket</b>. See top Rome attractions on this comprehensive sightseeing tour, such as Vatican City, the Colosseum, Trevi Fountain, Tiber Island and much more. Visit Rome’s must-see Colosseum - the scene of gory gladiator battles during the Roman Empire - and Piazza Venezia with its ostentatious Monument to Vittorio Emanuele II. Pass downtown streets near the eye-catching Trevi Fountain or perhaps hop off at Vatican City to visit one of the most sacred places in Christendom. Free at own leisure. Overnight at Hotel"
		),
		"day2" => array(
			"Rome - Florence",
			"Breakfast at Hotel. <b>Continue with your Hop On Hop Off 48hours Ticket</b>. There is no better way to see Rome than on a double-decker, hop-on hop-off bus! See top Rome attractions on this comprehensive sightseeing tour, such as Vatican City, the Colosseum, Trevi Fountain, Tiber Island and much more. In the afternoon around 2pm, there is a departure transfer to a Railway Station to connect your train to Florence. Meet upon arrival and transfer to your Hotel. The rest of the day you are free to enjoy Florence. Overnight at Hotel."
		),
		"day3" => array(
			"Florence - Venice",
			"Breakfast at Hotel. Today experience the <b>Hop On Hop Off Tour of Florence</b>. Explore Florence with 2 routes on this Hop On Hop Off tour. In the romantic region of Tuscany lies the jewel City of the Renaissance, Florence. Surrounded by the Appenine Mountain chain, it lies in a fertile valley surrounded by the charming Tuscan vineyards and olive groves. The art galleries and museums of Florence are renowned throughout the world and holidaymakers flock to admire the superb David of Michelangelo, the Venus of Botticelli and the stunning Duomo with its marble facade which glows in the golden sunlight. The start point for Line A is S M Novella Station and for Line B is S.Frediano. Later in the afternoon at around 2pm departure transfer to Florence Railway Station to connect your train to Venice St Lucia Station. Arrive in Venice and you will be transferred to your Hotel. Overnight at Hotel."
		),
		"day4" => array(
			"Venice",
			"Breakfast at Hotel. In the morning make your own way to the Tourist Office, Plaza San Marcos by 0900hrs. Join a <b>Guided Walking Tour of Venice (SIC Basis)</b>. This interesting tour is centred around the history of St. Mark's Square and its main monuments. Visit the St. Mark's Basilica with its marble and mosaics and the Ducal Palace where the old Republic of Venice exercised their political and commercial powers in the Doge Palace. Cross the Bridge of Sighs arriving at the famous Venice Jail. Finishing off the tour is the opportunity to visit the glass factory on the island of Murano (The visit to Murano is optional and not included in the walking tour). The rest of the afternoon you are free to explore Venice. Overnight at Hotel."
		),
		"day5" => array(
			"Venice - Milan",
			"Breakfast at Hotel. There will be a departure transfer to the train station to connect <b>train to Milan</b>. Arrive and transfer to your Hotel by afternoon. Check into Hotel by 1400hrs. Later make your own way to the starting point of the <b>Hop On Hop Off Milan Tour</b> which offers two routes with one ticket. It offers a panoramic open top double-decker red bus which allows you the opportunity to discover this busy and attractive City from a preferential point of view. With two lines and 12 stops, both lines touch the most popular sights of Milan. The magnificent Gothic Cathedral of Duomo, the world famous La Scala Opera House and the beautiful fortress of the Castello Sforzesco. Overnight at Hotel."
		),
		"day6" => array(
			"Milan - Rome",
			"Breakfast at Hotel. Today in the morning, continue with your Hop On Hop Off Milan Tour which offers two routes with one ticket. With it's panoramic open top double-decker red buses, it offers you the opportunity to discover this busy and attractive City from a preferential point of view. With two lines and 12 stops, both lines touch the most popular sights of Milan. The first being the magnificent Gothic Cathedral of Duomo. The second is the world famous La Scala Opera House, and the third is the beautiful fortress of the Castello Sforzesco. Later, there is a departure transfer in the afternoon to the train station. Train travel to Rome. Arrival transfer to your Hotel near the Airport. Stay overnight at Galileo Hotel or similar Marcello Royal Hotel."
		),
		"day7" => array(
			"Rome",
			"Breakfast at Hotel. Day free at leisure until departure transfer to Airport."
		),
		"inclusions" => array(
			"6 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Return Airport or Train Station Transfers in all Cities",
			"Train travel between Rome - Florence - Venice - Milan - Rome",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program"
		)
	),
	array(
		"code" => "",
		"daysnights" => "5 Days 4 Nights Experience Rome",
		"day1" => array(
			"Rome",
			"Meet and greet upon arrival and transfer to your Hotel. (Private Transfer). Check in Time is 1400hrs. Rest of the day is free at leisure. Overnight at Hotel."
		),
		"day2" => array(
			"Rome",
			"Breakfast at Hotel. <b>Discover Rome with Hop On Hop Off 48hours Ticket</b>. There is no better way to see Rome than on a double-decker, hop-on hop-off bus! See top Rome attractions on this comprehensive sightseeing tour, such as Vatican City, the Colosseum, Trevi Fountain, Tiber Island and much more. Layers of history have left the city with architectural treasures scattered at every twist and turn, and its Baroque piazzas and ancient antiquities are a joy to behold. Admire top city attractions, such as Rome’s must-see Colosseum - the scene of gory gladiator battles during the Roman Empire - and Piazza Venezia with its ostentatious Monument to Vittorio Emanuele II. Pass downtown streets near the eye-catching Trevi Fountain or perhaps hop off at Vatican City to visit one of the most sacred places in Christendom. Free at leisure. Overnight at Hotel"
		),
		"day3" => array(
			"Rome",
			"Breakfast at Hotel. <b>Continue with your Hop On Hop Off 48hours Ticket</b>. Free at own leisure. Overnight at Hotel"
		),
		"day4" => array(
			"Rome - Shopping Day at Castel Romano Designer Outlet",
			"Breakfast at Hotel, Make your own way to starting point of this tour and Discover a shopping paradise at <b>Castel Romano</b>, a luxurious outdoor mall just south of Rome, on this independent shopping tour! Meet your host near Rome Termini train station and then hop aboard your coach for the short journey south to <b>Castel Romano Designer Outlet</b>. Your host will have shown you where to catch your coach back from when dropping you off. Just return to that point at 1:45pm, 5pm or 8pm, and then board your coach for the trip back to Rome Termini station. If you travel between June and August, there's an additional departure at 8:30pm too. Overnight at Hotel"
		),
		"day5" => array(
			"Rome",
			"Breakfast at Hotel. Day free at leisure until departure transfer to Airport."
		),
		"inclusions" => array(
			"4 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Return Airport or Train Station Transfers",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program"
		)
	)
);
?>