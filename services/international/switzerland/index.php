<?php
$tourname = "Switzerland";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "7GVAES",
		"daysnights" => "8 Days and 7 Nights Switzerland",
		"day1" => array(
			"Geneva",
			"Meet and greet upon arrival at Geneva Airport and transfer to your Hotel by private transfer. Check into Hotel by 1400hrs. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day2" => array(
			"Geneva",
			"Breakfast at Hotel. You will make your own way to <b>Geneva Bus Station (Place Dorcière)</b> for your Geneva City Tour (SIC Basis) which will start at 1030hrs. The City tour of Geneva will bring you to visit the International part of Geneva through the centre of the world’s peacemaking organisations followed by a tour of the main tourist attractions of the City like the Fountain and the wonderful Flower Clock. Then get a taste of Geneva’s history with the mini-train Tramway Tour and dig even deeper into the Old Town with your guide on a <b>Walking tour</b>. The tour will take around 3 hours. Then you are free for the rest of the day. Overnight at Hotel."
		),
		"day3" => array(
			"Geneva - Interlaken",
			"Breakfast at Hotel. Get on your departure transfer to <b>Geneva Train Station</b> where you will experience a ride on Swiss Rail. Train Ticket Included to <b>Interlaken (Standard Class Train Travel Included)</b>. Enjoy your train journey. Arrive in Interlaken and arrival transfer to your hotel. Check into Hotel by 1400hrs. Rest of the day is free at leisure. Overnight at Hotel."
		),
		"day4" => array(
			"Interlaken - Jungfrau - Top of Europe",
			"Breakfast at Hotel. Proceed on your own to the starting point of this tour at the train station of Interlaken. Here will be a <b>Full Day Tour Jungfrau (Train Basis)</b>. An unforgettable Alpine tour to the majestic world of the Jungfraujoch, Top of Europe at 3'454 m /11'333 ft. A scenic cogwheel train for the most interesting mountain train trip via Kleine Scheidegg at the foot of the famous Eiger North Face to Jungfraujoch, Europe's highest railway station. Enjoy the incredible world of eternal snow and ice. Visit the Ice Palace, experience the breathtaking panorama view from the Sphinx Observation Terrace overlooking the Aletsch Glacier (Europe's longest glacier) and the snow-capped peaks of the neighbouring countries. The down hill mountain trip takes us to Interlaken. Overnight at Hotel."
		),
		"day5" => array(
			"Interlaken - Lucerne",
			"Breakfast at Hotel. You are free in the morning to enjoy this beautiful paradise on earth. Later, check out of the Hotel and departure transfer to train station included to connect to your train to Lucerne. Arrive and you will be met and transferred to your hotel. Check into Hotel by 1400hrs. Rest of the day free to explore Lucerne or shop. Overnight at Hotel."
		),
		"day6" => array(
			"Lucerne - Mount Titlis",
			"Breakfast at Hotel. Proceed on your own to the Lucerne Tourist Office where you will board your coach for the Full Day Tour to Mt Titlis (SIC Basis). This is an adventure excursion to the highest vantage point in Central Switzerland. A pleasant ride with our deluxe motor coach takes you to the mountain resort of Engelberg in Central Switzerland, where you will board the aerial cable way for a 45 minute journey from the green valley bottom to the high alpine glacier region at 3020m /10000 ft which guarantees snow the whole year round. Enjoy a magnificent ride in the world's first ever revolving 'ROTAIR' gondola, which offers a 360° panoramic view into the Alps. During the 2 hours stay on the mountain visit the Ice Grotto, experience the new Ice Flyer. A chair lift ride over the glacier with fantastic views into crevasses and ice falls and enjoy a snow slide on the Fun lift (These activities are included in our tour). After the descent to Engelberg we board our coach for a drive to Lucerne. Time for leisure approximately 1 hour. A short tour of Lucerne and then some time for shopping. End of Tour. Overnight at Hotel."
		),
		"day7" => array(
			"Lucerne - Zurich",
			"Breakfast at Hotel. Check out of Hotel and departuure transfer to train station to connect to your train to Zurich. (Standard Class Train Travel Included). Arrive and transfer to your hotel. . (You should reach your hotel by 11am) Check into Hotel by 1400hrs (Leave your baggage at Bell Desk in case if you cannot check in hotel if room will only be possible after 1400hrs). Proceed on your own to Zurich Bus Station by 1245hrs where you will take the 5hours <b>Best of Zurich City Tour including the Lindt Chocolate Factory Outlet</b> - Enjoy a comprehensive afternoon coach tour of Zurich and see the top attractions the city has to offer. Follow this up with an introduction to the trendy Zurich West district and an indulgent shopping trip at the world-famous Lindt Chocolate Factory outlet. Make your own way back to the hotel. Overnight at Hotel."
		),
		"day8" => array(
			"Zurich",
			"Breakfast at Hotel. Day free at leisure until departure transfer to Zurich Airport or Train Station for your onward journey. (Private Transfer)."
		),
		"inclusions" => array(
			"7 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Airport or Train Station Transfers in all cities",
			"2nd class train travel Geneva - Interlaken - Lucerne - Zurich",
			"Sightseeing Tour as per itinerary"
		)
	)
);
?>