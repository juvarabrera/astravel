<?php
$tourname = "Brunei";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2BWNFE",
		"daysnights" => "3 Days 2 Nights Free and Easy Package!",
		"day1" => array(
			"Arrival in Brunei",
			"Meet and greet upon arrival to Brunei International Airport.",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Half day City tour",
			"Breakfast at your hotel.",
			"Half day Brunei City."
		),
		"day3" => array(
			"Departure",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Brunei International Airport",
			"eturn airport transfers on SIC basis",
			"Two night accommodation",
			"Daily breakfast",
			"Complimentary half day city tour."
		)
	),
	array(
		"code" => "3BWNIP",
		"daysnights" => "4 Days 3 Nights Brunei Package!",
		"day1" => array(
			"Arrival in Brunei",
			"Meet and greet upon arrival to Brunei International Airport.",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Half day Brunei City & Water Village Tour",
			"Breakfast at your hotel.",
			"Half day Brunei City & Water Village Tour on SIC basis"
		),
		"day3" => array(
			"Full Day Seria Oiltown Tour with lunch ",
			"Breakfast at your hotel.",
			"Full Day Seria Oiltown Tour with lunch on SIC basis"
		),
		"day4" => array(
			"Depart Brunei",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Brunei International Airport",
			"Return airport transfers on SIC basis",
			"Three night accommodation",
			"Daily breakfast",
			"Half day Brunei City & Water Village Tour",
			"Full day Seria Oiltown Tour with Lunch"
		)
	),
	array(
		"code" => "3BWNNP",
		"daysnights" => "4 Days 3 Nights Brunei Package!",
		"day1" => array(
			"Arrival in Brunei",
			"Meet and greet upon arrival to Brunei International Airport.",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Full day Temburong national Park Nature Escapade",
			"Breakfast at your hotel.",
			"Full day Temburong national Park Nature Escapade SIC basis"
		),
		"day3" => array(
			"Half day Proboscis Monkey River Safari ",
			"Breakfast at your hotel.",
			"Half day Proboscis Monkey River Safari on SIC basis"
		),
		"day4" => array(
			"Depart Brunei",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Brunei International Airport",
			"Return airport transfers on SIC basis",
			"Three night accommodation",
			"Daily breakfast",
			"Half day Proboscis Monkey River Safari ",
			"Full day Temburong National Park Nature Escapade"
		)
	),
	array(
		"code" => "3BWNMP",
		"daysnights" => "4 Days 3 Nights Miri Day Trip!",
		"day1" => array(
			"Arrival in Brunei",
			"Meet and greet upon arrival to Brunei International Airport.",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Half day City Tour",
			"Breakfast at your hotel.",
			"Half day City Tour"
		),
		"day3" => array(
			"Miri Oiltown Full day tour ",
			"Breakfast at your hotel.",
			"Miri Oiltown Full day tour on SIC basis"
		),
		"day4" => array(
			"Depart Brunei",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Brunei International Airport",
			"Return airport transfers on SIC basis",
			"Three night accommodation",
			"Daily breakfast",
			"Miri Oiltown Full day tour",
			"Half day Brunei city tour"
		)
	),
	array(
		"code" => "4BWNDP",
		"daysnights" => "5 Days 4 Nights Miri Day Trip!",
		"day1" => array(
			"Arrival in Brunei",
			"Meet and greet upon arrival to Brunei International Airport.",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Half day City Tour",
			"Breakfast at your hotel.",
			"Half day City Water Village Tour "
		),
		"day3" => array(
			"Full day Seria Oiltown Tour",
			"Breakfast at your hotel.",
			"Full day Seria Oiltown Tour with lunch on SIC basis"
		),
		"day4" => array(
			"Full Day Temburong National Park",
			"Breakfast at your hotel.",
			"Full Day Temburong National Park on SIC basis"
		),
		"day5" => array(
			"Depart Brunei",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Brunei International Airport",
			"Return airport transfers on SIC basis",
			"Three night accommodation",
			"Daily breakfast",
			"Miri Oiltown Full day tour",
			"Half day Brunei city tour"
		)
	)
);
?>