<?php
$tourname = "Sri Lanka";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "5CMBSP",
		"daysnights" => "6 Days 5 Nights Sri Lanka Package!",
		"day1" => array(
			"Arrival in Colombo",
			"Meet and greet upon arrival to Colombo",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."

		),
		"day2" => array(
			"Sigiriya Polonnaruwa / Minneriya (B)",
			"Breakfast at your hotel.",
			"Transfer to Sigiriya.",
			"Polonnaruwa City Tour - Minneriya National Park. Overnight at your hotel."
		),
		"day3" => array(
			"Dambulla - Matale - Kandy (B)",
			"Breakfast at your hotel.",
			"Visit Dambulla Rock Temple - Spice Garden in Matale - Kandy City tour. Overnight at your hotel."
		),
		"day4" => array(
			"Kandy - Peradeniya - Tea Plantation - Nuwera Eliya (B)",
			"Breakfast at your hotel.",
			"Visit Royal Botanical Garden - Tea Plantation and a factory - continue to Nuwara Eliya. Overnight at your hotel."
		),
		"day5" => array(
			"Nuwera Eliya - Kitulagala - Colombo (B)",
			"Breakfast at your hotel.",
			"Transfer to Colombo. Proceed to Colombo City Tour. Overnight at your hotel."
		),
		"day6" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Colombo",
			"Round trip airport transfers",
			"Five nights hotel accommodation",
			"Half board (Dinner + Breakfast)",
			"All taxes(Sri Lanka)"
		)
	)
);
?>