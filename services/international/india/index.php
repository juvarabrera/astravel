<?php
$tourname = "India";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "4DELGT",
		"daysnights" => "5 Days 4 Nights Golden Triangle Tour!",
		"day1" => array(
			"Arrival in New Delhi",
			"Meet and greet upon arrival to New Delhi",
			"Transfer to the hotel of your choice for check in",
			"Free at own leisure for the rest of the day. Overnight at your hotel"
		),
		"day2" => array(
			"Delhi - Agra (B/L/D)",
			"Breakfast at your hotel",
			"Agra City tour (Taj Mahal - Agra Fort - handicraft & textile industry). Overnight at your hotel"
		),
		"day3" => array(
			"Agra - Jaipur (B/L/D)",
			"Breakfast at your hotel",
			"Visit Fatehpur Sikri - Birla Temple(laxmi Narayan Temple) - City Palace . Overnight at your hotel"
		),
		"day4" => array(
			"Jaipur (B/L/D)",
			"Breakfast at your hotel",
			"Jaipur City Tour. Visit Amer Fort - Jal Mahal - Hawa Mahal. Overnight at your hotel"
		),
		"day5" => array(
			"Jaipur - Dehi (B)",
			"Breakfast at your hotel",
			"Visit Jama Masjid - Kingdom of Dreams",
			"Transfer out to the airport for your flight out"
		),
		"inclusions" => array(
			"Meet and greet services upon arrival",
			"Round trip airport transfers",
			"Four nights hotel accommodation",
			"Full board meals",
			"Tours as per the itinerary",
			"Monument Entrance + Elephant ride at Amer Fort in Jaipur",
			"Entry ticket & dinner at Kingdom of Dream",
			"All fuel charge, Toll taxes, Driver allowance, enter state taxes etc",
			"Presently applicable government taxes. (India)"
		)
	),
	array(
		"code" => "5DELGT",
		"daysnights" => "6 Days 5 Nights Golden Triangle Tour",
		"day1" => array(
			"Arrival in New Delhi",
			"Meet and greet upon arrival to New Delhi",
			"Transfer to the hotel of your choice for check in",
			"Free at own leisure for the rest of the day. Overnight at your hotel"
		),
		"day2" => array(
			"Delhi - Agra (B/L/D)", 
			"Breakfast at your hotel",
			"Drive towards Agra. Upon arrival, proceed to short city tour. Visit handicraft & textile industry. Overnight stay at your hotel in Agra"
		),
		"day3" => array(
			"Agra (B/L/D)", 
			"Breakfast at your hotel", 
			"Agra City tour (Taj Mahal - Agra Fort - Shopping). Overnight at your hotel"
		),
		"day4" => array(
			"Agra - Jaipur (B/L/D)", 
			"Breakfast at your hotel", 
			"Visit Fatehpur Sikri - Birla Temple(laxmi Narayan Temple) - City Palace . Overnight at your hotel"
		),
		"day5" => array(
			"Jaipur (B/L/D)", 
			"Breakfast at your hotel", 
			"Jaipur City Tour. Visit Amer Fort - Jal Mahal - Hawa Mahal. Overnight at your hotel"
		),
		"day6" => array(
			"Jaipur - Dehi (B)", 
			"Breakfast at your hotel", 
			"Visit Jama Masjid - Kingdom of Dreams", 
			"Transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival", 
			"Round trip airport transfers", 
			"Five nights hotel accommodation", 
			"Full board meals", 
			"Tours as per the itinerary", 
			"Monument Entrance + Elephant ride at Amer Fort in Jaipur", 
			"Entry ticket & dinner at Kingdom of Dream", 
			"All fuel charge, Toll taxes, Driver allowance, enter state taxes etc", 
			"Presently applicable government taxes.(India)"
		)
	),
	array(
		"code" => "6DELGT",
		"daysnights" => "7 Days 6 Nights Golden Triangle Tour<br><br>New Delhi - Srinagar - Gulmarg - Agra - Jaipur - New Delhi",
		"inclusions" => array(
			"Meet and greet services upon arrival", 
			"Round trip airport transfers", 
			"Five nights hotel accommodation", 
			"Full board meals", 
			"Tours as per the itinerary", 
			"Monument Entrance + Elephant ride at Amer Fort in Jaipur", 
			"Entry ticket & dinner at Kingdom of Dream", 
			"All fuel charge, Toll taxes, Driver allowance, enter state taxes etc", 
			"Presently applicable government taxes.(India)"
		)
	),
	array(
		"code" => "7DELGT",
		"daysnights" => "8 Days 7 Nights Golden Triangle Tour<br><br>New Delhi - Srinagar - Gulmarg - Agra - Jaipur - New Delhi",
		"inclusions" => array(
			"Meet and greet services upon arrival", 
			"Round trip airport transfers", 
			"Five nights hotel accommodation", 
			"Full board meals", 
			"Tours as per the itinerary", 
			"Monument Entrance + Elephant ride at Amer Fort in Jaipur", 
			"Entry ticket & dinner at Kingdom of Dream", 
			"All fuel charge, Toll taxes, Driver allowance, enter state taxes etc", 
			"Presently applicable government taxes.(India)"
		)
	)
);
?>