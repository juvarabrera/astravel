<?php
$tourname = "Scotland";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "4EDIBS",
		"daysnights" => "5 Days 4 Nights Best of Scotland",
		"day1" => array(
			"Edinburgh",
			"Arrival transfer at London Heathrow Airport to your Hotel. Check into Hotel by 1400hrs. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day2" => array(
			"Edinburgh",
			"Breakfast at Hotel. <b>Hop On Hop Off Edinburgh Tour with 1 Day Ticket</b> - Make your own way to the starting point of the tour which is Hanover Street. The tour will highlight all the things to do and see in Edinburgh as you tour through the city streets. Overnight at Hotel."
		),
		"day3" => array(
			"Edinburgh",
			"Breakfast at Hotel, <b>Full Day Original Loch Ness Tour</b> - Make your own way to the starting point of the tour. This is the Original Loch Ness and the Highlands tour. Explore the beautiful Scottish Highlands, discover majestic Urquhart Castle and take a monster cruise on Loch Ness. We leave Edinburgh behind we head towards Stirling with its ancient castle then stop for morning coffee stop at Kilmahog. Next it's Rob Roy Country and dramatic Rannoch Moor before arriving at the Weeping Glen - Glen Coe. After a short drive we arrive at Fort William which sits in the shadow of Ben Nevis, Britain's highest mountain. After time for lunch there's a photo stop at Spean Bridge before we enter the scenic beauty of the famous Great Glen. Our journey follows the route of the Caledonian Canal to Loch Ness and the impressive ruins of Urquhart Castle. On our homeward journey, we pass by Inverness then enter the wild beauty of the Cairngorm National Park. Our descent through the Grampian Mountains takes us into scenic Perthshire. The last part of our tour takes us through the ancient Kingdom of Fife to the famous Forth Rail Bridge and the conclusion of our tour in Edinburgh. Overnight at Hotel."
		),
		"day4" => array(
			"Edinburgh",
			"Breakfast at Hotel, Highland Glens, Castles and Whisky Tour - Make your own way to the starting point of the tour. We make our way into the Kingdom of Fife and our first stop of the day at Dunfermline Abbey and Palace.We continue our tour through beautiful Glen Devon and pass the famous Gleneagles Hotel. The attractive spa town of Crieff is the location of quaint Glenturret Malt Whisky Distillery (optional). Next we venture into the Highlands taking the picturesque route through the \"Sma Glen\" and Strathbaan. Enjoy fine views of the Firth of Forth and the Forth Bridges when we make our last stop of the day in pretty South Queensferry. It’s a short drive back to Edinburgh. Overnight at Hotel."
		),
		"day5" => array(
			"Edinburgh",
			"Breakfast at Hotel. Day free at leisure until departure transfer to Edinburgh Airport or Train Station for your onward journey. (Private Transfer)."
		),
		"inclusions" => array(
			"4 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Return Airport or Train Station Transfers",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program"
		)
	)
);
?>