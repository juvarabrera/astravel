<?php
$tourname = "Hong Kong";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2HKGFE",
		"daysnights" => "3 Days 2 Nights Hong Kong Free and Easy Package!",
		"day1" => array(
			"Arrival in Hong Kong",
			"Meet and greet upon arrival to Hong Kong",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Hong Kong (B)",
			"Breakfast Outside.",
			"Kowloon Morning City tour. Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast Outside.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Two nights hotel accommodation",
			"Daily Breakfast",
			"Morning Kowloon City tour"
		)
	),
	array(
		"code" => "2HKGDE",
		"daysnights" => "3 Days 2 Nights Hong Kong with Disneyland Package!",
		"day1" => array(
			"Arrival in Hong Kong",
			"Meet and greet upon arrival to Hong Kong",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Hong Kong (B)",
			"Breakfast Outside.",
			"Kowloon Morning City tour - Hong Kong Disneyland Tour. Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast Outside.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Two nights hotel accommodation",
			"Daily Breakfast",
			"Morning Kowloon City tour",
			"1 day pass for Disneyland with Round trip Transfers"
		)
	),
	array(
		"code" => "2HKGOP",
		"daysnights" => "3 Days 2 Nights Hong Kong with Ocean Park Package!",
		"day1" => array(
			"Arrival in Hong Kong",
			"Meet and greet upon arrival to Hong Kong",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Hong Kong (B)",
			"Breakfast Outside.",
			"Kowloon Morning City tour - Ocean Park Tour. Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast Outside.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Two nights hotel accommodation",
			"Daily Breakfast",
			"Morning Kowloon City tour",
			"1 day pass for Ocean Park with Round trip Transfers"
		)
	),
	array(
		"code" => "2HKGMP",
		"daysnights" => "3 Days 2 Nights Hong Kong with Macau Package!",
		"day1" => array(
			"Arrival in Hong Kong",
			"Meet and greet upon arrival to Hong Kong",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Hong Kong (B)",
			"Breakfast Outside.",
			"Kowloon Morning City tour - Macau Tour. Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast Outside.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Two nights hotel accommodation",
			"Daily Breakfast",
			"Morning Kowloon City tour",
			"Macau Tour with 1 way ferry ticket"
		)
	),
	array(
		"code" => "2HKGSH",
		"daysnights" => "2 Days 1 Nights Shenzhen Free and Easy Package!",
		"day1" => array(
			"Arrival in Hong Kong (L)",
			"Meet and greet upon arrival to Hong Kong",
			"Transfer to the hotel of your choice for check in.",
			"Shenzhen City Tour. Overnight at your hotel."
		),
		"day2" => array(
			"Shenzhen (B)",
			" Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out/ transfer to City hotel."
		),
		"inclusions" => array(
			"-	Meet and greet services upon arrival ",
			"Round trip transfers to/from Hong Kong ",
			"One night hotel accommodation",
			"Daily Breakfast, One Lunch",
			"Shenzhen City tour"
		)
	)
);
?>