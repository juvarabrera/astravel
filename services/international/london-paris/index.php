<?php
$tourname = "London/Paris";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "5LHRLP - R",
		"daysnights" => "6 Days 5 Nights Discover London & Paris",
		"day1" => array(
			"London",
			"Arrive and make your own way to your hotel. Check in Time is 1400hrs. Rest of the day is free at ownleisure. Overnight at Hotel."
		),
		"day2" => array(
			"London",
			"Breakfast at Hotel. The Original London Sightseeing Tour: Hop-on Hop-off 24hours Ticket - See top London attractions at your own pace on a comprehensive hop-on hop-off sightseeing tour! See all the top sights like the London Eye, Big Ben, Westminster Abbey, Tower Bridge and more. Redeem your voucher, and then ride around London aboard your spacious open-top double- decker bus. Free at own leisure. Overnight at Hotel."
		),
		"day3" => array(
			"London",
			"Breakfast at Hotel. Day free at leisure to explore London on your own. Overnight at Hotel."
		),
		"day4" => array(
			"London - Paris",
			"Breakfast at Hotel. Make your own way to the train station to connect your Eurostar train to Paris (Standard Class Train Ticket Included). Arrive and proceed to your Hotel on own. Then you may start Discovering Paris at your leisure with 2 Days Hop-on Hop-off Tour. Get a bird's eye view of the cobbled streets of the Left Bank and the Napoleonic splendour of the Ecole Militaire. See the Eiffel Tower, Paris' most famous landmark. Stop off at the Arc de Triomphe and walk down the stylish Champs-Elysees. Become acquainted with some of the world's most famous artworks at the Louvre including the mysterious Mona Lisa. Your ticket will be valid for 2 consecutive days. Overnight at Hotel."
		),
		"day5" => array(
			"Paris",
			"Breakfast at Hotel. Continue with your 2nd Day of your Hop-on Hop-off Tour is the easiest way to get around town. You can hop-on and off at any of the nine stops as often as you like over a one or two day period. Free at own leisure. Overnight at Hotel."
		),
		"day6" => array(
			"Paris",
			"Breakfast at Hotel. Make your own way to the airport. Check out of Hotel by 1100hrs."
		),
		"inclusions" => array(
			"5 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program",
			"One way Travel on Standard Class Eurostar to Paris",
			"Government Taxes and Service Charges"
		)
	),
	array(
		"code" => "5LHRLP",
		"daysnights" => "6 Days 5 Nights London and Paris",
		"day1" => array(
			"London",
			"Meet and greet upon arrival and transfer to your Hotel. (Private Transfer). Check in Time is 1400hrs. Rest of the day is free at leisure. Overnight at Hotel."
		),
		"day2" => array(
			"London",
			"Breakfast at Hotel. <b>The Original London Sightseeing Tour: Hop-on Hop-off 24hours Ticket</b> - See top London attractions at your own pace on a comprehensive hop-on hop-off sightseeing tour! See all the top sights like the London Eye, Big Ben, Westminster Abbey, Tower Bridge and more. Free at leisure. Overnight at Hotel."
		),
		"day3" => array(
			"London",
			"Breakfast at Hotel. Day free at leisure to explore London on your own. Overnight at Hotel."
		),
		"day4" => array(
			"London - Paris",
			"Breakfast at Hotel. There will be a departure transfer to St Pancras Station to connect your <b>Eurostar train to Paris (Standard Class Train Ticket Included)</b>. Arrival transfer to your Hotel in Paris. Check into Hotel by 1400hrs. Then you may start Discovering Paris at your leisure with <b>2 Days Hop-on Hop-off Tour</b>. See the Eiffel Tower, Paris' most famous landmark. Stop off at the Arc de Triomphe and walk down the stylish Champs-Elysees. Become acquainted with some of the world's most famous artworks at the Louvre including the mysterious Mona Lisa. Overnight at Hotel."
		),
		"day5" => array(
			"Paris",
			"Breakfast at Hotel. Continue with your 2nd Day of your Hop-on Hop-off Tour is the easiest way to get around town. You can hop-on and off at any of the nine stops as often as you like over a one or two day period. The open- top double-decker bus provides fantastic views from the top floor. Buses depart every 10 minutes in summer and every 20 minutes in winter, every day of the year. Recorded commentary is available onboard in French, English, Spanish, German, Italian, Japanese, Russian and Chinese. You will receive headphones for the commentary the first time you use your ticket. Overnight at Hotel"
		),
		"day6" => array(
			"Paris",
			"Breakfast at Hotel. Day free until departure transfer to Paris CDG or ORLY Airport. Check out of Hotel by 1100hrs."
		),
		"inclusions" => array(
			"5 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Arrival & Departure Transfers in London & Paris",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program",
			"One way Travel on Standard Class Eurostar to Paris",
			"Government Taxes and Service Charges"
		)
	),
	array(
		"code" => "6LHRCL",
		"daysnights" => "7 Days 6 Nights Charming London & Paris",
		"day1" => array(
			"London",
			"Arrival transfer at London Heathrow Airport to your Hotel. Check into Hotel by 1400hrs. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day2" => array(
			"London",
			"Breakfast at Hotel. <b>The Original London Sightseeing Tour: Hop-on Hop-off 24hours Ticket</b> - See top London attractions at your own pace on a comprehensive hop-on hop-off sightseeing tour! Free at own leisure. Overnight at Hotel."
		),
		"day3" => array(
			"London - Paris",
			"Breakfast at Hotel. Prepare for your departure transfer to St Pancras Station to connect your <b>Eurostar train to Paris (Standard Class Train Ticket Included)</b>. Arrival transfer to your Hotel in Paris. Check in at Hotel. Overnight at Hotel."
		),
		"day4" => array(
			"Paris",
			"Breakfast at Hotel. Start Discovering Paris at your leisure with 2 Days Hop-on Hop-off Tour. The Paris City Hop-on Hop-off Tour is the easiest way to get around town. See the Eiffel Tower, Paris' most famous landmark. Stop off at the Arc de Triomphe and walk down the stylish Champs-Elysees. Become acquainted with some of the world's most famous artworks at the Louvre including the mysterious Mona Lisa. Your ticket will be valid for 2 consecutive days. Overnight at Hotel."
		),
		"day5" => array(
			"Paris",
			"Breakfast at Hotel. Continue with your 2nd Day of your Hop-on Hop-off Tour is the easiest way to get around town. You can hop-on and off at any of the nine stops as often as you like over a one or two day period. The open- top double-decker bus provides fantastic views from the top floor. Buses depart every 10 minutes in summer and every 20 minutes in winter, every day of the year. Recorded commentary is available onboard in French, English, Spanish, German, Italian, Japanese, Russian and Chinese. You will receive headphones for the commentary the first time you use your ticket. Overnight at Hotel"
		),
		"day6" => array(
			"Paris - London",
			"Breakfast at Hotel. Prepare for your departure transfer to <b>Eurostar Train Station to connect to your Eurostar Train to London (Standard Class Train Ticket Included)</b>. Meet and transfer upon arrival at London St Pancras Station to your Hotel. Check into Hotel by 1400hrs. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day7" => array(
			"London",
			"Breakfast at Hotel. Day free until departure transfer to London Heathrow Airport. Check out of Hotel by 1100hrs."
		),
		"inclusions" => array(
			"6 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Arrival & Departure Transfers in London & Paris",
			"Return Travel on Standard Class Eurostar to Paris",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program"
		)
	)
);
?>