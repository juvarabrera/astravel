<?php
$tourname = "Thailand";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "3BKKAP",
		"daysnights" => "4 Days 3 Nights Bangkok - Ayutthaya Package!",
		"day1" => array(
			"Arrival in Bangkok",
			"Meet and greet upon arrival to Bangkok",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Ayutthaya (B/L/D)",
			"Breakfast at your hotel.",
			"Ayutthaya - Wat Panancheung - Ayutthaya Floating Market. ",
			"Overnight at your hotel."
		),
		"day3" => array(
			"Bangkok (B/L/D)",
			"Breakfast at your hotel.",
			"Wat Arun - Golden Buddha Temple - Shopping J.J. Mall. Overnight at your hotel."
		),
		"day4" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Three nights hotel accommodation",
			"Full board meals",
			"Tour 1: Ayutthaya - Wat Panancheung - Ayutthaya Floating Market.",
			"Tour 2: Wat Arun - Golden Buddha Temple - Shopping J.J. Mall"
		)
	),
	array(
		"code" => "2BKKFE",
		"daysnights" => "3 Days 2 Nights Bangkok Free and Easy Package!",
		"day1" => array(
			"Arrival in Bangkok",
			"Meet and greet upon arrival to Bangkok",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Bangkok (B)",
			"Breakfast at your hotel.",
			"Free Bangkok City Tour; visit Bangkok Art in Paradise Museum.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Two nights hotel accommodation",
			"Daily Breakfast",
			"Bangkok City Tour"
		)
	),
	array(
		"code" => "2BKKBP",
		"daysnights" => "3 Days 2 Nights Bangkok Package!",
		"day1" => array(
			"Arrival in Bangkok",
			"Meet and greet upon arrival to Bangkok",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Bangkok (B/L/D)",
			"Breakfast at your hotel.",
			"Bangkok Tour (Wat Arun - Bangkok Art in Paradise - Honey Farm - MBK). Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Two nights hotel accommodation",
			"Full board meals",
			"Bangkok Tour (Wat Arun - Bangkok Art in Paradise - Honey Farm - MBK)."
		)
	),
	array(
		"code" => "2BKKSP",
		"daysnights" => "3 Days 2 Nights Shopping Tour Package!",
		"day1" => array(
			"Arrival in Bangkok",
			"Meet and greet upon arrival to Bangkok ",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Bangkok (B/L)",
			"Breakfast at your hotel.",
			"Gems Jewelry - Natural Honey Product - Thai Silk & Leather Factory. MBK - Siam Paragon - Pratunam Complex. Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival",
			"Round trip airport transfers ",
			"Two nights hotel accommodation",
			"Daily Breakfast and One Lunch"
		)
	),
	array(
		"code" => "3BKKBB",
		"daysnights" => "4 Days 3 Nights Bangkok Blast Package!",
		"day1" => array(
			"Arrival in Bangkok",
			"Meet and greet upon arrival to Bangkok",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Bangkok (B/L/D)",
			"Breakfast at your hotel.",
			"Bangkok Tour (Wat Arun - Reclining Buddha Temple - Bangkok Art in Paradise). ",
			"Overnight at your hotel."
		),
		"day3" => array(
			"Bangkok (B/L/D)",
			"Breakfast at your hotel.",
			"Bangkok Tour (Grand Palace - Emerald Buddha Temple - Vimarnmek Palace - MBK). Overnight at your hotel."
		),
		"day4" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."),
		"inclusions" => array(
			"Meet and greet services upon arrival",
			"Round trip airport transfers",
			"Two nights hotel accommodation",
			"Full board meals",
			"Bangkok Tour (Grand Palace - Emerald Buddha Temple - Vimarnmek Palace - MBK).",
			"Bangkok Tour (Wat Arun - Reclining Buddha Temple - Bangkok Art in Paradise)."
		)
	),
	array(
		"code" => "2CNXFE",
		"daysnights" => "3 Days 2 Nights Chiang Mai Free and Easy Package!",
		"day1" => array(
			"Arrival in Chiang Mai",
			"Meet and greet upon arrival to Chiang Mai",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Chiang Mai (B/L/D)",
			"Breakfast at your hotel.",
			"Doi Suthep - Home Industries. Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Two nights hotel accommodation",
			"Full board meals",
			"Tour: Doi Suthep - Home Industries"
		)
	),
	array(
		"code" => "3BKKHH",
		"daysnights" => "4 Days 3 Nights Bangkok - Hua Hin Package!",
		"day1" => array(
			"Arrival in Bangkok",
			"Meet and greet upon arrival to Bangkok",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel in Bangkok."
		),
		"day2" => array(
			"Hua Hin (B/L/D)",
			"Breakfast at your hotel.",
			"Tum Khao Luang Temple - Plern Waan Market. Overnight at your hotel in Hua Hin."
		),
		"day3" => array(
			"Bangkok (B/L/D)",
			"Breakfast at your hotel.",
			"Golden Buddha Temple - MBK. Overnight at your hotel in Bangkok."
		),
		"day4" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival",
			"Round trip airport transfers",
			"Two nights hotel accommodation in Bangkok",
			"One night accommodation in Hua Hin",
			"Full Board Meals",
			"Tour 1: Tum Khao Luang Temple - Plern Waan Market",
			"Tour 2: Golden Buddha Temple - MBK"
		)
	),
	array(
		"code" => "2BKKPB",
		"daysnights" => "3 Days 2 Nights Pattaya - Bangkok Package!",
		"day1" => array(
			"Arrival in Bangkok",
			"Meet and greet upon arrival to Bangkok",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel in Pattaya."
		),
		"day2" => array(
			"Pattaya (B/L/D)",
			"Breakfast at your hotel.",
			"Nongnooch Village - Bangkok Art in Paradise - MBK. Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival",
			"Round trip airport transfers",
			"One night hotel accommodation in Bangkok",
			"One night hotel accommodation in Pattaya",
			"Full board meals",
			"Bangkok tour",
			"Pattaya tour"
		)
	),
	array(
		"code" => "3BKKPT",
		"daysnights" => "4 Days 3 Nights Pattaya - Bangkok Package!",
		"day1" => array(
			"Arrival in Bangkok",
			"Meet and greet upon arrival to Bangkok",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Bangkok (B/L/D)",
			"Breakfast at your hotel.",
			"Wat Arun - Golden Buddha Temple - Tiger Zoo and Crocodile Farm. Overnight at your hotel in Pattaya."
		),
		"day3" => array(
			"Pattaya(B/L/D)",
			"Breakfast at your hotel.",
			"Free at own leisure for the rest of the day. Overnight at your hotel in Bangkok."
		),
		"day4" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival",
			"Round trip airport transfers",
			"Two nights hotel accommodation in Bangkok",
			"One night hotel accommodation in Pattaya",
			"Full board meals",
			"Bangkok tour",
			"Pattaya tour"
		)
	),
	array(
		"code" => "2HKTFE",
		"daysnights" => "3 Days 2 Nights Phuket Free and Easy Package!",
		"day1" => array(
			"Arrival in Phuket",
			"Meet and greet upon arrival to Phuket",
			"Transfer to the hotel of your choice for check in.",
			"Half day City tour. Overnight at your hotel."
		),
		"day2" => array(
			"Phuket (B)",
			"Breakfast at your hotel.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival",
			"Round trip airport transfers",
			"Two nights hotel accommodation",
			"Daily Breakfast",
			"Free half day city visit to Chalong Temple and shopping"
		)
	),
	array(
		"code" => "2HKTPP",
		"daysnights" => "3 Days 2 Nights Phuket Package!",
		"day1" => array(
			"Arrival in Phuket",
			"Meet and greet upon arrival to Phuket",
			"Transfer to the hotel of your choice for check in.",
			"Sightseeing drive passing Phuket city and shopping. Overnight at your hotel."
		),
		"day2" => array(
			"Phuket (B/L/D)",
			"Breakfast at your hotel.",
			"Full day tour at Phi Phi Island and Ao Maya Bay Sea angel Cruise. Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"-	Meet and greet services upon arrival",
			"Round trip airport transfers",
			"Two nights hotel accommodation",
			"Full board meals",
			"Full day tour at Phi Phi Island and Ao Maya Bay Sea Angel Cruise"
		)
	),
	array(
		"code" => "3HKTFB",
		"daysnights" => "4 Days 3 Nights Pattaya - Bangkok Package!",
		"day1" => array(
			"Arrival in Phuket",
			"Meet and greet upon arrival to Phuket",
			"Transfer to the hotel of your choice for check in.",
			"Sightseeing drive passing Phuket city and shopping. Overnight at your hotel"
		),
		"day2" => array(
			"Phuket (B/L/D)",
			"Breakfast at your hotel.",
			"Full day tour at Phi Phi Island and Ao Maya Bay Sea angel Cruise. Overnight at your hotel."
		),
		"day3" => array(
			"Phuket (B/L/D)",
			"Breakfast at your hotel.",
			"Full day tour at Pang Nga Bay, James Bond Island 007 - Panyee Island. Overnight at your hotel."
		),
		"day4" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival",
			"Round trip airport transfers",
			"Three nights hotel accommodation",
			"Full board meals",
			"Full day tour at Pang Nga Bay, James Bond Island 007 - Panyee Island",
			"Full day tour at Phi Phi Island and Ao Maya Bay Sea Angel Cruise"
		)
	)
);
?>