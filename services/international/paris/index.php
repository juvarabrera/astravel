<?php
$tourname = "Paris";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "3CDGDP",
		"daysnights" => "4 Days 3 Nights Discover Paris",
		"day1" => array(
			"Paris",
			"Meet and greet upon arrival and transfer to your Hotel. Check into Hotel. Check in Time is 1400hrs. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day2" => array(
			"Paris",
			"Breakfast at Hotel. Discover Paris at your leisure <b>2 Days Hop-on Hop-off Tour</b>. It is the easiest way to get around town. You can hop-on and off at any of the nine stops as often as you like over two day period. Get a bird's eye view of the cobbled streets of the Left Bank and the Napoleonic splendor of the Ecole Militaire. See the Eiffel Tower, Paris' most famous landmark. Stop off at the Arc de Triomphe and walk down the stylish Champs-Elysees. Become acquainted with some of the world's most famous artworks at the Louvre including the mysterious Mona Lisa. Your ticket will be valid for 2 consecutive days. Overnight at Hotel."
		),
		"day3" => array(
			"Paris",
			"Breakfast at Hotel. Continue with your 2nd Day of your Hop-on Hop-off Tour is the easiest way to get around town. Free at own leisure. Overnight at Hotel"
		),
		"day4" => array(
			"Paris",
			"Breakfast at Hotel. Day free until departure transfer to Airport or Train Station."
		),
		"inclusions" => array(
			""
		)
	)
);
?>