<?php
$tourname = "Vietnam";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2HANFE",
		"daysnights" => "3 Days 2 Nights Hanoi Free and Easy Package!",
		"day1" => array(
			"Arrival in Hanoi ",
			"Meet and greet upon arrival to Hanoi ",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Hanoi (B)",
			"Breakfast at your hotel.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival  ",
			"Round trip airport transfers ",
			"Two nights hotel accommodation",
			"Daily Breakfast"
		)
	),
	array(
		"code" => "2HANHP",
		"daysnights" => "3 Days 2 Nights Hanoi Package!",
		"day1" => array(
			"Arrival in Hanoi",
			"Meet and greet upon arrival to Hanoi",
			"Transfer to the hotel of your choice for check in.",
			"Half day Hanoi City Tour. Overnight at your hotel."
		),
		"day2" => array(
			"Hanoi - Halong (B/L/D)",
			"Breakfast at your hotel.",
			"Halong Bay Tour. Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Two nights hotel accommodation",
			"Full board meals",
			"Tour 1: Half day Hanoi City Tour",
			"Tour 2: Full day Halong Bay Tour"
		)
	),
	array(
		"code" => "3HANNV",
		"daysnights" => "4 Days 3 Nights Northern Vietnam Package!",
		"day1" => array(
			"Arrival in Hanoi",
			"Meet and greet upon arrival to Hanoi",
			"Transfer to the hotel of your choice for check in.",
			"Half day Hanoi City Tour. Overnight at your hotel."
		),
		"day2" => array(
			"Halong (B/L/D)",
			"Breakfast at your hotel.",
			"Halong Bay Tour. Overnight at your hotel."
		),
		"day3" => array(
			"Hua Lu (B/L/D)",
			"Breakfast at your hotel.",
			"Hua Lu Tour. Overnight at your hotel."
		),
		"day4" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Three nights hotel accommodation",
			"Full Board meals",
			"Half day Hanoi city tour",
			"Full day Halong Bay Yout",
			"Full day Hua Lu Tour"
		)
	),
	array(
		"code" => "2SGNFE",
		"daysnights" => "3 Days 2 Nights Ho Chi Minh (Saigon) Free and Easy Package!",
		"day1" => array(
			"Arrival in Ho Chi Minh City",
			"Meet and greet upon arrival to Ho Chi Minh City",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Ho Chi Minh City (B)",
			"Breakfast at your hotel.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Two nights hotel accommodation",
			"Daily Breakfast"
		)
	),
	array(
		"code" => "2GSNSP",
		"daysnights" => "3 Days 2 Nights Saigon Package!",
		"day1" => array(
			"Arrival in Ho Chi Minh City (D)",
			"Meet and greet upon arrival to Ho Chi Minh City",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Ho Chi Minh City (B/L/D)",
			"Breakfast at your hotel.",
			"Choice of Full day Cu Chi Tunnel Tour or Full day Mekong Delta Tour. Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Two nights hotel accommodation",
			"Full board meals",
			"Choice of Full day Cu Chi Tunnel Tour or Full day Mekong Delta Tour"
		)
	),
	array(
		"code" => "3SGNSP",
		"daysnights" => "4 Days 3 Nights Saigon Package!",
		"day1" => array(
			"Arrival in Ho Chi Minh City (D)",
			"Meet and greet upon arrival Ho Chi Minh City",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Ho Chi Minh City (B/L/D)",
			"Breakfast at your hotel.",
			"Full day Ho Chi Minh City Tour - Cu Chi Tunnel Tour. Overnight at your hotel."
		),
		"day3" => array(
			"Ho Chi Minh City (B/L/D)",
			"Breakfast at your hotel.",
			"Full day Ho Chi Minh City Tour - My Tho. Overnight at your hotel."
		),
		"day4" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Three nights hotel accommodation",
			"Full board meals",
			"Tour 1: Full day Ho Chi Minh City Tour - Cu Chi Tunnel Tour",
			"Tour 2: Full day Ho Chi Minh City Tour - My Tho"
		)
	),
	array(
		"code" => "4SGNFB",
		"daysnights" => "5 Days 4 Nights Saigon Full Blast Package!",
		"day1" => array(
			"Arrival in Ho Chi Minh City (D)",
			"Meet and greet upon arrival to Ho Chi Minh City",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Vung Tau (B/L/D)",
			"Breakfast at your hotel.",
			"Full day Cu Chi Tunnel Tour. Overnight at your hotel in Vung Tau."
		),
		"day3" => array(
			"Ho Chi Minh City (B/L/D)",
			"Breakfast at your hotel.",
			"Full day Ho Chi Minh Tour. Overnight at your hotel."
		),
		"day4" => array(
			"Ho Chi Minh City (B/L/D)",
			"Breakfast at your hotel.",
			"Full day My Tho Tour. Overnight at your hotel."
		),
		"day5" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Three nights hotel accommodation in Ho Chi Minh City",
			"One night hotel accommodation in Vung Tau",
			"Full board meals",
			"Tour 1: Full day Cu Chi Tunnel Tour",
			"Tour 2: Full day Ho Chi Minh Tour",
			"Tour 3: Full day My Tho Tour"
		)
	)
);
?>