<?php
$tourname = "Cambodia";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2PNHFE",
		"daysnights" => "3 Days 2 Nights Phnom Penh Free and Easy Package!",
		"day1" => array(
			"Arrival in Phnom Penh",
			"Meet and greet upon arrival to Phnom Penh.",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Phnom Penh",
			"Breakfast at your hotel.",
			"Free at your own leisure.",
			"Overnight stay at hotel."
		),
		"day3" => array(
			"Departure",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Phnom Penh",
			"Roundtrip airport transfers",
			"Two nights hotel accommodation",
			"Daily breakfast"
		)
	),
	array(
		"code" => "2REPFE",
		"daysnights" => "3 Days 2 Nights Siem Reap Free and Easy Package!",
		"day1" => array(
			"Arrival in Siem Reap",
			"Meet and greet upon arrival to Siem Reap.",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Siem Reap (B/L/D)",
			"Breakfast at your hotel.",
			"Siem Reap Tour. ( Angkor Thom - Ta Prohm Temple - Angkor Wat Temple)",
			"Overnight stay at hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Siem Reap",
			"Private return airport transfers",
			"Two nights hotel accommodation",
			"Daily breakfast, one lunch and one dinner",
			"Siem Reap City tour  ( Angkor Thom - Ta Prohm Temple - Angkor Wat Temple)"
		)
	),
	array(
		"code" => "4REPPNH",
		"daysnights" => "5 Days 4 Nights Cambodia (Siem Reap - Phnom Penh) Package!",
		"day1" => array(
			"Arrival in Siem Reap (B)",
			"Meet and greet upon arrival to Siem Reap.",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Siem Reap (B/L/D)",
			"Breakfast at your hotel.",
			"Siem Reap Tour. ( Angkor Thom - Ta Prohm Temple - Angkor Wat Temple)",
			"Overnight stay at hotel."
		),
		"day3" => array(
			"Siem Reap - Phnom Penh by SIC bus (B/L/D)",
			"Breakfast at your hotel.",
			"Drive to the floating village of Chong Khneas.",
			"Transfer to Phnom Penh by bus.",
			"Upon arrival at Phnom Penh, check in at your hotel and dinner at a local restaurant.",
			"Overnight stay at the hotel."
		),
		"day4" => array(
			"Phnom Penh (B/L/D)",
			"Breakfast at your hotel.",
			"Phnom Penh city tour"
		),
		"day5" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Siem Reap",
			"Private return airport transfers",
			"Four nights hotel accommodation",
			"Full board meals",
			"Bus ticket by A/C SIC bus from Siem Reap - Phnom Penh",
			"Sightseeing and Entrance fees",
			"Boat fee at Tonie Sap Great Lake",
			"English Speaking guide",
			"Cool towels and Drinking water serve during tours."
		)
	)
);
?>