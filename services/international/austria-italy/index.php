<?php
$tourname = "Austria/Italy";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "9VIEGA",
		"daysnights" => "10 Days 9 Nights Glamours of Austria and Italy",
		"day1" => array(
			"Vienna",
			"Meet and greet upon arrival and transfer to your Hotel. Check into Hotel by 1400hrs. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day2" => array(
			"Vienna - Salzburg",
			"Breakfast at Hotel. Take the <b>Morning City Tour & Schönbrunn (SIC Basis)</b> (Pick up from most Hotels in Vienna City). This tour will give you an overall impression of the most significant historical sights of Vienna. Along the Ringstrasse we show you numerous grand buildings, such as the MAK, the State Opera House, the magnificent Museum of Fine Arts with its world famous art treasures of the Habsburgs and the Natural History Museum. The cultural district the Museums Quartier is home to the Hofburg (the former Habsburg winter residence), the Parliament, the City Hall and the Burg Theatre. The highlight of this tour will be a visit of the showrooms of Schönbrunn Palace; once the summer residence of the Habsburg Family and home of Maria Theresia. On the way back to the Opera we pass by the Belvedere Palace. Later return to your Hotel. In the afternoon we will depart to <b>Salzburg (Standard Train Travel Included)</b>. Meet upon arrival in Salzburg and transfer to your Hotel. Overnight at Hotel."
		),
		"day3" => array(
			"Salzburg - Innsbruck",
			"Breakfast at Hotel. In the morning you will have a <b>Half Day Salzburg City Tour of Sound (SIC Basis)</b> (You have to make your own way to the starting point of this tour at Mirabellplatz, directly in front of St. Andrä Church). This wonderful ride will show you the breathtaking views of the landscape where the opening scenes of the film “Sound of Music” were filmed. Relax and listen to the original “Sound of Music” soundtrack. Our English speaking guide not only shows you the highlights of the film “Sound of Music” but also the historical and architectural landmarks in the City, as well as the picturesque Lake District. The tour will last for around 4 hours. Later return to your Hotel on your own. In the afternoon departure transfer to the train station to connect to your <b>train to Innsbruck (Standard Train Travel Included)</b>. Arrive in Innsbruck and transfer to your Hotel. Check into Hotel. Overnight at Hotel."
		),
		"day4" => array(
			"Innsbruck",
			"Breakfast at Hotel. Today is free to just explore this breathtaking town in the Alps, visit the Crystal Factory Showroom or just relax. Overnight at Hotel."
		),
		"day5" => array(
			"Innsbruck - Venice",
			"Breakfast at Hotel. Take the departure transfer to the train station to connect your <b>train to Venice (Standard Train Travel Included)</b>. Meet upon arrival in Venice St Lucia Station and transfer to your Hotel. Check into Hotel. Overnight at Hotel."
		),
		"day6" => array(
			"Venice",
			"Breakfast at Hotel. In the morning make your own way to the Tourist Office, Plaza San Marcos by 0900hrs. Here you will have a <b>Guided Walking Tour of Venice (SIC)</b>. This interesting tour is centred around the history of St. Mark's Square and its main monuments. We will be visiting the St. Mark's Basilica with its marble and mosaics, and the Ducal Palace where the old Republic of Venice exercised their political and commercial powers in the Doge Palace. Then cross the Bridge of Sighs arriving at the famous Venice Jail. The rest of the afternoon free to explore Venice. Overnight at Hotel."
		),
		"day7" => array(
			"Venice - Florence",
			"Breakfast at Hotel. Check out of Hotel. Join the departure transfer to train station to connect <b>train to Florence (Standard Train Travel Included)</b>. Arrive and transfer to your Hotel by afternoon. Check into Hotel. Then you can make your own way to join the <b>Hop On Hop Off Tour of Florence</b>. Explore Florence with 2 routes on this Hop On Hop Off tour. In the romantic region of Tuscany lies the jewel city of the Renaissance, Florence. Surrounded by the Appenine Mountain chain it lies in a fertile valley surrounded by the charming Tuscan vineyards and olive groves. The art galleries and museums of Florence are renowned throughout the world and holidaymakers flock to admire the superb David of Michaelangelo, the Venus of Botticelli and the stunning Duomo with its marble façade which glows in the golden sunlight. The start point for Line A is S M Novella Station and for Line B is S. Frediano. Make your own way back to your Hotel. Overnight at Hotel."
		),
		"day8" => array(
			"Florence - Rome",
			"Breakfast at Hotel. Continue on your <b>Hop On Hop Off Tour of Florence</b>. Explore Florence with 2 routes on this Hop On Hop Off tour. In the romantic region of Tuscany lies the jewel city of the Renaissance, Florence. Surrounded by the Appenine Mountain chain it lies in a fertile valley surrounded by the charming Tuscan vineyards and olive groves. The art galleries and museums of Florence are renowned throughout the world and holidaymakers flock to admire the superb David of Michelangelo, the Venus of Botticelli and the stunning Duomo with its marble façade which glows in the golden sunlight. The start point for Line A is S M Novella Station and for Line B is S. Frediano. Later return to your Hotel and you will be transferred at around 2pm to Florence Train Station to connect your train to <b>Rome (Standard Train Travel Included)</b>. Arrive in Rome and you will be transferred to your Hotel. Overnight at Hotel."
		),
		"day9" => array(
			"Rome",
			"Breakfast at Hotel. <b>Discover Rome with Hop On Hop Off 24hours Ticket</b>. There is no better way to see Rome than on a double-decker, hop-on hop-off bus! See top Rome attractions on this comprehensive sightseeing tour, such as Vatican City, the Colosseum, Trevi Fountain, Tiber Island and much more.. One of the world’s most compelling cities, Rome has more to see than most other capitals all put together. Layers of history have left the city with architectural treasures scattered at every twist and turn, and its Baroque piazzas and ancient antiquities are a joy to behold.. Aboard a comfortable, open-top, double-decker bus, admire top city attractions, such as Rome’s must-see Colosseum - the scene of gory gladiator battles during the Roman Empire - and Piazza Venezia with its ostentatious Monument to Vittorio Emanuele II. Pass downtown streets near the eye-catching Trevi Fountain or perhaps hop off at Vatican City to visit one of the most sacred places in Christendom. With your ticket, you can remain on the bus for an entire loop (roughly 1.5-2 hours) as you listen to the informative audio commentary, or you can hop on and off at any of the eight stops around the city to explore Rome’s countless delights. Overnight at Hotel"
		),
		"day10" => array(
			"Rome",
			"Breakfast at Hotel. Day free at leisure until departure transfer to Rome Airport for your onward journey."
		),
		"inclusions" => array(
			"9 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Return Airport or Train Station Transfers",
			"Train Travel - Vienna - Salzburg - Innsbruck - Venice - Florence - Rome",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program"
		)
	)
);
?>