<?php
$tourname = "Nepal";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "3KTMNP",
		"daysnights" => "4 Days 3 Nights Nepal Package!",
		"day1" => array(
			"Arrival in Kathmandu (D)",
			"Meet and greet upon arrival to Kathmandu",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel"
		),
		"day2" => array(
			"Kathmandu - Nagarkot (B/L/D)",
			"Breakfast at your hotel.",
			"Kathmandu City Tour (Durbar Square Complex of Palaces, Courtyards and Temples [UNESCO world heritage site] - Swoyambhunath Stupa).",
			"Drive toward Nagarkot. Overnight at your hotel"
		),
		"day3" => array(
			"Nagarkot - Kathmandu (B/L/D)",
			"Breakfast at your hotel.",
			"Drive back to Kathmandu, enroute Bhaktapur City.",
			"Visit Pashupatinath temple - Boudnath Stupa. Overnight at your hotel"
		),
		"day4" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out"
		),
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Three nights hotel accommodation",
			"Air conditioned Transport as per itinerary.",
			"All meals as per itinerary",
			"All applicable Gov’t Taxes on Hotels and Transports(Nepal)",
			"Gov’t Service tax(Nepal)"
		)
	),
	array(
		"code" => "4KTMNP",
		"daysnights" => "5 Days 4 Nights Nepal Package!<br><br>Kathmandu - Nagarkot - Kathmandu",
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Three nights hotel accommodation",
			"Air conditioned Transport as per itinerary.",
			"All meals as per itinerary",
			"All applicable Gov’t Taxes on Hotels and Transports(Nepal)",
			"Gov’t Service tax(Nepal)"
		)
	),
	array(
		"code" => "5KTMNP",
		"daysnights" => "6 ays 5 Nights Nepal Package!<br><br>Kathmandu - Pokhara - Nagarkot - Kathmandu",
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Three nights hotel accommodation",
			"Air conditioned Transport as per itinerary.",
			"All meals as per itinerary",
			"All applicable Gov’t Taxes on Hotels and Transports(Nepal)",
			"Gov’t Service tax(Nepal)"
		)
	),
	array(
		"code" => "6KTMNP",
		"daysnights" => "7 Days 6 Nights Nepal Package!<br><br>Kathmandu - Pokhara - Nagarkot - Kathmandu",
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Three nights hotel accommodation",
			"Air conditioned Transport as per itinerary.",
			"All meals as per itinerary",
			"All applicable Gov’t Taxes on Hotels and Transports(Nepal)",
			"Gov’t Service tax(Nepal)"
		)
	),
	array(
		"code" => "7KTMNP",
		"daysnights" => "8 Days 7 Nights Nepal Package!<br><br>Kathmandu - Chitwan - Pokhara - Kathmandu",
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Three nights hotel accommodation",
			"Air conditioned Transport as per itinerary.",
			"All meals as per itinerary",
			"All applicable Gov’t Taxes on Hotels and Transports(Nepal)",
			"Gov’t Service tax(Nepal)"
		)
	)
);
?>