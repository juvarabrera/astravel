<?php
$tourname = "United Arab Emirates";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "3AUHAP",
		"daysnights" => "4 Days 3 Nights Abu Dhabi Free and Easy Package!",
		"day1" => array(
			"Arrival",
			"Meet and greet upon arrival",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Abu Dhabi (B)",
			"Breakfast at your hotel.",
			"Abu Dhabi City Tour. Overnight at your hotel."
		),
		"day3" => array(
			"Abu Dhabi (B)",
			"Breakfast at your hotel.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day4" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"-	Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Three nights hotel accommodation",
			"Daily Breakfast",
			"Abu Dhabi City Tour"
		)
	),
	array(
		"code" => "3DXBFE",
		"daysnights" => "3 Days 2 Nights Dubai Free and Easy Package!",
		"day1" => array(
			"Arrival",
			"Meet and greet upon arrival",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Dubai (B)",
			"Breakfast at your hotel.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival",
			"Round trip airport transfers",
			"Two nights hotel accommodation",
			"Daily Breakfast"
		)
	)/*,
	array(
		"code" => "3DXBFE",
		"daysnights" => "4 Days 3 Nights Greece Package with Cruise!",
		"inclusions" => array(
			"Meet and greet services upon arrival",
			"Round trip airport transfers",
			"Three nights hotel accommodation",
			"Daily Breakfast"
		)
	),
	array(
		"code" => "4DXBFE",
		"daysnights" => "5 Days 4 Nights Greece Package with Cruise!",
		"inclusions" => array(
			"Meet and greet services upon arrival",
			"Round trip airport transfers",
			"Three nights hotel accommodation",
			"Daily Breakfast"
		)
	)*/
);
?>