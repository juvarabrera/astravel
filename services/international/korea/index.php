<?php
$tourname = "Korea";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2INCFE",
		"daysnights" => "3 Days 2 Nights Free and Easy Package",
		"day1" => array(
			"Arrival in Incheon",
			"Meet and greet upon arrival to Incheon(driver only)",
			"Transfer to the hotel of your choice for check in.(2pm)",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Seoul (B,L)",
			"Breakfast at the Hotel.",
			"Tour at Gyongbok Palace, and National Folk Museum. Drive pass the Presidential Blue House, Cheong Wa Dae Known as the Blue House, is the official residence of the President of Korea.",
			"Visit Korean Ginseng Center. Visit Cosmetic Shop. Shopping",
			"Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast Outside.",
			"Free at own leisure until you transfer out to the airport for your flight out"
		),
		"inclusions" => array(
			"Meet and greet services upon arrival",
			"Round trip airport transfers",
			"Two nights hotel accommodation",
			"Daily Breakfast"
		)
	),
	array(
		"code" => "3INCSP",
		"daysnights" => "4 Days 3 Nights Seoul Package",
		"day1" => array(
			"Arrival in Incheon",
			"Meet and greet upon arrival to Incheon. (Tour Guide)",
			"Breakfast for U-dong Noodle at local restaurant.",
			"Transfer to Petite France - My love from The Star shooting place and visit Nami Island by ferry and visit Winter Sonata’s shooting place.",
			"Transfer to Seoul, free shopping at Dongademun Market & Dongdaemun Design Plaza.",
			"Transfer to the hotel of your choice for check in.",
			"Overnight at your hotel."
		),
		"day2" => array(
			"(B, L, D)",
			"Breakfast at hotel.",
			"Visit Kimchi-making session, where you also have the opportunity to take photos while dressed in Hanbok.",
			"Transfer to Yongin and enjoy unlimited ticket at Everland Theme Park –(if rainy, Everland change to Lotte World).",
			"Overnight at your hotel."
		),
		"day3" => array(
			"(B, L, D)",
			"Breakfast at hotel.",
			"Seoul City Tour – Gyeongbok Palace, National Folk Museum and drive by Cheong Wa Dae know as the blue house, is the official residence of the President of Korea.",
			"Proceed to Ice Gallery (Indoor ice carving gallery-the ice sled, the ice castle, the aquarium, the North Pole section, the ice flower bed, the photo zone etc.",
			"Visit Ginseng shop and the Face shop and Duty free shop. Shopping at Myeongdong street.",
			"Overnight at your hotel."
		),
		"day4" => array(
			"Departure (B, L)",
			"Breakfast at hotel.",
			"Transfer to N Seoul Tower Observatory lift and Alive Museum, Love locks shooting location of \"My love from the star\".",
			"Visit Korean Red Pine Tree, amethyst factory.",
			"Transfer to Incheon Int'l Airport for departure.",
			"Overnight at your hotel."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival",
			"Round trip airport transfers",
			"Three nights hotel accommodation",
			"Meals as per itinerary",
			"Tours as per itinerary",
			"Tour Guide"
		)
	)
);
?>