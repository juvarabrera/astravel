<?php
$tourname = "Greece";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "4ATHGP",
		"daysnights" => "5 Days 4 Nights Greece Package",
		"day1" => array(
			"Arrival in Athens (D)",
			"Meet and greet upon arrival to Athens",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Athens (B/D)",
			"Breakfast at your hotel.",
			"Half day Athens Tour (Parliament House - Tom of the Unknown Soldier - Panathenian Stadium).",
			"Visit the Acropolis - Olympian Zeus.",
			"Visit Cape Sounion’s Tour. Overnight at your hotel."
		),
		"day3" => array(
			"Athens - Delphi - Kalambaka (B/D)",
			"Breakfast at your hotel.",
			"Depart Athens, drive through Thebes - Levadia - Arachova - Delphi.",
			"Drive through Amphissa, Lamia and Trikala and arrive in Kalambaka. Overnight at your hotel."
		),
		"day4" => array(
			"Kalambaka - Mateora - Athens (B/L/D)",
			"Breakfast at your hotel.",
			"Drive to Meteora - Athens (Thermopylae). Overnight at your hotel."
		),
		"day5" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Athens",
			"Round trip airport transfers",
			"Four nights hotel accommodation",
			"Tours as per itinerary",
			"Meals as per itinerary",
			"Entance fees to archeological sites in Athens, Delphi and Meteora",
			"Porter services at hotels and buses"
		)
	),
	array(
		"code" => "6ATHGP",
		"daysnights" => "7 Days 6 Nights Greece Package with Cruise!<br><br>Athens - Olympia - Delphi - Meteora - Athens",
		"inclusions" => array(
			"Meet and greet services upon arrival at Athens",
			"Round trip airport transfers",
			"Four nights hotel accommodation",
			"Tours as per itinerary",
			"Meals as per itinerary",
			"Entance fees to archeological sites in Athens, Delphi and Meteora",
			"Porter services at hotels and buses"
		)
	),
	array(
		"code" => "7ATHGP",
		"daysnights" => "8 Days 7 Nights Greece Package with Cruise!<br><br>Athens - Delphi - Meteora - Santorini - Akrotiri - Athens",
		"inclusions" => array(
			"Meet and greet services upon arrival at Athens",
			"Round trip airport transfers",
			"Four nights hotel accommodation",
			"Tours as per itinerary",
			"Meals as per itinerary",
			"Entance fees to archeological sites in Athens, Delphi and Meteora",
			"Porter services at hotels and buses"
		)
	)
);
?>