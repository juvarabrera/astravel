<?php
$tourname = "Myanmar";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2RGNFE",
		"daysnights" => "3 Days 2 Nights Yangon Free and Easy Package!",
		"day1" => array(
			"Arrival in Yangon",
			"Meet and greet upon arrival to Yangon",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Yangon (B/L)",
			"Breakfast at your hotel.",
			"Yangon city tour with lunch. Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Yangon",
			"Round trip airport transfers ",
			"Two nights hotel accommodation",
			"Daily Breakfast, One lunch at a local restaurant",
			"Yangon City Tour"
		)
	),
	array(
		"code" => "2RGNHP",
		"daysnights" => "3 Days 2 Nights Yangon Highlight Package!",
		"day1" => array(
			"Arrival in Yangon",
			"Meet and greet upon arrival to Yangon",
			"Transfer to the hotel of your choice for check in.",
			"Afternoon city tour. Overnight at your hotel."
		),
		"day2" => array(
			"Yangon (B)",
			"Breakfast at your hotel.",
			"Yangon Full day tour. Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Yangon",
			"Round trip airport transfers ",
			"Two nights hotel accommodation",
			"Daily Breakfast",
			"Yangon City Tour",
			"(2bottles drinking water + 2 refreshing tower) per per person per day",
			"English Speaking Station tour guide service "
		)
	),
	array(
		"code" => "2RGNRP",
		"daysnights" => "4 Days 3 Nights Yangon Golden Rock Package!",
		"day1" => array(
			"Arrival in Yangon",
			"Meet and greet upon arrival to Yangon",
			"Transfer to the hotel of your choice for check in.",
			"Afternoon city tour. Overnight at your hotel."
		),
		"day2" => array(
			"Yangon (B)",
			"Breakfast at your hotel.",
			"Kyaikhtiyo (Golden Rock) tour (by car). Overnight at your hotel."
		),
		"day3" => array(
			"Yangon (B)",
			"Breakfast at your hotel.",
			"Bago Tour (by car). Overnight at your hotel."
		),
		"day4" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Thanlyin Tour(by car and boat)",
			"Transfer to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival at Yangon",
			"Round trip airport transfers ",
			"Three nights hotel accommodation",
			"Daily Breakfast",
			"Yangon City Tour + Kyaikhtiyo Tour + Bago tour + Thanlyin Tour",
			"(2bottles drinking water + 2 refreshing tower) per per person per day",
			"English Speaking Station tour guide service",
			"Truck Car at Kyaikhtiyo Trip as per program",
			"Boat fare as per program"
		)
	)
);
?>