<?php
$tourname = "Scandinavia";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "6HELSC - R",
		"daysnights" => "7 Days 6 Nights Scandinavian Capitals",
		"day1" => array(
			"Helsinki",
			"Arrive in Helsinki, the capital of Finland. Make your own way to your hotel. Check into Hotel by 1400hrs. Rest of the afternoon free at leisure. Overnight at Hotel."
		),
		"day2" => array(
			"Helsinki - Stockholm",
			"Breakfast at Hotel. Take the Guided City Tour of Helsinki (SIC Basis) . This sightseeing tour with live commentary by an authorised guide in English and Swedish. The many sights include the Senate Square, Uspenski Cathedral, the Parliament Building and the Olympic Stadium. Stops are made at the Sibelius Monument and, when possible, at the Temppeliaukio Church along with many more sights. Afternoon free at leisure and then in the late afternoon make your own way to Helsinki Port to connect to your overnight Cruise Ferry to Stockholm. Overnight on Cruise Ferry."
		),
		"day3" => array(
			"Stockholm",
			"Breakfast on board Cruise. Arrive in Stockholm, the Capital of Scandinavia. Arrive and make your own way to your Hotel. Leave your baggage if room is not ready. The official check in time is 1400hrs. Make your own way to the starting point at Gustav Adolfs Torg. Stockholm Panorama Tour. The tour show you the most famous buildings and views, including a stop at Fjällgatan, where the view of Stockholm’s islands and waters is truly wonderful. You will see the Old Town which is considered to be the heart of Stockholm. Kansen is the worlds largest outdoor museum. Stockholm City Hall is perhaps the City's most distinguished feature. The Vasa is the world’s only surviving 17 th Century ship and one of the foremost tourist sights in the world. Return to your Hotel on your own. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day4" => array(
			"Stockholm - Copenhagen",
			"Breakfast at Hotel. You are free to discover Stockholm in the morning and visit the famous shopping streets and malls. In the afternoon, make your own way to train station to connect to a 5 hour train journey to reach the lovely city of Copenhagen, the capital of Denmark. Before your train arrives do not miss the famous Öresund bridge connecting Sweden with Denmark. Arrive and make your own way to your Hotel. Overnight at Hotel."
		),
		"day5" => array(
			"Copenhagen - Oslo",
			"Breakfast at Hotel. Check out of Hotel and leave your baggage at reception. Then make your own way to the starting point of Hop on Hop Off Classic Tour of Copenhagen (SIC Basis). We call this tour Line A. On this tour you will see Amalienborg Palace, Gefi on Fountain, Langelinie, Nyhavn, Strøget, Christiansborg, Nyboder, Rosenborg Castle, Tivoli, Botanical Gardens, the Royal Library, the exciting replacement for the Little Mermaid and much more. In the late afternoon make your own way to Copenhagen Port to connect your overnight cruise to Oslo. At 1600hrs board the Cruise for Oslo for a wonderful cruise on the North Sea and a night long party. Overnight on Cruise."
		),
		"day6" => array(
			"Oslo",
			"Breakfast on board Cruise. Arrive in Oslo by 0930hrs. Arrive and make your own way to your Hotel. Check into Hotel by 1400hrs. Rest of the day free at leisure to explore this City on your own. Overnight at Hotel."
		),
		"day7" => array(
			"Oslo",
			"Breakfast at Hotel. Make your own way to connect your flight to your onward destination."
		),
		"inclusions" => array(
			"6 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Train travel between Stockholm - Copenhagen",
			"Overnight Cruise from Helsinki - Stockholm & Copenhagen - Oslo",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program"
		)
	),
	array(
		"code" => "5ARNDS",
		"daysnights" => "6 Days 5 Nights Discover Scandinavia",
		"day1" => array(
			"Stockholm",
			"Meet upon arrival at Arlanda Airport and transfer to your Hotel. Check into Hotel by 1400hrs. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day2" => array(
			"Stockholm - Copenhagen",
			"Breakfast at Hotel. Today you are free to discover <b>Stockholm's famous shopping streets and malls</b>. In the afternoon, join a departure transfer to the train station to connect to a 5 hour <b>train journey to reach the lovely city of Copenhagen</b>, the capital of Denmark <b>(Standard Train Travel Included)</b>. Before your train arrives do not miss the famous Öresund bridge connecting Sweden with Denmark. Copenhagen got its name from the word \"köbenhavn\", which means \"Merchants Harbour\". It is a captivating, friendly City where there is always something wonderful around the corner to see, smell, taste, feel and listen to. Meet upon arrival and transfer to your Hotel. Overnight at Hotel."
		),
		"day3" => array(
			"Copenhagen - Oslo",
			"Breakfast at Hotel. Check out of Hotel and leave your baggage at reception. Then make your own way to the starting point of <b>Hop on Hop Off Classic Tour of Copenhagen (SIC Basis)</b>. Copenhagen is a major regional centre of culture, business, media, and science. Find out why they call it wonderful Copenhagen with your Hop On Hop tour of Denmark's vibrant and graceful Capital and view Copenhagen's swirling towers and turrets. We call this tour Line A. On this tour you will see Amalienborg Palace, Gefi on Fountain, Langelinie, Nyhavn, Strøget, Christiansborg, Nyboder, Rosenborg Castle, Tivoli, Botanical Gardens, the Royal Library, the exciting replacement for the Little Mermaid and much more. In the late afternoon departure transfer to Copenhagen Port to connect your overnight cruise to Oslo. At 1600hrs board the Cruise for Oslo for a wonderful ride on the North Sea and a night long party. Overnight on Cruise."
		),
		"day4" => array(
			"Oslo",
			"Breakfast on board Cruise. Arrive in Oslo by 0930hrs. You will be met upon arrival and transferred to your Hotel. Check into Hotel by 1400hrs. Rest of the day you are free to explore this wonderful city. Overnight at Hotel."
		),
		"day5" => array(
			"Oslo - Stockholm",
			"Breakfast at Hotel. Prepare for your departure transfer to Oslo Train Station to connect to your <b>train to Stockholm (Standard Train Travel Included)</b>. Arrive in Stockholm and transfer to your Hotel. Check into Hotel. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day6" => array(
			"Stockholm",
			"Breakfast at Hotel. Day free at leisure until departure transfer to Arlanda Airport."
		),
		"inclusions" => array(
			"5 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Return Airport or Train Station or Port Transfers in all Cities",
			"Train travel between Stockholm - Copenhagen & Oslo - Stockholm",
			"Overnight Cruise from Copenhagen - Oslo",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program"
		)
	),
	array(
		"code" => "6HELCS",
		"daysnights" => "7 Days 6 Nights Capitals of Scandinavia",
		"day1" => array(
			"Helsinki",
			"Arrival in Helsinki, the capital of Finland. Meet and greet and transfer from Vantaa Airport to your Hotel. Check into Hotel by 1400hrs. Rest of the afternoon free at leisure. Overnight at Hotel."
		),
		"day2" => array(
			"Helsinki - Stockholm",
			"Breakfast at Hotel. Take the <b>Guided City Tour of Helsinki (SIC Basis)</b>. This sightseeing tour with live commentary by an authorised guide in English and Swedish. The many sights include the Senate Square, Uspenski Cathedral, the Parliament Building and the Olympic Stadium. Stops are made at the Sibelius Monument and, when possible, at the Temppeliaukio Church along with many more sights. Afternoon free at leisure and then in the late afternoon departure transfer to Helsinki Port to connect to your overnight <b>Cruise Ferry to Stockholm</b>. Overnight on Cruise Ferry."
		),
		"day3" => array(
			"Stockholm",
			"Breakfast on board Cruise. Arrive in Stockholm, the Capital of Scandinavia at 09:30. Arrive and transfer to your Hotel. Leave your baggage if room is not ready. The official check in time is 1400hrs. Make your own way to the starting point at Gustav Adolfs Torg. <b>Stockholm Panorama Tour</b> starts at 1200hrs. The tour show you the most famous buildings and views, including a stop at Fjällgatan, where the view of Stockholm’s islands and waters is truly wonderful. You will see the Old Town which is considered to be the heart of Stockholm. Kansen is the worlds largest outdoor museum. Stockholm City Hall is perhaps the City's most distinguished feature. The Vasa is the world’s only surviving 17 th Century ship and one of the foremost tourist sights in the world. Return to your Hotel on your own. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day4" => array(
			"Stockholm - Copenhagen",
			"Breakfast at Hotel. You are free to discover Stockholm in the morning and visit the famous shopping streets and malls. In the afternoon, departure transfer to train station to Copenhagen, the capital of Denmark. Before your train arrives do not miss the famous Öresund bridge connecting Sweden with Denmark. Copenhagen got its name from the word \"köbenhavn\", which means Merchants Harbour. It is a captivating, friendly city where there is always something wonderful around the corner to see, smell, taste, feel and listen to. Meet upon arrival and transfer to your Hotel. Overnight at Hotel."
		),
		"day5" => array(
			"Copenhagen - Oslo",
			"Breakfast at Hotel. Check out of Hotel and leave your baggage at reception. Then make your own way to the starting point of <b>Hop on Hop Off Classic Tour of Copenhagen (SIC Basis)</b>. On this tour you will see Amalienborg Palace, Gefi on Fountain, Langelinie, Nyhavn, Strøget, Christiansborg, Nyboder, Rosenborg Castle, Tivoli, Botanical Gardens, the Royal Library, the exciting replacement for the Little Mermaid and much more. In the late afternoon departure transfer to Copenhagen Port to connect your overnight cruise to Oslo. At 1600hrs board the <b>Cruise for Oslo</b> for a wonderful cruise on the North Sea and a night long party. Overnight on Cruise <b>(Standard Ferry Travel Included)</b>."
		),
		"day6" => array(
			"Oslo",
			"Breakfast on board Cruise. Arrive in Oslo by 0930hrs. You will be met upon arrival and transferred to your Hotel. Check into Hotel by 1400hrs. Rest of the day free at leisure to explore this City on your own. Overnight at Hotel."
		),
		"day7" => array(
			"Oslo",
			"Breakfast at Hotel. Day free at leisure for departure to Oslo Airport to connect your flight to your onward destination."
		),
		"inclusions" => array(
			"6 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Return Airport or Train Station or Port Transfers in all Cities",
			"Train travel between Stockholm - Copenhagen",
			"Overnight Cruise from Helsinki - Stockholm & Copenhagen - Oslo",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program"
		)
	)
);
?>