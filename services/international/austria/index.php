<?php
$tourname = "Austria";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "4VIEBA",
		"daysnights" => "6 Days 5 Nights Best of Austria",
		"day1" => array(
			"Vienna",
			"Meet and greet upon arrival and transfer to your Hotel. Check into Hotel by 1400hrs. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day2" => array(
			"Vienna - Salzburg",
			"Breakfast at Hotel. Take a <b>Morning City Tour & Schönbrunn (SIC Basis)</b> <i>(Pick up from most Hotels in Vienna City)</i>. This tour will give you an overall impression of the most significant historical sights of Vienna. Along the Ringstrasse, we show you numerous grand buildings, such as the MAK, the State Opera House, the magnificent Museum of Fine Arts with its world famous art treasures of the Habsburgs and the Natural History Museum. You also will get the chance to see the cultural district where the Museums Quartier, the Hofburg (the former Habsburg winter residence), the Parliament, the City Hall and the Burg theater are all located. The highlight of this tour will be a visit to the showrooms of Schönbrunn Palace. Once the summer residence of the Habsburg family and home of Maria Theresia. On the way back to the Opera we pass by the Belvedere Palace. Later, return to your Hotel. In the afternoon we will depart to <b>Salzburg (Standard Train Travel Included)</b>. Meet upon arrival in Salzburg and transfer to your Hotel. Overnight at Hotel."
		),
		"day3" => array(
			"Salzburg - Innsbruck",
			"Breakfast at Hotel. In the morning you will go on a <b>Half Day Salzburg City Tour of Sound (SIC Basis)</b>. <i>(You will have to make your own way to the starting point of this tour at Mirabellplatz, directly in front of St. Andrä Church)</i>. This wonderful ride lets you witness the breathtaking views of the landscape where the opening scenes of the film “Sound of Music” were filmed. Relax and listen to the original “Sound of Music” soundtrack. Our English-speaking guide not only shows you the highlights of the film “Sound of Music”, but also the historical and architectural landmarks in the city, as well as a part of the picturesque Lake District. The tour will last around 4 hours. Later, return to your Hotel on your own. In the afternoon you will have a departure transfer to train station to connect to your <b>train to Innsbruck (Standard Train Travel Included)</b>. Arrive in Innsbruck and transfer to your Hotel. Check into Hotel. Overnight at Hotel."
		),
		"day4" => array(
			"Innsbruck",
			"Breakfast at Hotel. Today is free to just explore this breathtaking town in the Alps. Visit the Crystal Factory Showroom or just relax. Overnight at Hotel."
		),
		"day5" => array(
			"Innsbruck - Vienna",
			"Breakfast at Hotel. Prepare for your departure transfer to train station to connect your <b>train to Vienna (Standard Train Travel Included)</b>. Upon arrival, transfer to your Hotel. Check into Hotel by 1400hrs. Rest of the afternoon free at leisure. Overnight at Hotel."
		),
		"day6" => array(
			"Vienna",
			"Breakfast at Hotel. Day free at leisure until departure transfer to Airport."
		),
		"inclusions" => array(
			"5 Nights Accommodation at Hotels as mentioned or similar",
			"Breakfast Daily",
			"Return Airport or Train Station Transfers",
			"Train Travel - Vienna - Salzburg - Innsbruck - Vienna",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program"
		)
	)
);
?>