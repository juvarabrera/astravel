<?php
$tourname = "Bhutan";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2PBHBP",
		"daysnights" => "4 Days 3 Nights Bhutan Package",
		"day1" => array(
			"Arrival in Paro",
			"Meet and greet upon arrival to Paro.",
			"Transfer to Thimpu. Upon arrival, Proceed to visit National memorial Chorten - Trashichhoedzong",
			"Overnight at your hotel in Thimpu."

		),
		"day2" => array(
			"Thimpu - Paro (B)",
			"Breakfast at your hotel.",
			"Thimpu City Tour. Overnight at your hotel in Paro."
		),
		"day3" => array(
			"Paro (B)",
			"Breakfast at your hotel.",
			"Paro Tour(Taktshang Lhakhang - Kyichu Lhakhang). Overnight at your hotel in Paro."
		),
		"day4" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival",
			"Round trip airport transfers",
			"Three nights hotel accommodation",
			"Daily Breakfast",
			"Tour 1: National memorial Chorten - Trashichhoedzong",
			"Tour 2: Thimpu City Tour",
			"Tour 3: Paro Tour(Taktshang Lhakhang - Kyichu Lhakhang)",
			"All transfers and sighseeing as per the itinerary",
			"English speaking guide",
			"Entrance fees",
			"Gov’t Royalty and Taxes(Bhutan)"
		)
	),
	array(
		"code" => "4PBHBP",
		"daysnights" => "6 Days 5 Nights Bhutan Package",
		"day1" => array(
			"Arrival in Paro.",
			"Meet and greet upon arrival to Paro.",
			"Transfer to Thimpu. Upon arrival, Proceed to visit National memorial Chorten - Trashichhoedzong.",
			"Overnight at your hotel in Thimpu."

		),
		"day2" => array(
			"Thimpu (B)",
			"Breakfast at your hotel",
			"Thimpu City Tour. Overnight at your hotel in Thimpu"
		),
		"day3" => array(
			"Thimpu - Punakha (75km, 3hrs drive) (B)",
			"Breakfast at your hotel",
			"Drive up to Dochu-la pass - chorten - mani wall - prayer flags",
			"Ecursion to Chimi Lhakhang",
			"Visit Punakha Dzong. Overnight at your hotel in Punakha"
		),
		"day4" => array(
			"Punakha - Paro (B)",
			"Breakfast at your hotel",
			"Drive to Paro",
			"Visit Ta Dzong (Bhutan’s National Museum) - Rinpung Dzong",
			"Overnight at your hotel in Paro"
		),
		"day5" => array(
			"Paro (B)",
			"Breakfast at your Hotel",
			"Morning excursion to Takshang Lhakhang",
			"Visit Kyichu Lhakhang (one of the oldest monasteries of the Kingdom",
			"vernight in Paro"
		),
		"day6" => array(
			"Departure (B)",
			"Breakfast at your hotel",
			"Free at own leisure until you transfer out to the airport for your flight out"
		),
		"inclusions" => array(
			"Meet and greet services upon arrival",
			"Round trip airport transfers",
			"Five nights hotel accommodation",
			"Daily Breakfast",
			"Tour 1: National memorial Chorten - Trashichhoedzong",
			"Tour 2: Thimpu City Tour",
			"Tour 3: Punakha Tour",
			"Tour 4: Ta Dzong - Rinpung Dzong",
			"Tour 5: Paro Tour(Taktshang Lhakhang - Kyichu Lhakhang)",
			"All transfers and sighseeing as per the itinerary",
			"English speaking guide",
			"Entrance fees",
			"Gov’t Royalty and Taxes(Bhutan)"
		)
	)
);
?>