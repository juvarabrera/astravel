<?php
$tourname = "London/Scotland";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "5LHRLS",
		"daysnights" => "6 Days 5 Nights Discover London & Scotland",
		"day1" => array(
			"London",
			"Arrival transfer at London Heathrow Airport to your Hotel. Check into Hotel by 1400hrs. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day2" => array(
			"London",
			"Breakfast at Hotel. <b>The Original London Sightseeing Tour: Hop-on Hop-off 24hours Ticket</b> - See top London attractions at your own pace on a comprehensive hop-on hop-off sightseeing tour. See all the top sights like the London Eye, Big Ben, Westminster Abbey, Tower Bridge and more. Redeem your voucher, and then ride around London aboard your spacious open-top double- decker bus. Free at own leisure. Overnight at Hotel."
		),
		"day3" => array(
			"London - Edinburgh",
			"Breakfast at Hotel, departure transfer to train station to connect your train to <b>Edinburgh (Standard Class Train Ticket Included)</b>. Arrive and transfer to your Hotel. Check into Hotel by 1400hrs. Rest of the day free at leisure. Overnight at Hotel."
		),
		"day4" => array(
			"Edinburgh",
			"Breakfast at Hotel. <b>Hop On Hop Off Edinburgh Tour with 1 Day Ticket</b> - Make your own way to the starting point of the tour which is Hanover Street. Compare Edinburgh's Old Town, where families lived in cramped conditions and rubbish was thrown out of the windows to the elegant Georgian New Town with its three main streets and a square at either end. Hear about the hangings that took place in the Grassmarket as you marvel at the impenetrable Castle Rock above you and hear about the attempts to capture Edinburgh Castle. See the Royal Mile which joins Edinburgh Castle to the Palace of Holyrood House. The tour will highlight all the things to do and see in Edinburgh as you tour through the city streets .Overnight at Hotel"
		),
		"day5" => array(
			"Edinburgh",
			"Breakfast at Hotel, <b>Full Day Original Loch Ness Tour</b> - Make your own way to the starting point of the tour. This is the Original Loch Ness and the Highlands tour. We leave Edinburgh behind we head towards Stirling with its ancient castle then stop for morning coffee stop at Kilmahog. Next it's Rob Roy Country and dramatic Rannoch Moor before arriving at the Weeping Glen - Glen Coe. After a short drive we arrive at Fort William which sits in the shadow of Ben Nevis, Britain's highest mountain. After time for lunch there's a photo stop at Spean Bridge before we enter the scenic beauty of the famous Great Glen. Our journey follows the route of the Caledonian Canal to Loch Ness and the impressive ruins of Urquhart Castle. The Castle provides a superb vantage point to see the deepest part of the loch. Here you can enjoy an optional visit to the castle and take a short cruise on the loch. On our homeward journey, we pass by Inverness then enter the wild beauty of the Cairngorm National Park. Our descent through the Grampian Mountains takes us into scenic Perthshire. The last part of our tour takes us through the ancient Kingdom of Fife to the famous Forth Rail Bridge and the conclusion of our tour in Edinburgh. Overnight at Hotel."
		),
		"day6" => array(
			"Edinburgh",
			"Breakfast at Hotel. Day free at leisure until departure transfer to Edinburgh Airport or Train Station for your onward journey. (Private Transfer)."
		),
		"inclusions" => array(
			"5 Nights Accommodation at Hotels as mentioned or similar",
			"Continental Breakfast Daily",
			"Arrival & Departure Transfers in all cities",
			"Sightseeing Tours on Seat In Coach Tours as mentioned in program",
			"Train Travel from London - Edinburgh"
		)
	)
);
?>