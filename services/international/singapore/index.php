<?php
$tourname = "Singapore";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2SINFE",
		"daysnights" => "3 Days 2 Nights Free and Easy Package!",
		"day1" => array(
			"Arrival in Singapore",
			"Meet and greet upon arrival to Singapore",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Singapore (B)",
			"Breakfast at your hotel.",
			"Compulsory half day city tour ending in Orchard Road.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day3" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Two nights hotel accommodation",
			"Daily Breakfast"
		)
	),
	array(
		"code" => "3SINTC",
		"daysnights" => "4 Days 3 Nights Tri-City Tour",
		"day1" => array(
			"Arrival in Singapore",
			"Meet and greet upon arrival to Singapore",
			"Transfer to the hotel of your choice for check in.",
			"Free at own leisure for the rest of the day. Overnight at your hotel."
		),
		"day2" => array(
			"Malaysia (B)",
			"Breakfast at your hotel.",
			"Johor Bahru or Kuala Lumpur, Malaysia Tour. Overnight at your hotel."
		),
		"day3" => array(
			"Indonesia (B)",
			"Breakfast at your hotel.",
			"Batam, Indonesia Tour with Lunch. Overnight at your hotel."
		),
		"day4" => array(
			"Departure (B)",
			"Breakfast at your hotel.",
			"Singapore half day city tour end in Orchard Road.(either 1st or 4th day)",
			"Free at own leisure until you transfer out to the airport for your flight out."
		),
		"inclusions" => array(
			"Meet and greet services upon arrival ",
			"Round trip airport transfers ",
			"Three nights hotel accommodation",
			"Daily Breakfast",
			"Batam, Indonesia Tour with Lunch",
			"Johor Bahru/Kuala Lumpur Tour"
		)
	)
);
?>