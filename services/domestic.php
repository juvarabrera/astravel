<div class="body-container color">
	<div class="wrapper">
		<div class="column-container">
			<div class="col-7" id="lblInformation">
				<h1>Making your Booking</h1><p>To make your tour/travel booking, simply select the package that you want and send over an email to AWT (refer to contact us) or better else, call and/or visit the company's office.</p>
			</div>
			<div class="col-3 left">
				<h1>Information</h1>
				<ul>
					<li><a onclick="showInfo('making-your-booking');">Making your Booking</a></li>
					<li><a onclick="showInfo('travel-insurance');">Travel Insurance</a></li>
					<li><a onclick="showInfo('payment-system');">Payment System</a></li>
					<li><a onclick="showInfo('cancellation-of-bookings');">Cancellation of bookings</a></li>
				</ul>
				<a href="services.php" class="button block">Back to Services</a>
			</div>
		</div>
	</div>
</div>
<div class="body-container">
	<div class="wrapper">
		<h1 class="center">Domestic Routes</h1>
		<div class="package-container">
		<?php
		$packages = array("North Luzon", "Bacolod", "Banaue", "Baguio", "Baler", "Bataan", "Batanes", "Bicol", "Bohol", "Boracay", "Cagayan de Oro", "Cebu", "Davao", "Dumaguete", "Ilocos", "Iloilo", "Manila", "Coron", "El Nido", "Puerto Princesa", "Pangasinan", "Puerto Galera", "Subic");
		foreach($packages as $package) {
			$code = str_replace(" ", "-", str_replace("/", "-", strtolower($package)));
			if(!file_exists("services/domestic/$code"))
				mkdir("services/domestic/$code", 0777, true);
		?>
			<a href="?domestic&p=<?php echo $code; ?>" class="package-list" style="background-image: url(services/domestic/<?php echo $code; ?>/thumbnail.jpg)">
				<div class="filter"></div>
				<div class="text"><?php echo $package; ?></div>
			</a>
		<?php
		}
		?>
		</div>
	</div>
</div>