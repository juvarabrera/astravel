<?php
$tourname = "North Luzon";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "3NORVP",
		"daysnights" => "For 4 days and 3 nights. Hundred Islands/Baguio/Vigan Package!",
		"inclusions" => array(
			"Roundtrip private transportation (Manila-Alaminos-Vigan-Baguio-Manila)",
			"1 night room accommodation in Alaminos",
			"1 night room accommodation in Baguio",
			"1 night room accommodation in Vigan",
			"Daily breakfast",
			"Halfday island hopping tour",
			"Baguio City Tour and Vigan Heritage Tour"
		)
	),
	array(
		"code" => "3NORBP",
		"daysnights" => "For 4 days and 3 nights. Banaue/Baguio Package!",
		"inclusions" => array(
			"Roundtrip private transportation (Manila-Banaue-Baguio-Manila)",
			"2 nights room accommodation at Banaue ",
			"1 night room accommodation at Baguio",
			"Daily breakfast inside hotel",
			"View Point & Village immersion tour in Banaue",
			"Baguio City Tour in Baguio"
		)
	),
	array(
		"code" => "4NORBP",
		"daysnights" => "For 5 days and 4 nights. Banaue/Sagada/Baguio Package!",
		"inclusions" => array(
			"Roundtrip private transportation (Manila-Banaue-Sagada-Baguio-Manila)",
			"2 nights room accommodation in Banaue",
			"1 night room accommodation in Sagada",
			"1 night room accommodation in Baguio",
			"Daily breakfast",
			"View Point & Village immersion tour in Banaue",
			"Hanging Coffin, Burial Cave and Sumaging Cave in Sagada",
			"Baguio City Tour"
		)
	)
);
?>