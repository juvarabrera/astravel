<?php
$tourname = "Cagayan de Oro";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2CGYFE",
		"daysnights" => "3 Days 2 Nights Cagayan de Oro Free and Easy Package!",
		"inclusions" => array(
			"Two nights hotel accommodation",
			"Roundtrip airport transfers",
			"Daily breakfast"
		),
		"optional" => array(
			"Bukidnon Countryside Tour from Cagayan De Oro with Lunch",
			"Camiguin Daytour from Cagayan De Oro with Lunch",
			"Cagayan De Oro Tour with Divine Mercy Shrine",
			"Cagayan De Oro City Tour + Whitewater Rafting with Lunch",
			"Cagayan De Oro City Day Tour",
			"Divine Mercy Shrine + Initao Misamis Oriental from Cagayan De Oro",
			"Divine Mercy Shrine Misamis Oriental from Cagayan De Oro",
			"Dahilayan Forest Park + Whitewater Rafting from Cagayan de Oro",
			"Dahilayan Forest Park + Del Monte Plantation from Cagayan de Oro",
			"Dahilayan Forest Park from Cagayan De Oro",
			"Cagayan De Oro City Tour + Makahambus Cave & Gorge with lunch",
			"Iligan Day Tour from Cagayan De Oro with Lunch",
			"Iligan Waterfalls from Cagayan De Oro with Lunch"
		)
	),
	array(
		"code" => "3CGYFE",
		"daysnights" => "4 Days 3 Nights Cagayan de Oro Free and Easy Package!",
		"inclusions" => array(
			"Three nights hotel accommodation",
			"Roundtrip airport transfers",
			"Daily breakfast"
		),
		"optional" => array(
			"Bukidnon Countryside Tour from Cagayan De Oro with Lunch",
			"Camiguin Daytour from Cagayan De Oro with Lunch",
			"Cagayan De Oro Tour with Divine Mercy Shrine",
			"Cagayan De Oro City Tour + Whitewater Rafting with Lunch",
			"Cagayan De Oro City Day Tour",
			"Divine Mercy Shrine + Initao Misamis Oriental from Cagayan De Oro",
			"Divine Mercy Shrine Misamis Oriental from Cagayan De Oro",
			"Dahilayan Forest Park + Whitewater Rafting from Cagayan de Oro",
			"Dahilayan Forest Park + Del Monte Plantation from Cagayan de Oro",
			"Dahilayan Forest Park from Cagayan De Oro",
			"Cagayan De Oro City Tour + Makahambus Cave & Gorge with lunch",
			"Iligan Day Tour from Cagayan De Oro with Lunch",
			"Iligan Waterfalls from Cagayan De Oro with Lunch"
		)
	)
);
?>