<?php
$tourname = "Ilocos";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2LAOFE",
		"daysnights" => "3 Days 2 Nights Ilocos Free and Easy Package!",
		"inclusions" => array(
			"Two nights hotel accommodation",
			"Roundtrip airport transfers",
			"Daily breakfast"
		),
		"optional" => array(
			"Vigan Tour",
			"Vigan - Laoag Tour",
			"Laoag Tour",
			"Laoag  - Pagudpud Tour"
		)
	),
	array(
		"code" => "3LAOFE",
		"daysnights" => "4 Days 3 Nights Ilocos Free and Easy Package!",
		"inclusions" => array(
			"Three nights hotel accommodation",
			"Roundtrip airport transfers",
			"Daily breakfast"
		),
		"optional" => array(
			"Vigan Tour",
			"Vigan - Laoag Tour",
			"Laoag Tour",
			"Laoag  - Pagudpud Tour"
		)
	)
);
?>