<?php
$tourname = "Banaue";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2BANBP",
		"daysnights" => "3 Days 2 Nights Banaue Barkada Package!",
		"inclusions" => array(
			"Roundtrip private transportation (Manila-Banaue-Manila)",
			"Two nights room accommodation",
			"Daily breakfast",
			"View Point and Village immersion tour"
		)
	)
);
?>