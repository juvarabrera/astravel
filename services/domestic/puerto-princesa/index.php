<?php
$tourname = "Puerto Princesa";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2PPSFE",
		"daysnights" => "3 Days 2 Nights Puerto Princessa Free and Easy Package",
		"inclusions" => array(
			"Two nights hotel accommodation",
			"Roundtrip airport transfers",
			"Daily breakfast"
		),
		"optional" => array(
			"Puerto Princesa City Tour",
			"Honda Bay Island Hopping",
			"Underground River tour",
			"Dos Palmas Tour",
			"Firefly Watching Tour",
			"Tabon Cave Tour",
			"Estrella Falls Tour"
		)
	),
	array(
		"code" => "3PPSFE",
		"daysnights" => "4 Days 3 Nights Puerto Princessa Free and Easy Package",
		"inclusions" => array(
			"Three nights hotel accommodation",
			"Roundtrip airport transfers",
			"Daily breakfast"
		),
		"optional" => array(
			"Puerto Princesa City Tour",
			"Honda Bay Island Hopping",
			"Underground River tour",
			"Dos Palmas Tour",
			"Firefly Watching Tour",
			"Tabon Cave Tour",
			"Estrella Falls Tour"
		)
	)
);
?>