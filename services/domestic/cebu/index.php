<?php
$tourname = "Cebu";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2CEBFE",
		"daysnights" => "3 Days 2 Nights Cebu Free and Easy Package!",
		"inclusions" => array(
			"Two nights hotel accommodation",
			"Roundtrip airport transfers",
			"Daily breakfast"
		),
		"optional" => array(
			"Half day City Tour",
			"Twin City Tour (Cebu City and Mactan)",
			"Cebu City Tour",
			"Day Swing to Bohol",
			"Island hopping",
			"Mactan Island Tour",
			"Panoramic Tour",
			"Sky Adventure",
			"Mactan Island and Yellow Submarine Undersea Tour",
			"Whale Sharks in Oslob, Cebu"
		)
	),
	array(
		"code" => "3CEBFE",
		"daysnights" => "4 Days 3 Nights Cebu Free and Easy Package!",
		"inclusions" => array(
			"Three nights hotel accommodation",
			"Roundtrip airport transfers",
			"Daily breakfast"
		),
		"optional" => array(
			"Half day City Tour",
			"Twin City Tour (Cebu City and Mactan)",
			"Cebu City Tour",
			"Day Swing to Bohol",
			"Island hopping",
			"Mactan Island Tour",
			"Panoramic Tour",
			"Sky Adventure",
			"Mactan Island and Yellow Submarine Undersea Tour",
			"Whale Sharks in Oslob, Cebu"
		)
	)
);
?>