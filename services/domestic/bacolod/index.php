<?php
$tourname = "Bacolod";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2BCDFE",
		"daysnights" => "3 Days 2 Nights Bacolod Free and Easy Package!",
		"inclusions" => array(
			"Roundtrip airport transfers",
			"Two nights room accommodation",
			"Daily breakfast"
		),
		"optional" => array(
			"Bacolod City Tour (Whole Day) with Ruins",
			"Bacolod City Tour (Half Day)"
		)
	),
	array(
		"code" => "3BCDFE",
		"daysnights" => "4 Days 3 Nights Bacolod Free and Easy Package!",
		"inclusions" => array(
			"Roundtrip airport transfers",
			"Two nights room accommodation",
			"Daily breakfast"
		),
		"optional" => array(
			"Bacolod City Tour (Whole Day) with Ruins",
			"Bacolod City Tour (Half Day)"
		)
	)
);
?>