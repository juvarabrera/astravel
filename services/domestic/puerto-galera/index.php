<?php
$tourname = "Puerto Galera";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2PTGFE",
		"daysnights" => "3 Days 2 Nights Puerto Galera Free and Easy Package!!",
		"inclusions" => array(
			"Two nights hotel accommodation",
			"Roundtrip ferry transfers",
			"Daily breakfast"
		)
	),
	array(
		"code" => "3PTGFE",
		"daysnights" => "4 Days 3 Nights Puerto Galera Free and Easy Package!!",
		"inclusions" => array(
			"Three nights hotel accommodation",
			"Roundtrip ferry transfers",
			"Daily breakfast"
		)
	)
);
?>