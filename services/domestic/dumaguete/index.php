<?php
$tourname = "Dumaguete";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2DGTFE",
		"daysnights" => "3 Days 2 Nights Dumaguete Free and Easy Package!",
		"inclusions" => array(
			"Two nights hotel accommodation",
			"Roundtrip airport transfers",
			"Daily breakfast"
		),
		"optional" => array(
			"Dumaguete Whole Day City and Countryside Tour with Lunch",
			"Apo Snorkelling Tour with Lunch",
			"Dumaguete Dolphin Watching + Campuyo Sandbar with Lunch",
			"Dumaguete Lake Balinsasayao Nature Trek / Sightseeing Tour",
			"Dumaguete Three Town Mountain Bike Tour",
			"Dumaguete Half Day City Tour + Lunch + Valencia/ Bacong Sidetrip",
			"Dumaguete Casaroro Falls Tour with Lunch",
			"Dumaguete Four Waterfalls Tour with Lunch",
			"Dumaguete Lake Balinsasayao Nature Trek and Kayak or Balanan Canoe and Kayak Tours",
			"Dumaguete Lake Balanan Sightseeing Tours with Lunch",
			"Dumaguete Mabinay 3 Caves Tour",
			"Dumaguete Oslob Whaleshark Tour",
			"Dumaguete Mabinay and Palimpinon White-water Kayak Tour with Lunch",
			"Dumaguete Caving and Canyoning Adventure with Lunch"
		)
	),
	array(
		"code" => "3DGTFE",
		"daysnights" => "4 Days 3 Nights Dumaguete Free and Easy Package!",
		"inclusions" => array(
			"Three nights hotel accommodation",
			"Roundtrip airport transfers",
			"Daily breakfast"
		),
		"optional" => array(
			"Dumaguete Whole Day City and Countryside Tour with Lunch",
			"Apo Snorkelling Tour with Lunch",
			"Dumaguete Dolphin Watching + Campuyo Sandbar with Lunch",
			"Dumaguete Lake Balinsasayao Nature Trek / Sightseeing Tour",
			"Dumaguete Three Town Mountain Bike Tour",
			"Dumaguete Half Day City Tour + Lunch + Valencia/ Bacong Sidetrip",
			"Dumaguete Casaroro Falls Tour with Lunch",
			"Dumaguete Four Waterfalls Tour with Lunch",
			"Dumaguete Lake Balinsasayao Nature Trek and Kayak or Balanan Canoe and Kayak Tours",
			"Dumaguete Lake Balanan Sightseeing Tours with Lunch",
			"Dumaguete Mabinay 3 Caves Tour",
			"Dumaguete Oslob Whaleshark Tour",
			"Dumaguete Mabinay and Palimpinon White-water Kayak Tour with Lunch",
			"Dumaguete Caving and Canyoning Adventure with Lunch"
		)
	)
);
?>