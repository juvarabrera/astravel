<?php
$tourname = "Baguio";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2BAGFE",
		"daysnights" => "3 Days 2 Nights Baguio Free and Easy Package!",
		"inclusions" => array(
			"Two nights hotel accommodation",
			"Daily breakfast"
		),
		"optional" => array(
			"Baguio Countryside Tour",
			"Baguio City Tour",
			"Marian Devotees Baguio Tour",
			"Balatoc Mines Underground Tour from Baguio City",
			"Masters Garden Organic Farm Tour + Lunch"
		)
	),
	array(
		"code" => "3BAGFE",
		"daysnights" => "4 Days 3 Nights Baguio Free and Easy Package!",
		"inclusions" => array(
			"Three nights hotel accommodation",
			"Daily breakfast"
		),
		"optional" => array(
			"Baguio Countryside Tour",
			"Baguio City Tour",
			"Marian Devotees Baguio Tour",
			"Balatoc Mines Underground Tour from Baguio City",
			"Masters Garden Organic Farm Tour + Lunch"
		)
	)
);
?>