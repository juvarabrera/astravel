<?php
$tourname = "Davao";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2DVOFE",
		"daysnights" => "3 Days 2 Nights Davao Free and Easy Package!",
		"inclusions" => array(
			"Two nights hotel accommodation",
			"Roundtrip airport transfers",
			"Daily breakfast"
		),
		"optional" => array(
			"Samal Island by Land Adventure + Bat Cave + Hagimit Falls + Maxima + Lunch",
			"Davao City Tour",
			"Pearl Farm Day Tour + Buffet Lunch",
			"Davao River Rafting Adventure + Picnic Lunch",
			"Philippine Eagle + Malagos Resort Walingwaling Tour + Snacks",
			"Eden Nature Park + Snacks",
			"Davao Outland Zipline Adventure + City Tour + Lunch",
			"Philippine Eagle + Crocodile Park + City Tour + Buffet Lunch",
			"Davao City Tour + Shopping + Samal Island Beach Tour + Picnic Lunch",
			"Durian Plantation Agri-Tour Experience + Lunch",
			"Banana Tagum Farm Agri-Tour Experience + Lunch"
		)
	),
	array(
		"code" => "3DVOFE",
		"daysnights" => "4 Days 3 Nights Boracay Free and Easy Package!",
		"inclusions" => array(
			"Three nights hotel accommodation",
			"Roundtrip airport transfers",
			"Daily breakfast"
		),
		"optional" => array(
			"Samal Island by Land Adventure + Bat Cave + Hagimit Falls + Maxima + Lunch",
			"Davao City Tour",
			"Pearl Farm Day Tour + Buffet Lunch",
			"Davao River Rafting Adventure + Picnic Lunch",
			"Philippine Eagle + Malagos Resort Walingwaling Tour + Snacks",
			"Eden Nature Park + Snacks",
			"Davao Outland Zipline Adventure + City Tour + Lunch",
			"Philippine Eagle + Crocodile Park + City Tour + Buffet Lunch",
			"Davao City Tour + Shopping + Samal Island Beach Tour + Picnic Lunch",
			"Durian Plantation Agri-Tour Experience + Lunch",
			"Banana Tagum Farm Agri-Tour Experience + Lunch"
		)
	)
);
?>