<?php
$tourname = "Iloilo";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2ILOFE",
		"daysnights" => "3 Days 2 Nights Iloilo Free and Easy Package!",
		"inclusions" => array(
			"Two nights hotel accommodation",
			"Roundtrip airport transfers",
			"Daily breakfast"
		),
		"optional" => array(
			"Iloilo City & Countryside Full Day Tour",
			"Iloilo Half Day City Tour ",
			"Iloilo Half Day Countryside Tour ",
			"Guimaras Day Tour ",
			"Iloilo City & Countryside Full Day Tour"
		)
	),
	array(
		"code" => "3ILOFE",
		"daysnights" => "4 Days 3 Nights Iloilo Free and Easy Package!",
		"inclusions" => array(
			"Three nights hotel accommodation",
			"Roundtrip airport transfers",
			"Daily breakfast"
		),
		"optional" => array(
			"Iloilo City & Countryside Full Day Tour",
			"Iloilo Half Day City Tour ",
			"Iloilo Half Day Countryside Tour ",
			"Guimaras Day Tour ",
			"Iloilo City & Countryside Full Day Tour"
		)
	)
);
?>