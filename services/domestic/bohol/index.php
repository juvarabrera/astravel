<?php
$tourname = "Bohol";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2TAGFE",
		"daysnights" => "3 Days 2 Nights Bohol Free and Easy Package!",
		"inclusions" => array(
			"Two nights hotel accommodation",
			"Round trip airport transfers",
			"Daily breakfast"
		),
		"optional" => array(
			"Countryside tour",
			"Panglao Tour",
			"Island hopping (Balicasag & Virgin island) & Dolphin/Whale watching tours",
			"Island Hopping & Snorkeling Tour (Pamilacan Island)",
			"Island Hopping & Snorkeling Tour (Balicasag Island)",
			"Twin island hopping tour and Snorkeling (Pamilacan & Balicasag)",
			"Bohol Loboc Night Cruise With Dinner",
			"Bohol Countryside Tour + Lunch + Zipline and Cable Car",
			"Bohol Firefly Watching Tour in Loay River with Dinner at Bohol Bee Farm"
		)
	),
	array(
		"code" => "3TAGFE",
		"daysnights" => "4 Days 3 Nights Bohol Free and Easy Package!",
		"inclusions" => array(
			"Three nights hotel accommodation",
			"Round trip airport transfers",
			"Daily breakfast"
		),
		"optional" => array(
			"Countryside tour",
			"Panglao Tour",
			"Island hopping (Balicasag & Virgin island) & Dolphin/Whale watching tours",
			"Island Hopping & Snorkeling Tour (Pamilacan Island)",
			"Island Hopping & Snorkeling Tour (Balicasag Island)",
			"Twin island hopping tour and Snorkeling (Pamilacan & Balicasag)",
			"Bohol Loboc Night Cruise With Dinner",
			"Bohol Countryside Tour + Lunch + Zipline and Cable Car",
			"Bohol Firefly Watching Tour in Loay River with Dinner at Bohol Bee Farm"
		)
	)
);
?>