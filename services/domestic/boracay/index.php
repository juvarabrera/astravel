<?php
$tourname = "Boracay";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2MPHFE",
		"daysnights" => "3 Days 2 Nights Boracay Free and Easy Package!",
		"inclusions" => array(
			"Two nights hotel accommodation",
			"Round trip airport transfers via Caticlan/Kalibo",
			"Daily breakfast"
		),
		"optional" => array(
			"Boracay Island Hopping with lunch",
			"Paraw Sailing",
			"3-in-1 Combo: Zorb Harness + Jetskiing + Parasailing",
			"Boracay Horseback Riding",
			"Boracay Parasailing",
			"Boracay Helmet Diving",
			"Boracay Jetskiing"
		)
	),
	array(
		"code" => "3MPHFE",
		"daysnights" => "4 Days 3 Nights Boracay Free and Easy Package!",
		"inclusions" => array(
			"Three nights hotel accommodation",
			"Round trip airport transfers via Caticlan/Kalibo",
			"Daily breakfast"
		),
		"optional" => array(
			"Boracay Island Hopping with lunch",
			"Paraw Sailing",
			"3-in-1 Combo: Zorb Harness + Jetskiing + Parasailing",
			"Boracay Horseback Riding",
			"Boracay Parasailing",
			"Boracay Helmet Diving",
			"Boracay Jetskiing"
		)
	)
);
?>