<?php
$tourname = "Bicol";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2LGPAS",
		"daysnights" => "3 Days 2 Nights Albay - Sorsogon Package!",
		"inclusions" => array(
			"Two nights hotel accommodation (Legaspi and Donsol)",
			"Round trip airport transfers",
			"Daily breakfast",
			"Van with Driver",
			"Legaspi City Tour",
			"Whale shark Watching",
			"Firefly watching",
			"Boat fees ",
			"Registration fee",
			"Entrance fees"
		)
	),
	array(
		"code" => "2WNPAS",
		"daysnights" => "3 Days 2 Nights Naga Package!",
		"inclusions" => array(
			"Two nights hotel accommodation",
			"Daily breakfast",
			"Round trip airport transfers",
			"Naga City Tour"
		)
	),
	array(
		"code" => "2WNPCP",
		"daysnights" => "3 Days 2 Nights Caramoan Package!",
		"inclusions" => array(
			"Two nights hotel accommodation",
			"Round trip airport transfers",
			"Daily breakfast",
			"Island Hopping Tour"
		),
		"optional" => array(
			"Half day Legaspi City Tour",
			"One day Legaspi City Tour",
			"Misibis Day Tour",
			"Donsol Whaleshark Interaction with Tour Transfers",
			"Naga City Tour",
			"Sorsogon Day Tour (min. 4 pax)"
		)
	)
);
?>