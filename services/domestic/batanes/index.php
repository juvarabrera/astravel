<?php
$tourname = "Batanes";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2BSOFB",
		"daysnights" => "3 Days 2 Nights Batanes Package!",
		"inclusions" => array(
			"Two nights hotel accommodation",
			"Round trip airport transfers",
			"Full board meals",
			"North Batan island tour",
			"South Batan island tour",
			"Land transfers",
			"Municipal fees",
			"Government fees and taxes (Batanes)"
		)
	),
	array(
		"code" => "2BSOWS",
		"daysnights" => "3 Days 2 Nights Batanes Package!",
		"inclusions" => array(
			"Two nights hotel accommodation",
			"Round trip airport transfers",
			"Full board meals",
			"North Batan island tour",
			"South Batan island tour",
			"Sabtang island tour",
			"Land transfers",
			"Boat transfers",
			"Municipal fees",
			"Government fees and taxes (Batanes)"
		)
	),
	array(
		"code" => "3BSOFB",
		"daysnights" => "4 Days 3 Nights Batanes Package!",
		"inclusions" => array(
			"Three nights hotel accommodation",
			"Round trip airport transfers",
			"Full board meals",
			"North Batan island tour",
			"South Batan island tour",
			"Sabtang island Tour ",
			"Land transfers",
			"Boat transfers (Ivana Port - Sabtang - Ivana port)",
			"Municipal fees",
			"Government fees and taxes (Batanes)"
		)
	),
	array(
		"code" => "4BSOFB",
		"daysnights" => "5 Days 4 Nights Batanes Package!",
		"inclusions" => array(
			"Four nights hotel accommodation",
			"Round trip airport transfers",
			"Full board meals",
			"North Batan island tour",
			"South Batan island tour",
			"Sabtang island Tour ",
			"Land transfers",
			"Boat transfers (Ivana Port - Sabtang - Ivana port)",
			"Municipal fees",
			"Government fees and taxes (Batanes)"
		)
	)
);
?>