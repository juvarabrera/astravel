<?php
$tourname = "El Nido, Palawan";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2PPSEN",
		"daysnights" => "3 Days 2 Nights Free and Easy Package",
		"inclusions" => array(
			"Two nights room accommodation",
			"Round trip airport transfers from Puerto Princesa",
			"Daily breakfast"
		)
	),
	array(
		"code" => "3PPSEN",
		"daysnights" => "4 Days 3 Nights Free and Easy Package",
		"inclusions" => array(
			"Three nights room accommodation",
			"Round trip airport transfers from Puerto Princesa",
			"Daily breakfast"
		)
	),
	array(
		"code" => "2PPSEN - A",
		"daysnights" => "3 Days 2 Nights El Nido Package - A",
		"inclusions" => array(
			"Two nights room accommodation",
			"Round trip airport transfers from Puerto Princesa",
			"Daily Breakfast",
			"Island Hopping - A",
			"Picnic Lunch",
			"Rental of Masks and Snorkels"
		),
		"highlights" => array(
			"Small Lagoon",
			"Big Lagoon",
			"Simezu Island",
			"Seven Commando"
		)
	),
	array(
		"code" => "2PPSEN - B",
		"daysnights" => "3 Days 2 Nights El Nido Package - B",
		"inclusions" => array(
			"Two nights room accommodation",
			"Round trip airport transfers from Puerto Princesa",
			"Daily Breakfast",
			"Island Hopping - B",
			"Picnic Lunch",
			"Rental of Masks and Snorkels"
		),
		"highlights" => array(
			"Pangulasian Island",
			"Snake Island",
			"Cudugnon Cave",
			"Cathedral Cave",
			"Pinagbuyutan Island"
		)
	),
	array(
		"code" => "2PPSEN - C",
		"daysnights" => "3 Days 2 Nights El Nido Package - C",
		"inclusions" => array(
			"Two nights room accommodation",
			"Round trip airport transfers from Puerto Princesa",
			"Daily Breakfast",
			"Island Hopping – C",
			"Picnic Lunch",
			"Rental of Masks and Snorkels"
		),
		"highlights" => array(
			"Hidden Beach",
			"Matinloc Shrine",
			"Tapiutan Island",
			"Secret Beach",
			"Bukal Island"
		)
	),
	array(
		"code" => "2PPSEN - D",
		"daysnights" => "3 Days 2 Nights El Nido Package - D",
		"inclusions" => array(
			"Two nights room accommodation",
			"Round trip airport transfers from Puerto Princesa",
			"Daily Breakfast",
			"Island Hopping – D",
			"Picnic Lunch",
			"Rental of Masks and Snorkels"
		),
		"highlights" => array(
			"Cadlao Lagoon",
			"Pasadigan Beach",
			"Natnat Beach",
			"Bukal Island"
		)
	)
);
?>