<?php
$tourname = "Bataan";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "1BATBP",
		"daysnights" => "2 Days 1 Night Bataan Package",
		"inclusions" => array(
			"One night room accommodation",
			"Daily Breakfast"
		),
		"optional" => array(
			"Las Casas de Acuzar Heritage Tour",
			"Bataan World War 2 Battle Site"
		)
	),
	array(
		"code" => "2BATBP",
		"daysnights" => "3 Days 2 Nights Bataan Package",
		"inclusions" => array(
			"Two nights room accommodation",
			"Daily Breakfast"
		),
		"optional" => array(
			"Las Casas de Acuzar Heritage Tour",
			"Bataan World War 2 Battle Site"
		)
	)
);
?>