<?php
$tourname = "Subic";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "1SFSFE",
		"daysnights" => "2 Days 1 Night Subic Free and Easy Package!",
		"inclusions" => array(
			"One night room accommodation",
			"Daily breakfast"
		),
		"optional" => array(
			"Ocean Adventure Park Subic Admission Tickets",
			"Swim Encounter with Dolphins at Ocean Adventure Subic",
			"Dolphin Beach Encounter at Ocean Adventure Subic",
			"Dive Encounter with Dolphins at Ocean Adventure Subic",
			"Animal Trainer Adventure at Ocean Adventure Subic"
		)
	),
	array(
		"code" => "2SFSFE",
		"daysnights" => "3 Days 2 Nights Subic Free and Easy Package!",
		"inclusions" => array(
			"Two nights room accommodation",
			"Daily breakfast"
		),
		"optional" => array(
			"Ocean Adventure Park Subic Admission Tickets",
			"Swim Encounter with Dolphins at Ocean Adventure Subic",
			"Dolphin Beach Encounter at Ocean Adventure Subic",
			"Dive Encounter with Dolphins at Ocean Adventure Subic",
			"Animal Trainer Adventure at Ocean Adventure Subic"
		)
	)
);
?>