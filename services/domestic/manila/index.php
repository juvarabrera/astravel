<?php
$tourname = "Manila";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2MNLFE",
		"daysnights" => "3 Days 2 Nights Manila Package",
		"inclusions" => array(
			"Two nights hotel accommodation",
			"Round trip airport transfer",
			"Daily breakfast",
			"Half day Manila city tour"
		),
		"optional" => array(
			"Manila Whole day city tour",
			"Manila Ocean Park",
			"Manila Museum Tours",
			"Manila Bay Dinner Cruise",
			"Half day shopping tour",
			"Whole day shopping tour",
			"Cultural Dinner Show"
		)
	),
	array(
		"code" => "3MNLFE",
		"daysnights" => "4 Days 3 Nights Manila Package",
		"inclusions" => array(
			"Three nights hotel accommodation",
			"Round trip airport transfer",
			"Daily breakfast",
			"Half day Manila city tour"
		),
		"optional" => array(
			"Manila Whole day city tour",
			"Manila Ocean Park",
			"Manila Museum Tours",
			"Manila Bay Dinner Cruise",
			"Half day shopping tour",
			"Whole day shopping tour",
			"Cultural Dinner Show"
		)
	)
);
?>