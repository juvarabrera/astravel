<?php
$tourname = "Coron, Palawan";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2USUCT",
		"daysnights" => "3 Days 2 Nights Coron Trip",
		"day1" => array(
			"Lualhati Park",
			"Souvenir Shop",
			"Cashew Factory",
			"Mt. Tapyas",
			"Maquinit Hotspring"
		),
		"day2" => array(
			"Kayangan Lake",
			"Atwayan Beach",
			"CYC Beach",
			"Coral Garden",
			"Quin Reef",
			"Hidden Lagoon"
		),
		"inclusions" => array(
			"2 Nights Accommodation",
			"Daily Breakfast",
			"1 day town tour",
			"1 day island hopping",
			"Roundtrip airport transfer
			Pick and drop hotel to wharf vice versa",
			"Tourist Boat",
			"Life Vest",
			"All Permits and entrance fees",
			"Tour guide",
			"Buffet lunch and pm snacks during island hopping",
			"Cottage rental",
			"Government taxes"
		)
	),
	array(
		"code" => "2USUIE",
		"daysnights" => "3 Days 2 Nights Coron Island Tour + Island Escapade",
		"day1" => array(
			"Kayangan Lake",
			"Atwayan Beach",
			"CYC Beach",
			"Coral Garden",
			"Quin Reef",
			"Hidden Lagoon"
		),
		"day2" => array(
			"Banana Island",
			"Bulog Dos",
			"Malcapuya Island"
		),
		"inclusions" => array(
			"2 Nights Accommodation",
			"Daily Breakfast",
			"1 days island hopping",
			"Roundtrip airport transfer",
			"Pick and drop hotel to wharf vice versa",
			"Tourist Boat",
			"Life Vest",
			"All Permits and entrance fees",
			"Tour guide",
			"Buffet lunch and pm snacks during island hopping",
			"Cottage rental",
			"Government taxes"
		)
	),
	array(
		"code" => "2USUWP",
		"daysnights" => "3 Days 2 Nights Coron Island Tour + Reefs and Wrecks Adventure",
		"day1" => array(
			"Kayangan Lake",
			"Atwayan Beach",
			"CYC Beach",
			"Coral Garden",
			"Quin Reef",
			"Hidden Lagoon"
		),
		"day2" => array(
			"Pass Island",
			"Lusong Coral Garden",
			"Lusong Gunboat Shipwreck",
			"East Tangat Wreck"
		),
		"inclusions" => array(
			"Two nights hotel accommodation",
			"Roundtrip airport transfers",
			"Daily breakfast",
			"Two days island hopping tour",
			"Pick and drop hotel to wharf vice versa",
			"Tourist Boat",
			"Life Vest",
			"All Permits and entrance fees",
			"Tour guide",
			"Buffet lunch and pm snacks during island hopping",
			"Cottage rental",
			"Government taxes"
		)
	),
	array(
		"code" => "2USUCS",
		"daysnights" => "3 Days 2 Nights Coron Island Tour + Calauit Safari Tour",
		"day1" => array(
			"Kayangan Lake",
			"Atwayan Beach",
			"CYC Beach",
			"Coral Garden",
			"Quin Reef",
			"Hidden Lagoon"
		),
		"day2" => array(
			"Calauit Safari Tour",
			"Black Island",
			"Lusong Gunboat Shipwreck",
			"Lusong Coral Garden",
			"East Tangat Wreck"
		),
		"inclusions" => array(
			"2 Nights Accommodation",
			"Daily Breakfast",
			"2 days island hopping",
			"Roundtrip airport transfer",
			"Pick and drop hotel to wharf vice versa",
			"Tourist Boat",
			"Life Vest",
			"All Permits and entrance fees",
			"Tour guide",
			"Buffet lunch and pm snacks during island hopping",
			"Cottage rental",
			"Government taxes"
		)
	),
	array(
		"code" => "3USUIE",
		"daysnights" => "4 Days 3 Nights Coron Island Tour + Island Escapade",
		"day1" => array(
			"Lualhati Park",
			"Souvenir Shop",
			"Cashew Factory",
			"Mt. Tapyas",
			"Maquinit Hotspring"
		),
		"day2" => array(
			"Kayangan Lake",
			"Atwayan Beach",
			"CYC Beach",
			"Coral Garden",
			"Quin Reef",
			"Hidden Lagoon"
		),
		"day3" => array(
			"Banana Island",
			"Bulog Dos",
			"Malcapuya Island"
		),
		"inclusions" => array(
			"3 Nights Accommodation",
			"Daily Breakfast",
			"1 day Town Tour",
			"2 days Island Hopping",
			"Roundtrip airport transfer",
			"Pick and drop hotel to wharf vice versa",
			"Tourist Boat",
			"Life Vest",
			"All Permits and entrance fees",
			"Tour guide",
			"Buffet lunch and pm snacks during island hopping",
			"Cottage rental",
			"Government taxes"
		)
	),
	array(
		"code" => "3USUWP",
		"daysnights" => "4 Days 3 Nights Coron Island Tour + Reefs and Wrecks Adventure",
		"day1" => array(
			"Lualhati Park",
			"Souvenir Shop",
			"Cashew Factory",
			"Mt. Tapyas",
			"Maquinit Hotspring"
		),
		"day2" => array(
			"Kayangan Lake",
			"Atwayan Beach",
			"CYC Beach",
			"Coral Garden",
			"Quin Reef",
			"Hidden Lagoon"
		),
		"day3" => array(
			"Pass Island",
			"Lusong Coral Garden",
			"Lusong Gunboat Shipwreck",
			"East Tangat Wreck"
		),
		"inclusions" => array(
			"3 Nights Accommodation",
			"Daily Breakfast",
			"1 day town tour",
			"2 days island hopping",
			"Roundtrip airport transfer",
			"Pick and drop hotel to wharf vice versa",
			"Tourist Boat",
			"Life Vest",
			"All Permits and entrance fees",
			"Tour guide",
			"Buffet lunch and pm snacks during island hopping",
			"Cottage rental",
			"Government taxes"
		)
	),
	array(
		"code" => "3USUCS",
		"daysnights" => "4 Days 3 Nights Coron Island Tour + Calauit Safari Tour",
		"day1" => array(
			"Lualhati Park",
			"Souvenir Shop",
			"Cashew Factory",
			"Mt. Tapyas",
			"Maquinit Hotspring"
		),
		"day2" => array(
			"Kayangan Lake",
			"Atwayan Beach",
			"CYC Beach",
			"Coral Garden",
			"Quin Reef",
			"Hidden Lagoon"
		),
		"day3" => array(
			"Calauit Safari Tour",
			"Black Island",
			"Lusong Gunboat Shipwreck",
			"Lusong Coral Garden",
			"East Tangat Wreck"
		),
		"inclusions" => array(
			"3 Nights Accommodation",
			"Daily Breakfast",
			"1 day town tour",
			"2 days island hopping",
			"Roundtrip airport transfer",
			"Pick and drop hotel to wharf vice versa",
			"Tourist Boat",
			"Life Vest",
			"All Permits and entrance fees",
			"Tour guide",
			"Buffet lunch and pm snacks during island hopping",
			"Cottage rental",
			"Government taxes"
		)
	)
);
?>