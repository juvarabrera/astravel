<?php
$tourname = "Pangasinan";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "2PANFE",
		"daysnights" => "3 Days 2 Nights Pangasinan Free and Easy Package!",
		"inclusions" => array(
			"Two nights hotel accommodation",
			"Roundtrip airport transfers",
			"Daily breakfast",
			"Island Hopping Tour"
		)
	),
	array(
		"code" => "3PANFE",
		"daysnights" => "4 Days 3 Nights Pangasinan Free and Easy Package!",
		"inclusions" => array(
			"Three nights hotel accommodation",
			"Roundtrip airport transfers",
			"Daily breakfast",
			"Island Hopping Tour"
		)
	)
);
?>