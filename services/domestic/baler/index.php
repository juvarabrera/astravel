<?php
$tourname = "Baler";
$code = str_replace(" ", "-", str_replace("/", "-", strtolower($tourname)));
$packages = array(
	array(
		"code" => "1BLRBP",
		"daysnights" => "2 Days 1 Night Baler Package",
		"inclusions" => array(
			"One night room accommodation",
			"Round trip transfers ( Manila - Baler - Manila)",
			"Daily Breakfast",
			"One or Two days Baler Tour",
			"Baler Surfing with Instructor"
		)
	),
	array(
		"code" => "2BLRBP",
		"daysnights" => "3 Days 2 Nights Baler Package",
		"inclusions" => array(
			"Two nights room accommodation",
			"Round trip transfers ( Manila - Baler - Manila)",
			"Daily Breakfast",
			"One or Two days Baler Tour",
			"Baler Surfing with Instructor"
		)
	)
);
?>