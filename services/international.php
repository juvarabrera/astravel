<div class="body-container color">
	<div class="wrapper">
		<div class="column-container">
			<div class="col-7" id="lblInformation">
				<h1>Making your Booking</h1><p>To make your tour/travel booking, simply select the package that you want and send over an email to AWT (refer to contact us) or better else, call and/or visit the company's office.</p>
			</div>
			<div class="col-3 left">
				<h1>Information</h1>
				<ul>
					<li><a onclick="showInfo('making-your-booking');">Making your Booking</a></li>
					<li><a onclick="showInfo('travel-insurance');">Travel Insurance</a></li>
					<li><a onclick="showInfo('payment-system');">Payment System</a></li>
					<li><a onclick="showInfo('cancellation-of-bookings');">Cancellation of bookings</a></li>
				</ul>
				<a href="services.php" class="button block">Back to Services</a>
			</div>
		</div>
	</div>
</div>
<div class="body-container">
	<div class="wrapper">
		<h1 class="center">East Asia</h1>
		<div class="package-container">
		<?php
		$packages = array("China", "Hong Kong", "Korea", "Macau");
		foreach($packages as $package) {
			$code = str_replace(" ", "-", str_replace("/", "-", strtolower($package)));
		?>
			<a href="?international&p=<?php echo $code; ?>" class="package-list" style="background-image: url(services/international/<?php echo $code; ?>/thumbnail.jpg)">
				<div class="filter"></div>
				<div class="text"><?php echo $package; ?></div>
			</a>
		<?php
		}
		?>
		</div>
		<h1 class="center">South East Asia</h1>
		<div class="package-container">
		<?php
		$packages = array("Brunei", "Cambodia", "Indonesia", "Laos", "Malaysia", "Myanmar", "Singapore", "Thailand", "Vietnam");
		foreach($packages as $package) {
			$code = str_replace(" ", "-", str_replace("/", "-", strtolower($package)));
		?>
			<a href="?international&p=<?php echo $code; ?>" class="package-list" style="background-image: url(services/international/<?php echo $code; ?>/thumbnail.jpg)">
				<div class="filter"></div>
				<div class="text"><?php echo $package; ?></div>
			</a>
		<?php
		}
		?>
		</div>
		<h1 class="center">South Asia</h1>
		<div class="package-container">
		<?php
		$packages = array("Bhutan", "India", "Maldives", "Nepal", "Sri Lanka");
		foreach($packages as $package) {
			$code = str_replace(" ", "-", str_replace("/", "-", strtolower($package)));
		?>
			<a href="?international&p=<?php echo $code; ?>" class="package-list" style="background-image: url(services/international/<?php echo $code; ?>/thumbnail.jpg)">
				<div class="filter"></div>
				<div class="text"><?php echo $package; ?></div>
			</a>
		<?php
		}
		?>
		</div>
		<h1 class="center">Middle East Asia</h1>
		<div class="package-container">
		<?php
		$packages = array("United Arab Emirates");
		foreach($packages as $package) {
			$code = str_replace(" ", "-", str_replace("/", "-", strtolower($package)));
		?>
			<a href="?international&p=<?php echo $code; ?>" class="package-list" style="background-image: url(services/international/<?php echo $code; ?>/thumbnail.jpg)">
				<div class="filter"></div>
				<div class="text"><?php echo $package; ?></div>
			</a>
		<?php
		}
		?>
		</div>
		<h1 class="center">Europe</h1>
		<div class="package-container">
		<?php
		$packages = array("Europe", "Spain", "Scandinavia", "Italy", "Paris", "Iceland", "Scotland", "Austria", "London/Scotland", "London/Paris", "Switzerland", "United Kingdom", "Austria/Italy", "Italy/Switzerland", "Turkey", "Greece");
		sort($packages);
		foreach($packages as $package) {
			$code = str_replace(" ", "-", str_replace("/", "-", strtolower($package)));
		?>
			<a href="?international&p=<?php echo $code; ?>" class="package-list" style="background-image: url(services/international/<?php echo $code; ?>/thumbnail.jpg)">
				<div class="filter"></div>
				<div class="text"><?php echo $package; ?></div>
			</a>
		<?php
		}
		?>
		</div>
	</div>
</div>