<div class="body-container color">
	<div class="wrapper">
		<div class="column-container">
			<div class="col-6 offset-2 center">
				<h1>Services</h1>
				<p>The company offers a number of tour packages affordable enough and one that would meet the traveller's expectations and limitations.</p>
				<p>The tour package ranges from 2 to 7 days but flexible arrangements can be made by request.</p>
			</div>
		</div>
	</div>
</div>
<div class="body-container">
	<div class="wrapper">
		<div class="column-container">
			<div class="col-5 center">
				<h1>Domestic Route</h1>
				<a href="services.php?domestic" class="circle-image" style="background-image: url(images/skin/default/bg/domestic.jpg)">

				</a>
			</div>
			<div class="col-5 center">
				<h1>International Route</h1>
				<a href="services.php?international" class="circle-image" style="background-image: url(images/skin/default/bg/international.jpg)">

				</a>
			</div>
		</div>
		<div class="column-container" name="special">
			<div class="col-10 center">
				<p>While the company provides standard products and services, the company is also willing to provide special packages, domestic and international travel/tour packages for students, and corporate and business organizaitions.</p><br>
				<hr>
				<h1>Special Services</h1>
				<div class="icon-container">
				<?php
				$services = array(
					"Domestic and International Flight Ticketing",
					"Domestic and International Tour Packages",
					"Domestic and International Hotel Booking",
					"Domestic and International Tours",
					"Domestic and International Car Rentals",
					"International Attractions Admission",
					"Ferry ticketing",
					"International Cruise Booking",
					"Bus Ticket Reservation",
					"Passport Assistance",
					"Visa Assistance",
					"Travel Insurance"
				);
				foreach($services as $service) {
					$code = str_replace(" ", "-", strtolower($service));
				?>
					<div class="icon-list">
						<div class="icon" style="background-image: url(images/skin/default/icons/<?php echo $code; ?>.png);"></div>
						<span><?php echo $service; ?></span>
					</div>
				<?php
				}
				?>
				</div>
			</div>
		</div>
	</div>
</div>