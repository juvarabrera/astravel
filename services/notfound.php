<div class="body-container" id="bdyCon1">
	<div class="wrapper">
		<div class="column-container">
			<div class="col-6 offset-2 center">
				<h1>Package not found</h1>
				<p>The package you're looking is missing or does not exist.</p>
				<a onclick="hideContainer('bdyCon1');" class="button">Dismiss</a>
			</div>
		</div>
	</div>
</div>