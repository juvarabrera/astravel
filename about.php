<?php 
	$backDir = '../../';
	if(file_exists($backDir."config.php")) {
		require_once($backDir."config.php"); 
		$pageTitle = "AWT | About";
		require_once($backDir."pageview.php");
	}
?><!DOCTYPE html>
<html>
<head>
	<title>Astravel World Tours</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
	<link rel="shortcut icon" href="favicon.ico" />
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="styles/style.php">
	<script src="scripts/jquery.min.js"></script>
	<script src="scripts/smooth_scroll.js"></script>
	<meta name="keywords" content="Astravel world world tours Philippines ph astra star cavite operator cavite agent Philippine philippine Philippine agent Philippine operator international domestic customize group pilgrimage individual southeast asia promos philippine flight ticketing cavite flight ticketing international flight ticketing Philippine airlines cebu pacific air air asia zest tiger airways cebgo skyjet cathay pacific delta airlines united airways Korean air asiana airlines jetstar Malaysia airlines emirates luftansa klm Qatar airlines royal brunei airlines china airlines eva air china southern airlines china eastern airlines garuda Indonesia air busan jeju air jetstar asia airways silkair Singapore airlines tigerair thai airways Bangkok airways thai airasia Etihad Vietnam airlines  hotel reservation hotel booking Philippine hotel reservation Philippine hotel booking international hotel booking international hotel reservation international tickets universal studios legoland Disneyland madame tussauds noah’s ark sentosa hello kitty town genting highlands windows of the world cu chi tunnel nanta car rentals Philippine car rentals international car rentals ferry ticketing sun cruises 2go star cruises royal Caribbean oceania cruises celebrity cruises international cruise reservation Philippine ferry reservation Philippine ferry booking international cruise booking Philippine bus ticketing philtranco ohayahami dltb south west tours passport assistance Philippine passport assistance visa assistance Philippine visa assistance united states visa assistance Schengen visa assistance Europe visa assistance united kingdom visa assistance uae visa assistance abu dhabi visa assistance dubai visa assistance china visa assistance Taiwan visa assistance Australia visa assistance spain visa assistance france visa assistance south Africa visa assistance Netherlands visa assistance Holland visa assistance Greece visa assistance Switzerland visa assistance korea visa assistance japan visa assiatance Egypt visa assistance Czech republic visa assistance turkey visa assistance india visa assistance Sweden visa assistance Russia visa assistance Poland visa assistance Norway visa assistance Denmark visa assistance Ireland visa assistance Belgium visa assistance Austria visa assistance insurance domestic insurance Schengen insurance Europe insurance bluecross worldwide insurance north Luzon baguio ilocos norte ilocos sur banaue sagada alaminos hundred islands Bacolod baler bataan batanes bicol bohol boracay Cagayan de oro cebu cavite davao dumaguete Iloilo manila manila coron el nido Puerto princesa Palawan Puerto galera pangasinan subic tagaytay Corregidor china Beijing shanghai korea south korea seoul jeju island busan macau bruinei Cambodia Indonesia laos Jakarta bali Malaysia kuala lumpur Langkawi johor bahru Yogyakarta Myanmar Singapore Thailand Bangkok Phuket phi phi island Ayutthaya pattaya Vietnam Hanoi ho chi minh Saigon siem reap phnom penh Bhutan india Maldives Nepal sri lanka Tibet uae dubai abu dhabi Europe Austria Italy rome Vatican Greece Santorini Athens Iceland Reykjavik venice Switzerland London united kingdom uk paris france Scotland Scandinavia Norway finland Sweden Denmark spain madrid Barcelona turkey Israel Taiwan ctta member dot accredited agent dot accredited operator department of tourism accredited establishment philtoa Philippine operator association member cavite and tours association member facebook Instagram twitter tumblr packages astravel travel tour">
	<meta name="description" content="Astravel world world tours Philippines ph astra star cavite operator cavite agent Philippine philippine Philippine agent Philippine operator international domestic customize group pilgrimage individual southeast asia promos philippine flight ticketing cavite flight ticketing international flight ticketing Philippine airlines cebu pacific air air asia zest tiger airways cebgo skyjet cathay pacific delta airlines united airways Korean air asiana airlines jetstar Malaysia airlines emirates luftansa klm Qatar airlines royal brunei airlines china airlines eva air china southern airlines china eastern airlines garuda Indonesia air busan jeju air jetstar asia airways silkair Singapore airlines tigerair thai airways Bangkok airways thai airasia Etihad Vietnam airlines  hotel reservation hotel booking Philippine hotel reservation Philippine hotel booking international hotel booking international hotel reservation international tickets universal studios legoland Disneyland madame tussauds noah’s ark sentosa hello kitty town genting highlands windows of the world cu chi tunnel nanta car rentals Philippine car rentals international car rentals ferry ticketing sun cruises 2go star cruises royal Caribbean oceania cruises celebrity cruises international cruise reservation Philippine ferry reservation Philippine ferry booking international cruise booking Philippine bus ticketing philtranco ohayahami dltb south west tours passport assistance Philippine passport assistance visa assistance Philippine visa assistance united states visa assistance Schengen visa assistance Europe visa assistance united kingdom visa assistance uae visa assistance abu dhabi visa assistance dubai visa assistance china visa assistance Taiwan visa assistance Australia visa assistance spain visa assistance france visa assistance south Africa visa assistance Netherlands visa assistance Holland visa assistance Greece visa assistance Switzerland visa assistance korea visa assistance japan visa assiatance Egypt visa assistance Czech republic visa assistance turkey visa assistance india visa assistance Sweden visa assistance Russia visa assistance Poland visa assistance Norway visa assistance Denmark visa assistance Ireland visa assistance Belgium visa assistance Austria visa assistance insurance domestic insurance Schengen insurance Europe insurance bluecross worldwide insurance north Luzon baguio ilocos norte ilocos sur banaue sagada alaminos hundred islands Bacolod baler bataan batanes bicol bohol boracay Cagayan de oro cebu cavite davao dumaguete Iloilo manila manila coron el nido Puerto princesa Palawan Puerto galera pangasinan subic tagaytay Corregidor china Beijing shanghai korea south korea seoul jeju island busan macau bruinei Cambodia Indonesia laos Jakarta bali Malaysia kuala lumpur Langkawi johor bahru Yogyakarta Myanmar Singapore Thailand Bangkok Phuket phi phi island Ayutthaya pattaya Vietnam Hanoi ho chi minh Saigon siem reap phnom penh Bhutan india Maldives Nepal sri lanka Tibet uae dubai abu dhabi Europe Austria Italy rome Vatican Greece Santorini Athens Iceland Reykjavik venice Switzerland London united kingdom uk paris france Scotland Scandinavia Norway finland Sweden Denmark spain madrid Barcelona turkey Israel Taiwan ctta member dot accredited agent dot accredited operator department of tourism accredited establishment philtoa Philippine operator association member cavite and tours association member facebook Instagram twitter tumblr packages astravel travel tour">
</head>
<body>
<div id="container">
	<div class="full-cover small">
		<div class="photo"></div>
		<div class="filter"></div>
		<div class="text">Astravel World Tours<br><small><small>The Guiding Star</small></small></div>
		<div id="action-bar">
			<a id="logo" class="logo"></a>
			<ul class="navigation">
				<li><a href="index.php">Home</a></li>
				<li><a href="about.php">About</a></li>
				<li><a href="services.php">Services</a></li>
				<li><a href="contact.php">Contact Us</a></li>
			</ul>
		</div>
	</div>
	<div class="body-container">
		<div class="wrapper">
			<div class="column-container">
				<div class="col-8 offset-1">
					<h1>About</h1>
					<p>Astravel World Tours started as a small business concern born out of a desire to travel locally and abroad as well as to help or assists others experience the best of the travel adventure and get your money's worth.</p>
					<p>Fresh from a college degree in hotel and restaurant management earned in a top-rated university in the Philippines, Anna Kristy Dequito ventured into travel-related business solely providing airline ticketing services. The passion to travel and serve others was made via a sole proprietorship business named Astravel and Tours and this was in the year 2012.</p>
					<p>After gaining ground and envisioning to explore other markets, Astravel and Tours was expanded and reorganized as a corporation under Philippine laws. Certificate of Registration was issued by the Philippine's Securities and Exchange Commission (SEC) in October 21, 2014.</p>
					<p>After obtaining its incorporation documents and other government clearances as corporation with regional and global vision, Astravel World Tours is now an accredited travel agency of Department of Tourism (DOT) of the Philippines and a member of Philippine Tour Operators Association (PHILTOA) an Cavite Travel and Tours Association (CTTA).</p>
					<p>The  shift from a small business to a regionally and globally-conscious travel services organization continued to do its business with passion and pride not only to provide travel-related services that it affordable but moreso, a travel experience worth remembering for its clienteles.</p>
					<p>The name "Astravel" was derived inspired by a Latin word "Astra" which means "star". Connecting the kind of services the company provides - travel business - the word Astravel was born.  The resulting corporate name and concept of travel services provided by the company has biblical underpinnings inspired by the three wise men who spotted a star( a comet) that guided them to a manger where a child is born. With this in mind, Astravel World Tours will not just provide tickets for the trips but travel-related products and services that will meet the expectations of its clients.</p>
				</div>
				<div class="col-4">
					<img src="images/skin/default/logo/about.png" style="width: 100%;">
				</div>
				<div class="col-6">	
					<h1>Company</h1>
					<p>Travel for us is an extraordinary way that needs to be experienced as it creates a form of satisfaction not just for fun or leisure, but also for the desire of advancement in life. Business is one of the main purpose of travel but unknowingly, this leads to acquisition of greater knowledge and ideas as well, possibly greater than the one we acquired from any institutions.<br><br>
					Tourism, here and abroad, is one of the fastest growing industries, especially with today's demand, the need for leisure and business purposes are not only based on the necessity of one's cravings but also to experience the art of learning.<br><br>
					AsTravel is a pioneer company catering a wide variety of International and Domestic flights, Hotels, and tour packages which will suit our clients' needs and requested demands. We welcome individuals, groups and corporate clients to experience a one of a kind service in line of travel and tourism.<br><br>
					Customization guests request in tour packages is our expertise. We will be merging with the top operators here and abroad. Our travel agency will also connect with local, heritage, and world tourism for us to serve our clients at its best.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="body-container color">
		<div class="wrapper">
			<div class="column-container">
				<div class="col-4">
					<h1>Mission</h1>
					<p>The pioneer travel agency will be a competitive company assimilating excellence and personalized travel services whether it will be for business, education, leisure or fun. A great, efficient and equal service will be provided to all our clients.</p>
					<h1>Vision</h1>
					<p>Astravel aims to be recognized around the metro providing a one of a kind service in terms of customizing your own travel package.</p>
				</div>
				<div class="col-4 offset-1">
					<h1>Goals</h1>
					<p>
						T - rustworthy, always truthful<br><br>
						R - emarkable, extraordinary services<br><br>
						A - miable, friendly staffs<br><br>
						V - elocity, fast and on time services and transactions<br><br>
						E - quality, fairness to all our clients<br><br>
						L - egit, everything is legal
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="body-container logos">
		<div class="wrapper">
			<div class="column-container">
				<div class="col-10 center">
					<img src="images/skin/default/bg/ctta.png"/>
					<img src="images/skin/default/bg/dot.png"/>
					<img src="images/skin/default/bg/philtoa.png"/>
				</div>
			</div>
		</div>
	</div>
	<?php require_once("includes/footer.inc"); ?>
</div>
<script src="scripts/main.js"></script>
</body>
</html>