<?php 
	$backDir = '../../';
	if(file_exists($backDir."config.php")) {
		require_once($backDir."config.php"); 
		$pageTitle = "AWT | Contact";
		require_once($backDir."pageview.php");
	}
?><!DOCTYPE html>
<html>
<head>
	<title>Astravel World Tours</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
	<link rel="shortcut icon" href="favicon.ico" />
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="styles/style.php">
	<script src="scripts/jquery.min.js"></script>
	<script src="scripts/smooth_scroll.js"></script>
	<meta name="keywords" content="Astravel world world tours Philippines ph astra star cavite operator cavite agent Philippine philippine Philippine agent Philippine operator international domestic customize group pilgrimage individual southeast asia promos philippine flight ticketing cavite flight ticketing international flight ticketing Philippine airlines cebu pacific air air asia zest tiger airways cebgo skyjet cathay pacific delta airlines united airways Korean air asiana airlines jetstar Malaysia airlines emirates luftansa klm Qatar airlines royal brunei airlines china airlines eva air china southern airlines china eastern airlines garuda Indonesia air busan jeju air jetstar asia airways silkair Singapore airlines tigerair thai airways Bangkok airways thai airasia Etihad Vietnam airlines  hotel reservation hotel booking Philippine hotel reservation Philippine hotel booking international hotel booking international hotel reservation international tickets universal studios legoland Disneyland madame tussauds noah’s ark sentosa hello kitty town genting highlands windows of the world cu chi tunnel nanta car rentals Philippine car rentals international car rentals ferry ticketing sun cruises 2go star cruises royal Caribbean oceania cruises celebrity cruises international cruise reservation Philippine ferry reservation Philippine ferry booking international cruise booking Philippine bus ticketing philtranco ohayahami dltb south west tours passport assistance Philippine passport assistance visa assistance Philippine visa assistance united states visa assistance Schengen visa assistance Europe visa assistance united kingdom visa assistance uae visa assistance abu dhabi visa assistance dubai visa assistance china visa assistance Taiwan visa assistance Australia visa assistance spain visa assistance france visa assistance south Africa visa assistance Netherlands visa assistance Holland visa assistance Greece visa assistance Switzerland visa assistance korea visa assistance japan visa assiatance Egypt visa assistance Czech republic visa assistance turkey visa assistance india visa assistance Sweden visa assistance Russia visa assistance Poland visa assistance Norway visa assistance Denmark visa assistance Ireland visa assistance Belgium visa assistance Austria visa assistance insurance domestic insurance Schengen insurance Europe insurance bluecross worldwide insurance north Luzon baguio ilocos norte ilocos sur banaue sagada alaminos hundred islands Bacolod baler bataan batanes bicol bohol boracay Cagayan de oro cebu cavite davao dumaguete Iloilo manila manila coron el nido Puerto princesa Palawan Puerto galera pangasinan subic tagaytay Corregidor china Beijing shanghai korea south korea seoul jeju island busan macau bruinei Cambodia Indonesia laos Jakarta bali Malaysia kuala lumpur Langkawi johor bahru Yogyakarta Myanmar Singapore Thailand Bangkok Phuket phi phi island Ayutthaya pattaya Vietnam Hanoi ho chi minh Saigon siem reap phnom penh Bhutan india Maldives Nepal sri lanka Tibet uae dubai abu dhabi Europe Austria Italy rome Vatican Greece Santorini Athens Iceland Reykjavik venice Switzerland London united kingdom uk paris france Scotland Scandinavia Norway finland Sweden Denmark spain madrid Barcelona turkey Israel Taiwan ctta member dot accredited agent dot accredited operator department of tourism accredited establishment philtoa Philippine operator association member cavite and tours association member facebook Instagram twitter tumblr packages astravel travel tour">
	<meta name="description" content="Astravel world world tours Philippines ph astra star cavite operator cavite agent Philippine philippine Philippine agent Philippine operator international domestic customize group pilgrimage individual southeast asia promos philippine flight ticketing cavite flight ticketing international flight ticketing Philippine airlines cebu pacific air air asia zest tiger airways cebgo skyjet cathay pacific delta airlines united airways Korean air asiana airlines jetstar Malaysia airlines emirates luftansa klm Qatar airlines royal brunei airlines china airlines eva air china southern airlines china eastern airlines garuda Indonesia air busan jeju air jetstar asia airways silkair Singapore airlines tigerair thai airways Bangkok airways thai airasia Etihad Vietnam airlines  hotel reservation hotel booking Philippine hotel reservation Philippine hotel booking international hotel booking international hotel reservation international tickets universal studios legoland Disneyland madame tussauds noah’s ark sentosa hello kitty town genting highlands windows of the world cu chi tunnel nanta car rentals Philippine car rentals international car rentals ferry ticketing sun cruises 2go star cruises royal Caribbean oceania cruises celebrity cruises international cruise reservation Philippine ferry reservation Philippine ferry booking international cruise booking Philippine bus ticketing philtranco ohayahami dltb south west tours passport assistance Philippine passport assistance visa assistance Philippine visa assistance united states visa assistance Schengen visa assistance Europe visa assistance united kingdom visa assistance uae visa assistance abu dhabi visa assistance dubai visa assistance china visa assistance Taiwan visa assistance Australia visa assistance spain visa assistance france visa assistance south Africa visa assistance Netherlands visa assistance Holland visa assistance Greece visa assistance Switzerland visa assistance korea visa assistance japan visa assiatance Egypt visa assistance Czech republic visa assistance turkey visa assistance india visa assistance Sweden visa assistance Russia visa assistance Poland visa assistance Norway visa assistance Denmark visa assistance Ireland visa assistance Belgium visa assistance Austria visa assistance insurance domestic insurance Schengen insurance Europe insurance bluecross worldwide insurance north Luzon baguio ilocos norte ilocos sur banaue sagada alaminos hundred islands Bacolod baler bataan batanes bicol bohol boracay Cagayan de oro cebu cavite davao dumaguete Iloilo manila manila coron el nido Puerto princesa Palawan Puerto galera pangasinan subic tagaytay Corregidor china Beijing shanghai korea south korea seoul jeju island busan macau bruinei Cambodia Indonesia laos Jakarta bali Malaysia kuala lumpur Langkawi johor bahru Yogyakarta Myanmar Singapore Thailand Bangkok Phuket phi phi island Ayutthaya pattaya Vietnam Hanoi ho chi minh Saigon siem reap phnom penh Bhutan india Maldives Nepal sri lanka Tibet uae dubai abu dhabi Europe Austria Italy rome Vatican Greece Santorini Athens Iceland Reykjavik venice Switzerland London united kingdom uk paris france Scotland Scandinavia Norway finland Sweden Denmark spain madrid Barcelona turkey Israel Taiwan ctta member dot accredited agent dot accredited operator department of tourism accredited establishment philtoa Philippine operator association member cavite and tours association member facebook Instagram twitter tumblr packages astravel travel tour">
</head>
<body>
<div id="container">
	<div class="full-cover small">
		<div class="photo"></div>
		<div class="filter"></div>
		<div class="text">Astravel World Tours<br><small><small>The Guiding Star</small></small></div>
		<div id="action-bar">
			<a id="logo" class="logo"></a>
			<ul class="navigation">
				<li><a href="index.php">Home</a></li>
				<li><a href="about.php">About</a></li>
				<li><a href="services.php">Services</a></li>
				<li><a href="contact.php">Contact Us</a></li>
			</ul>
		</div>
	</div>
	<?php
	if(isset($_GET['success'])) {
	?>
	<div class="body-container color" id="bdyCon3">
		<div class="wrapper">
			<div class="col-7">
				<h1>Message</h1>
				<p>Your message has successfully sent to our mail!</p>
			</div>
			<div class="col-10 right">
				<a onclick="hideContainer('bdyCon3');" class="button">Dismiss</a>
			</div>
		</div>
	</div>
	<?php
	}if(isset($_GET['error'])) {
	?>
	<div class="body-container color" id="bdyCon3">
		<div class="wrapper">
			<div class="col-7">
				<h1>Error</h1>
				<p>There's an error while sending the message. Please fill up the form properly.</p>
			</div>
			<div class="col-10 right">
				<a onclick="hideContainer('bdyCon3');" class="button">Dismiss</a>
			</div>
		</div>
	</div>
	<?php
	}
	?>
	<div class="body-container">
		<div class="wrapper">
			<h1>Contact</h1>
			<div class="column-container">
				<div class="col-6">
					<form action="process.php?action=send" method="post">
						<table class="form">
							<tr>
								<td>Name</td>
								<td><input type="text" name="name" placeholder="Required"></td>
							</tr>
							<tr>
								<td>Email</td>
								<td><input type="text" name="email" placeholder="Required"></td>
							</tr>
							<tr>
								<td>Contact Number</td>
								<td><input type="text" name="contact" placeholder="Optional"></td>
							</tr>
							<tr>
								<td>Subject</td>
								<td><input type="text" name="subject" placeholder="Required"></td>
							</tr>
							<tr valign="top">
								<td>Message</td>
								<td><textarea name="message" placeholder="Required"><?php
								if(isset($_GET['p'])) {
									$p = $_GET['p'];
									echo 'Package Name: '.$p.'

';
								}
								if(isset($_GET['code'])) {
									$code = $_GET['code'];
									echo 'Tour Code: '.$code.'

';
								}
								echo 'Message here...'
								?></textarea></td>
							</tr>
							<tr>
								<td></td>
								<td><input type="submit" value="Send" name="submit"></td>
							</tr>
						</table>
					</form>
				</div>
				<div class="col-4">
					<b>Address: </b>Unit D Paris Bldg., Brgy. Zone 1-A, Dasmari&ntilde;as, Cavite<br><br>
					<b>Phone: </b>(046) 686-4102<br><br>
					<b>Phone/Fax: </b>(046) 424 - 0957<br><br>
					<b>Mobile Numbers:</b>
					<ul>
						<li><b>Globe: </b>09174267908</li>
						<li><b>Smart: </b>09205014107</li>
						<li><b>Sun Cellular: </b>09435267926</li>
					</ul>
					<b>Email: </b>inquiry@astravel.com.ph
				</div>
			</div>
		</div>
	</div>
	<div class="body-container logos">
		<div class="wrapper">
			<div class="column-container">
				<div class="col-10 center">
					<img src="images/skin/default/bg/ctta.png"/>
					<img src="images/skin/default/bg/dot.png"/>
					<img src="images/skin/default/bg/philtoa.png"/>
				</div>
			</div>
		</div>
	</div>
	<?php require_once("includes/footer.inc"); ?>
</div>
<script src="scripts/main.js"></script>
</body>
</html>