<!DOCTYPE html>
<html>
<head>
	<title>AsTravel</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
	<link rel="shortcut icon" href="../favicon.ico" />
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="/styles/style.php">
	<script src="/scripts/jquery.min.js"></script>
</head>
<body>
<div id="container">
	<div class="full-cover small">
		<div class="photo"></div>
		<div class="filter"></div>
		<div class="text">Astravel World Tours<br><small><small>The Guiding Star</small></small></div>
		<div id="action-bar">
			<a id="logo" class="logo"></a>
			<ul class="navigation">
				<li><a href="/index.php">Home</a></li>
				<li><a href="/about.php">About</a></li>
				<li><a href="/services.php">Services</a></li>
				<li><a href="/contact.php">Contact Us</a></li>
			</ul>
		</div>
	</div>
	<div class="body-container color">
		<div class="wrapper">
			<div class="column-container">
				<div class="col-6 center offset-2">
					<h1>Error 404</h1>
					<p>The page you are looking for does not exist or unavailable.</p>
				</div>
			</div>
		</div>
	</div>
	<?php require_once("../includes/footer.inc"); ?>
</div>
<script src="/scripts/main.js"></script>
</body>
</html>