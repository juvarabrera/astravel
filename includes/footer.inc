
	<div id="footer-container">
		<div class="photo"></div>
		<div class="filter"></div>
		<div class="wrapper">
			<div class="column-container">
				<div class="col-4 social">
					<h4>Astravel World Tours</h4>
					<p>Unit D Paris Bldg., Brgy. Zone 1-A, Dasmari&ntilde;as, Cavite<br>
					Phone: (046) 686-4102<br>
					Email us at inquiry@astravel.com.ph<br>
					<a href="/contact.php">More ways to contact us</a></p>
					<a href="https://www.facebook.com/astravelph" target="_blank"><img src="images/skin/default/bg/facebook.png"></a>
					<a href="https://twitter.com/myastravel" target="_blank"><img src="images/skin/default/bg/twitter.png"></a>
					<a href="https://instagram.com/astravelph/" target="_blank"><img src="images/skin/default/bg/instagram.png"></a>
					<a href="https://www.tumblr.com/blog/myastravel" target="_blank"><img src="images/skin/default/bg/tumblr.png"></a>
				</div>
				<div class="col-3">
					<h4>Company</h4>
					<ul>
						<li><a href="/about.php">About</a></li>
						<li><a href="/terms.php">Terms of Use</a></li>
						<li><a href="/privacy.php">Privacy Policy</a></li>
					</ul>
				</div>
				<div class="col-3">
					<h4>Services</h4>
					<ul>
						<li><a href="/services.php?domestic">Domestic Route</a></li>
						<li><a href="/services.php?international">International Route</a></li>
						<li><a href="/services.php">Special Services</a></li>
					</ul>
				</div>
			</div>
			<hr>
			<div class="column-container copyright">
				<div class="col-5">Copyright Astravel World Tours 2015</div>
				<div class="col-5 right">Designed by <a href="http://www.juvarabrera.com" target="_blank">Juvar Abrera</a></div>
			</div>
		</div>
	</div>